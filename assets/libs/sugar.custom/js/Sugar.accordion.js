﻿Sugar.accordion = {
	init:function(){
		Sugar.accordion.bind();
	},
	bind: function () {
		angular.element(document).on("click keydown", ".accordion-item-title", function (e) {

			if (e.type == 'keydown') {
				var keycode = e.keyCode || e.which || e.charCode;
				if (keycode != 32 && keycode!= 13) return;
			}
			//e.preventDefault();
			e.stopPropagation();

			$target = angular.element(this).parent();
			$target.toggleClass("active");
			//$target.find('.accordion-item-content').toggle();
		})
	}
}
Sugar.accordion.init();