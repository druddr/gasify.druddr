﻿Sugar.datepicker = {
	template: '',
	months: ['jan', 'fev', 'mar', 'abr', 'mai', 'jun', 'jul', 'ago', 'set', 'out', 'nov', 'dez'],
	getJsDate: function (input) {
		var parts = input.match(/(\d+)/g);
		return new Date(parts[0], parts[1] - 1, parts[2]);
	},
	date: function (date) {
		var currrent_date = date("d/m/Y");
		console.log(currrent_date);
		return current_date;
	},
	events: function () {

		angular.element(document).on(Sugar.events.touchstart, ".modal .calendar .day", function (e) {
			e.preventDefault();
			var day = str_pad(this.innerText, 2, '0', 'STR_PAD_LEFT');
			var $modal = angular.element(document.querySelectorAll(".modal.datepicker"));
			$modal.find("[data-day]").attr("data-day", day).html(day);
			var $current = angular.element(this);
			var day = str_pad($current.attr("data-calendar-day"), 2, "0", 'STR_PAD_LEFT');
			var month = str_pad($current.attr("data-calendar-month"), 2, "0", 'STR_PAD_LEFT');
			var year = $current.attr("data-calendar-year");
			$modal.find("[data-day]").attr("data-day", day).html(day);
			$modal.find("[data-month]").attr("data-month", month).html(Sugar.datepicker.months[parseInt(month - 1)]);
			$modal.find("[data-year]").attr("data-year", year).html(year);


			angular.element(document.querySelectorAll("#modal-date-picker .day")).removeClass("current");
			angular.element(this).addClass("current");

		});
		angular.element(document).on(Sugar.events.touchstart, ".modal .date-text .prev", function () {
			var $self = angular.element(this), $parent = $self.parent(),
				$day = $parent.find("[data-day]"),
				$month = $parent.find("[data-month]"),
				$year = $parent.find("[data-year]"),
				day = $day.attr("data-day") == '' ? date("d") : $day.attr("data-day"),
				month = $month.attr("data-month") == '' ? date("m") : $month.attr("data-month"),
				year = $year.attr("data-year") == '' ? date("Y") : $year.attr("data-year"),
				time = strtotime(" -1 month", strtotime(year + "-" + month + "-" + day));

			$day.html(date("d", time)).attr("data-day", date("d", time));
			$month.html(Sugar.datepicker.months[parseInt(date("m", time)) - 1]).attr("data-month", date("m", time));
			$year.html(date("Y", time)).attr("data-year", date("Y", time));
			angular.element(document.querySelectorAll(".modal .calendar")).html(Sugar.calendar.create(date("Y", time), date("m", time), date("d", time)));

		});
		angular.element(document).on(Sugar.events.touchstart, ".modal .date-text .next", function () {
			var $self = angular.element(this), $parent = $self.parent(),
				$day = $parent.find("[data-day]"),
				$month = $parent.find("[data-month]"),
				$year = $parent.find("[data-year]"),
				day = $day.attr("data-day") == '' ? date("d") : $day.attr("data-day"),
				month = $month.attr("data-month") == '' ? date("m") : $month.attr("data-month"),
				year = $year.attr("data-year") == '' ? date("Y") : $year.attr("data-year"),
				time = strtotime(" +1 month", strtotime(year + "-" + month + "-" + day));

			$day.html(date("d", time)).attr("data-day", date("d", time));
			$month.html(Sugar.datepicker.months[parseInt(date("m", time)) - 1]).attr("data-month", date("m", time));
			$year.html(date("Y", time)).attr("data-year", date("Y", time));
			angular.element(document.querySelectorAll(".modal .calendar")).html(Sugar.calendar.create(date("Y", time), date("m", time), date("d", time)));

		});
		angular.element(document).on("focus", "[data-date-picker-open]", function (e) {
			e.preventDefault();
			var $self = angular.element(this);
			var day = $self.val().substr(0, 2) == "" ? date("d") : $self.val().substr(0, 2);
			var month = $self.val().substr(3, 2) == "" ? date("m") : $self.val().substr(3, 2);
			var year = $self.val().substr(6, 4) == "" ? date("Y") : $self.val().substr(6, 4);

			Sugar.datepicker.open($self);
			angular.element(document.querySelectorAll(".modal .calendar")).html(Sugar.calendar.create(year, month, day));
			$self.blur();
		})
	},
	init: function () {
		if (angular.element(document.querySelectorAll("#modal-date-picker")).size() == 0) {
			angular.element(document.body).append('<div class="modal datepicker" id="modal-date-picker">' +
								'	<div class="title">Datepicker</div>' +
								'	<div class="content">' +
								'		<div class="date-text">' +
								'			<div class="prev fa fa-caret-left"></div>' +
								'			<div data-day class="day">20</div>' +
								'			<div data-month class="month">AGO</div>' +
								'			<div data-year class="year">2015</div>' +
								'			<div class="next fa fa-caret-right"></div>' +
								'		</div>' +
								'		<div class="week">' +
								'		</div>' +
								'		<div class="calendar">' +
								'		</div>' +
								'	</div>' +
								'	<div class="actions right">' +
								'		<button data-close-modal="modal-date-picker">Ok</button>' +
								'	</div>' +
								'</div>');
		}
		Sugar.datepicker.events();
	},
	open: function ($element) {
		var $modal = angular.element(document.body).find(".modal.datepicker");

		var day = $element.val().substr(0, 2);
		var month = $element.val().substr(3, 2);
		var year = $element.val().substr(6, 4);

		$modal.find("button").unbind(Sugar.events.touchstart).on(Sugar.events.touchstart, function () {
			var $current = $modal.find(".day.current");
			var day = str_pad($current.attr("data-calendar-day"), 2, "0", 'STR_PAD_LEFT');
			var month = str_pad($current.attr("data-calendar-month"), 2, "0", 'STR_PAD_LEFT');
			var year = $current.attr("data-calendar-year");
			$modal.find("[data-day]").attr("data-day", day).html(day);
			$modal.find("[data-month]").attr("data-month", month).html(Sugar.datepicker.months[parseInt(month - 1)]);
			$modal.find("[data-year]").attr("data-year", year).html(year);
			$element.val(day + "/" + month + "/" + year);
			$element.trigger("input");
		});

		$modal.find(".calendar .day").removeClass("current").each(function () {
			if (this.innerHTML == day) angular.element(this).addClass("current");

		});
		$modal.show().addClass("fade-in");

	},
	close: function ($element) {

	}
}
Sugar.datepicker.init();