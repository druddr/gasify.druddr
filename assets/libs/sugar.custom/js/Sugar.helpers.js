﻿Sugar.dateToISO = function (date) {
	if (date.indexOf("/") > -1) {
		var sdate = date.split("/");
		return sdate[2] + "-" + sdate[1] + "-" + sdate[0];
	}
	if (date.indexOf("/") == -1 && date.indexOf("-") == -1) {
		date = date + "";
		return date[4] + date[5] + date[6] + date[7] + '-' + date[2] + date[3] + '-' + date[0] + date[1];
	}
	return date;
};
Sugar.numberToDateISO = function (number) {
	var sdate = (number + "");
	return sdate.substr(4, 4) + "-" + sdate.substr(2, 2) + "-" + sdate.substr(0, 2);
};
Sugar.generateGuid = function () {
	var d = new Date().getTime();
	if (window.performance && typeof window.performance.now === "function") {
		d += performance.now(); //use high-precision timer if available
	}
	var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
		var r = (d + Math.random() * 16) % 16 | 0;
		d = Math.floor(d / 16);
		return (c == 'x' ? r : (r & 0x3 | 0x8)).toString(16);
	});
	return uuid;
};
Sugar.capitalize = function (s) {
	return s.charAt(0).toUpperCase() + s.slice(1);
}
Sugar.generateRandom = function (n) {
	var ranNum = Math.round(Math.random() * n);
	return ranNum;
}

// Função para retornar o resto da divisao entre números (mod)
Sugar.mod = function (dividendo, divisor) {
	return Math.round(dividendo - (Math.floor(dividendo / divisor) * divisor));
}
Sugar.valueGenerator = {

	// Função que gera números de CPF válidos
	cpf: function () {
		var n = 9;
		var n1 = Sugar.generateRandom(n);
		var n2 = Sugar.generateRandom(n);
		var n3 = Sugar.generateRandom(n);
		var n4 = Sugar.generateRandom(n);
		var n5 = Sugar.generateRandom(n);
		var n6 = Sugar.generateRandom(n);
		var n7 = Sugar.generateRandom(n);
		var n8 = Sugar.generateRandom(n);
		var n9 = Sugar.generateRandom(n);
		var d1 = n9 * 2 + n8 * 3 + n7 * 4 + n6 * 5 + n5 * 6 + n4 * 7 + n3 * 8 + n2 * 9 + n1 * 10;
		d1 = 11 - (Sugar.mod(d1, 11));
		if (d1 >= 10) d1 = 0;
		var d2 = d1 * 2 + n9 * 3 + n8 * 4 + n7 * 5 + n6 * 6 + n5 * 7 + n4 * 8 + n3 * 9 + n2 * 10 + n1 * 11;
		d2 = 11 - (Sugar.mod(d2, 11));
		if (d2 >= 10) d2 = 0;
		return '' + n1 + n2 + n3 + '.' + n4 + n5 + n6 + '.' + n7 + n8 + n9 + '-' + d1 + d2;
	},

	// Função que gera números de CNPJ válidos
	cnpj: function () {
		var n = 9;
		var n1 = Sugar.generateRandom(n);
		var n2 = Sugar.generateRandom(n);
		var n3 = Sugar.generateRandom(n);
		var n4 = Sugar.generateRandom(n);
		var n5 = Sugar.generateRandom(n);
		var n6 = Sugar.generateRandom(n);
		var n7 = Sugar.generateRandom(n);
		var n8 = Sugar.generateRandom(n);
		var n9 = 0;//Sugar.generateRandom(n);
		var n10 = 0;//Sugar.generateRandom(n);
		var n11 = 0;//Sugar.generateRandom(n);
		var n12 = 1;//Sugar.generateRandom(n);
		var d1 = n12 * 2 + n11 * 3 + n10 * 4 + n9 * 5 + n8 * 6 + n7 * 7 + n6 * 8 + n5 * 9 + n4 * 2 + n3 * 3 + n2 * 4 + n1 * 5;
		d1 = 11 - (Sugar.mod(d1, 11));
		if (d1 >= 10) d1 = 0;
		var d2 = d1 * 2 + n12 * 3 + n11 * 4 + n10 * 5 + n9 * 6 + n8 * 7 + n7 * 8 + n6 * 9 + n5 * 2 + n4 * 3 + n3 * 4 + n2 * 5 + n1 * 6;
		d2 = 11 - (Sugar.mod(d2, 11));
		if (d2 >= 10) d2 = 0;
		return '' + n1 + n2 + '.' + n3 + n4 + n5 + '.' + n6 + n7 + n8 + '/' + n9 + n10 + n11 + n12 + '-' + d1 + d2;
	},

	name: function () {
		var firstName = ["Runny", "Buttercup", "Dinky", "Stinky", "Crusty",
		"Greasy", "Gidget", "Cheesypoof", "Lumpy", "Wacky", "Tiny", "Flunky",
		"Fluffy", "Zippy", "Doofus", "Gobsmacked", "Slimy", "Grimy", "Salamander",
		"Oily", "Burrito", "Bumpy", "Loopy", "Snotty", "Irving", "Egbert"];
		var middleName = ["Waffer", "Lilly", "Rugrat", "Sand", "Fuzzy", "Kitty",
		 "Puppy", "Snuggles", "Rubber", "Stinky", "Lulu", "Lala", "Sparkle", "Glitter",
		 "Silver", "Golden", "Rainbow", "Cloud", "Rain", "Stormy", "Wink", "Sugar",
		 "Twinkle", "Star", "Halo", "Angel"];
		var lastName1 = ["Snicker", "Buffalo", "Gross", "Bubble", "Sheep",
		 "Corset", "Toilet", "Lizard", "Waffle", "Kumquat", "Burger", "Chimp", "Liver",
		 "Gorilla", "Rhino", "Emu", "Pizza", "Toad", "Gerbil", "Pickle", "Tofu",
		"Chicken", "Potato", "Hamster", "Lemur", "Vermin"];
		var lastName2 = ["face", "dip", "nose", "brain", "head", "breath",
		"pants", "shorts", "lips", "mouth", "muffin", "butt", "bottom", "elbow",
		"honker", "toes", "buns", "spew", "kisser", "fanny", "squirt", "chunks",
		"brains", "wit", "juice", "shower"];

		return firstName[Sugar.generateRandom(firstName.length - 1)] +
				" " + middleName[Sugar.generateRandom(middleName.length - 1)] +
				" " + lastName1[Sugar.generateRandom(lastName1.length - 1)] +
				lastName2[Sugar.generateRandom(lastName2.length - 1)];
	}
}