﻿Sugar.alert = function (message, type) {
	angular.element(document.querySelectorAll(".alert")).remove();
	var div = document.createElement("div");
	div.className = "alert " + type;
	div.innerHTML = "<div class='message animated slideInUp'>" + message + "</div>";
	document.body.appendChild(div);
	//angular.element(document.querySelectorAll(".app-bar,#main, nav.drawer")).addClass("blur");


	angular.element(div).on("click", function () {
		var $alert = $(this);
		$alert.addClass("fade-out");
		setTimeout(function () {
			$alert.remove();
			//angular.element(document.querySelectorAll(".app-bar,#main,nav.drawer")).removeClass("blur");
		}, 300)
	});

}
Sugar.alert.error = function (message) {
	Sugar.alert(message, 'fail');
}
Sugar.alert.success = function (message) {
	Sugar.alert(message, 'success');
}
