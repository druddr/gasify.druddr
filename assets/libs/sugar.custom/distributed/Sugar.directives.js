angular.module("Sugar", ["ui.utils.masks"]);
angular.module("Sugar").directive('longPress', function ($timeout) {
	return {
		restrict: 'A',
		link: function (scope, element, attrs, ctrl) {
			scope.timeout = null;
			element.bind('mousedown', function (e) {
				clearTimeout(scope.timeout);
				scope.timeout = setTimeout(function () {
					scope.$apply(function () {
						scope.$eval(attrs.longPress);
					})
				}, 500);
			})
			element.bind('mouseup mouseleave', function (e) {
				clearTimeout(scope.timeout);
				scope.timeout = null;
			})
		}
	}
});
function inputBase(aExtraInputDirectives) {
	aExtraInputDirectives = aExtraInputDirectives instanceof Array ? aExtraInputDirectives : [];

	var sTemplate = '' +
		'<div class="form-field-input form-field-group {{size}}">' +
		'	<label ng-if="label" for="input-{{uid}}" ng-class="{' +
		'				\'disabled\': (disabled),' +
		'				\'focused always-focused\':(type && type != \'text\' && type != \'password\') || prefix || sufix || (disabled && ngModel),' +
		'				\'invalid\':oController.$$parentForm[name].$invalid && oController.$$parentForm[name].$dirty' +
		'		   }">' +
		'		{{label}}' +
		'		<i ng-if="required" class="font-size-1" >*</i>' +
		'	</label>' +
		'	<input type="{{type}}"' +
		'		   class="form-control {{size}}"' +
		'		   ng-model="ngModel"' +
		'		   name="{{::name}}"' +
		'		   ng-required="required"' +
		'		   ng-minlength="minlength"' +
		'		   ng-maxlength="maxlength"' +
		'		   ' + aExtraInputDirectives.join(' ') + 
		'		   ng-disabled="disabled"' +
		'		   autocapitalize="off"' +
		'		   autocorrect="off"' +
		'		   autocomplete="off"' +
		'		   spellcheck="false" />' +
		'	<div class="input-border-bottom" ng-class="{\'invalid\': oController.$$parentForm[name].$invalid && oController.$$parentForm[name].$dirty}"></div>' +
		'	<div class="input-error-message">' +
		'		<div ng-if="oController.$$parentForm[name].$invalid && oController.$$parentForm[name].$dirty">' +
		'			{{invalidMessage ? invalidMessage : ("messages.formFieldInvalid"|translate)}}' +
		'		</div>' +
		'		<div ng-if="oController.$$parentForm[name].$error.required && oController.$$parentForm[name].$dirty">' +
		'			{{("messages.formFieldRequiredInvalid"|translate)}}' +
		'		</div>' +
		'		<div ng-if="oController.$$parentForm[name].$error.minlength && oController.$$parentForm[name].$dirty">' +
		'			{{("messages.formFieldMinLengthInvalid"|translate)}} {{minlength}}' +
		'		</div>' +
		'		<div ng-if="oController.$$parentForm[name].$error.maxlength && oController.$$parentForm[name].$dirty">' +
		'			{{("messages.formFieldMaxLengthInvalid"|translate)}} {{maxlength}}' +
		'		</div>' +
		'	</div>' +
		'</div>';


	return {
		restrict: 'E',
		replace: true,
		require: 'ngModel',
		scope: {
			label: '@?',
			ngModel: '=',
			name: '@?',
			disabled: '=?',
			type: '@?',
			required: '=?',
			minlength: '=?',
			maxlength: '=?',
			invalidMessage: '@?'
		},
		link: function(scope, element, attr, controller) {
			scope.oController = controller;
			scope.name = scope.name ? scope.name : Sugar.generateGuid();
			element.removeAttr('disabled ng-model type required minlength maxlength name label')
		},
		template: sTemplate

	}
}
angular.module("Sugar").directive("accordionItem", ['$compile', function ($compile) {
	return {
		replace: true,
		restrict: 'E',
		transclude: {
			name: 'name',
			content: 'content'
		},
		scope: {
			withPadding:'=?'
		},
		template:''+
		'<section class="accordion-item" ng-class="{\'with-padding\':withPadding}">' +
		'	<div class="accordion-item-title" tabindex="0" role="listitem">'+
		'		<span ng-transclude="name"></span>'+
		'		<i class="fa icon"></i>'+
		'	</div>'+
		'	<div class="accordion-item-content" ng-transclude="content"></div>'+
		'</section>',
		link: function (scope, element, attrs, ctrl, transclude) {

		}


	}
}])
angular.module("Sugar").directive("accordion", function () {
	return {
		replace: true,
		transclude: true,
		restrict: 'E',
		transclude: {

		},
		scope: {

		},
		template: '<article class="accordion" ng-transclude role="list"></article>',
		link: function (scope, element, attrs, ctrl, transclude) {

		}
	}
})
angular.module("Sugar").directive("actionToolbar", ['$compile',function ($compile) {
	return {
		replace: true,
		transclude: true,
		restrict: 'E',
		transclude: {
			content:'?',
			title:'?title'
		},
		scope: {
			top: '=?',
			bottom: '=?',
			region: '@?',
			fab: '=?',
			showCloseButton: '=?',
			closeButtonAction: '&?',
			allowSelection: '=?'
		},
		link: function (scope, element, attrs, ctrl, transclude) {
			$(".floating-action").fadeOut();
			if (scope.bottom) {
				scope.region = 'bottom';
			}
			if(scope.top){
				scope.region = 'top';
			}

			//In case item selection is active
			if (scope.allowSelection) {
				element.on('click', '.action', function (e) {
					$(this).parent().find(".action").removeClass('active');
					$(this).addClass('active');
				});
			}
			
			scope.closeButton = function (e) {
				element.fadeOut();
				$(".floating-action").fadeIn();
				if (e && e.preventDefault) {
					e.preventDefault();
				}
				if (scope.closeButtonAction) {
					return scope.closeButtonAction();
				}
			}
			
			var backButtonEvent = function () {
				element.fadeOut();
				$(".floating-action").fadeIn();
				var oAction = element.find(".close-action-toolbar");
				if (oAction) {
					oAction.trigger("click");
				}
				//After the bar is fadeOut and after all other back button events are executed
				setTimeout(function () {
					Sugar.preventBackButton = false;
				}, 100);
			}
			if (scope.showCloseButton) {
				Sugar.preventBackButton = true;
				document.addEventListener('backbutton', backButtonEvent, true);
				Sugar.addBackButtonAction(backButtonEvent);
				element.append($compile('<action icon="close" class="close-action-toolbar" action="closeButton()"></action>')(scope));
			}
			scope.$on("$destroy", function () {
				$(".floating-action").fadeIn();
				document.removeEventListener('backbutton', backButtonEvent, true);
				Sugar.removeBackButtonAction(backButtonEvent);
				setTimeout(function () {
					Sugar.preventBackButton = false;
				});
			});
		},
		template: '<section class="action-toolbar {{region ? region+\'-fixed\' : \'\'}}" ng-class="{\'floating-action\':fab}" ng-transclude></section>'
	}
}])
angular.module("Sugar").directive("action", ['$location','$rootScope',function ($location,$rootScope) {
	return {
		replace: true,
		transclude: true,
		restrict: 'E',
		transclude:{
			
		},
		scope: {
			action: '&?',
			icon: '@?',
			labelKey: '@?label',
			url: '@?',
			href: '@?url',
			active: '=?'
		},
		template:''+
		'<button class="action" ng-click="action()" ng-class="{ active: active,\'only-icon\':icon && !labelKey }">'+
		'	<i ng-if="icon" class="icon material-icons">{{icon}}</i>'+
		'	<span ng-if="label">{{label}}</span>'+
		'</button>',
		link: function (scope, element, attrs, ctrl, transclude) {
			if (scope.labelKey) {
				var label = $rootScope.language.instance.labels[scope.labelKey];
				
				scope.label = label ? label : scope.labelKey;
				
			}
			if (scope.url) {
				scope.action = function () {
					$location.path(scope.url);
					scope.active = true;
				}
			}
		}
	}
}])

angular.module("Sugar").directive("appBarTabs", ["$rootScope", function ($rootScope) {
	return {
		replace: true,
		restrict: 'E',
		scope: {
			tabs: '=?'
		},
		link: function (scope, element, attrs) {
			scope.$watch('tabs.visible', function () {
				//console.log('visible changed',scope.tabs.visible);
			});

			var $activeBar = element.find(".active-bottom-bar"),
				$activeTab,
				$activeContent;

			function activate(tabId) {
				element.find(".tab").removeClass("active");
				$activeTab = element.find("[data-tab-id=" + tabId + "]");
				if (!$activeTab.length) return false;
				$activeTab.addClass("active");
				$activeBar.css("width", $activeTab.outerWidth());

				var width = $activeTab.outerWidth();
				var left = $activeTab.position().left;
				var scroll = (left + width) - window.innerWidth > 0 ? (left + width + 50) - window.innerWidth : 0;
				//element.scrollLeft(scroll);
				element.animate({ scrollLeft: scroll }, { queue: false, duration: 400 });
				Sugar.transform.translate($activeBar, left);

				$activeContent = $("[data-tab-content-id=" + tabId + "]");
				$activeContent.addClass("active").fadeIn();

				document.body.scrollTop = 0;

				//Sugar.transform.translate($activeContent, 0);
				$("[data-tab-content-id]").not($activeContent).hide().removeClass("active");

				//Added after a problem with gmaps javaScript API
				$rootScope.$broadcast('sugar:activate-tab', tabId);

				return true;
			}

			scope.activate = function (sTabId) {
				activate(sTabId);
			}
			scope.widthStyle = '';
			var activated = false;
			scope.$watch('tabs.list.length', function (a, b) {
				//if (!scope.tabs || !scope.tabs.list || scope.tabs.list.length == 0) return;

				if (scope.tabs && scope.tabs.centered) {
					scope.widthStyle = 'width:' + ((100 / scope.tabs.list.length).toFixed(2)) + '%;';
				}

				activated = false;
				for (var i = 0; i < scope.tabs.list.length; i++) {
					if (scope.tabs.list[i].active) {
						activated = activate(scope.tabs.list[i].contentId);
					}
				}
				setTimeout(function () {
					if (!activated && scope.tabs.list.length) {
						activate(scope.tabs.list[0].contentId);
					}
				}, 0);
			});
			Sugar.touch.fnQueue.swipeLeft.push(function () {
				var $tab = element.find(".tab.active").next(".tab");
				if ($tab.length == 0) return;
				activate($tab.attr("data-tab-id"));
			});
			Sugar.touch.fnQueue.swipeRight.push(function () {
				var $tab = element.find(".tab.active").prev(".tab");
				if ($tab.length == 0) return;
				activate($tab.attr("data-tab-id"));
			});
		},
		template: '' +
		'<div class="tab-scroll">' +
		'	<ul class="tabs" ng-class="{ \'visible\': tabs.visible && tabs.list.length>0, \'centered\': tabs.centered }" >' +
		'		<li ng-repeat="tab in tabs.list" class="tab" data-tab-id="{{tab.contentId}}" tabindex="0" ng-click="activate(tab.contentId)" style="{{widthStyle}}">' +
		'			<i class="icon material-icons" ng-if="tab.icon">{{tab.icon}}</i>' +
		'			<span>{{tab.label}}</span>' +
		'		</li>' +
		'		<li class="active-bottom-bar"></li>' +
		'	</ul>' +
		'</div>'
	}
}])
angular.module("Sugar").directive("appBar", [function () {
	return {
		replace: true,
		scope: {
			customText: '=?',
			mainIcon: '=?',
			mainIconAction: '&?',
			mainIconHidden:'=?',
			actions: '=?',
			hidden: '=?',
			title: '=?',
			tabs: '=?'
		},
		link: function (scope, element) {
			var height = 60,
				lastScrollTop = document.body.scrollTop;
			$(window).on('scroll', function () {
				var currentHeight = $(".app-bar").height();
				if (document.body.scrollTop > 0 && currentHeight > height && document.body.scrollTop > lastScrollTop) {
					element.addClass("almost-hidden");
					Sugar.transform.translate(element, 0, height - currentHeight);
				}
				else {
					element.removeClass("almost-hidden");
					Sugar.transform.translate(element, 0, 0);
				}
				lastScrollTop = document.body.scrollTop;
			});
			angular.element(document.querySelectorAll("#main, body")).removeClass("no-app-bar");
			scope.$watch('customText', function (a, b) {
				if (!a || !a.length) {
					$("body").removeClass("custom-header-text");
					return;
				}
				$("body").addClass("custom-header-text");
			});

			scope.$watch('tabs.list.length', function (a, b) {
				if (!a) {
					$("body").removeClass("app-bar-extended");
					return;
				}
				$("body").addClass("app-bar-extended");
			});
		},
		template: '' +
		'<header class="nav app-bar" ng-class="{\'hidden\':hidden, \'custom-header-text\': customText,\'main-icon-hidden\':mainIconHidden }">' +
		'	<button ng-if="!mainIcon && !mainIconHidden" class="action toggle-sidebar toggle-left" onclick="Sugar.sidebar.toggle()">' +
		'		<i class="material-icons icon">menu</i>' +
		'	</button>' +
		'	<button ng-if="mainIcon && !mainIconHidden" class="action " ng-click="mainIconAction()">' +
		'		<i class="material-icons icon">{{mainIcon}}</i>' +
		'	</button>' +
		'	<a class="title">{{title}}</a>' +
		'	<div class="actions" ng-if="actions.list.length>0" ng-class="{\'open\': actions.open }">' +
		'		<action ng-repeat="action in actions.list" action="action.click()" icon="{{action.icon}}" label="{{action.name}}"></action>' +
		'	</div>' +
		'	<div class="custom-header-text" ng-if="customText">{{customText}}</div>' +
		'	<app-bar-tabs tabs="tabs"></app-bar-tabs>' +
		'	<search-box ng-if="search.text || search.method" search-text="search.text" search-method="search-method"></search-box>' +
		'</header>'
	}
}])
angular.module("Sugar").directive("card", ['$rootScope', function ($rootScope) {
	return {
		replace: true,
		transclude: true,
		restrict: 'E',
		scope: {
			title: '@',
			smallTitle: '@?',
			autoSize: '=?',
			description: '@?',
			image: '@?',
			actions: '=?',
			selectable: '@?',
			icon: '@?',
			cardAction: '&?',
			type: '@?',
		},
		template: '' +
		'<section class="card {{selectable}} {{type}}"' +
		'		 ng-class="{ \'selectable\':selectable, \'auto-size\' : autoSize, \'icon-card\':icon }"' +
		'		 tabindex="0"' +
		'		 on-enter-press="!cardAction || cardAction()"' +
		'		 onkeydown="Sugar.card.events.select(this, event)">' +
		'	<div onclick="Sugar.card.events.select(this.parentNode,event)" ng-click="!cardAction || cardAction()">' +
		'		<div ng-if="title" class="card-title">{{title}}</div>' +
		'		<a ng-if="icon" class="card-icon">' +
		'			<i class="icon material-icons">{{icon}}</i>' +
		'			<label>{{icon.text}}</label>' +
		'		</a>' +
		'		<div ng-if="smallTitle" class="card-small-title">{{smallTitle}}</div>' +
		'		<div ng-transclude="" class="card-description">{{description}}</div>' +
		'		<div ng-if="image" class="card-image">' +
		'			<img src="{{image}}" />' +
		'		</div>' +
		'	</div>' +
		'	<div ng-if="actions" class="actions ">' +
		'		<action ng-if="action.click||action.url" ng-repeat="action in actions" url="{{action.url}}" action="action.click()" on-enter-press="action.click()" label="{{action.label}}"></action>' +
		'	</div>' +
		'</section>',
		link: function (scope, element, attrs, ctrl, transclude) {
			scope.language = $rootScope.language;
		}
	}
}])
angular.module("Sugar").directive("formFieldGroup", function () {
	return {
		restrict: "C",
		link: function (scope, element) {
			var input = element.find("input"),
				oInputBorderBottom = element.find(".input-border-bottom");

			input.on('focus', function () {
				var oLabel = element.find('label');
				oLabel.addClass('focused');
				oInputBorderBottom.addClass("state-focus");
			});

			input.on('blur', function () {
				var oLabel = element.find('label');
				oInputBorderBottom.removeClass("state-focus");
				if (!this.value && !oLabel.hasClass('always-focused')) {
					oLabel.removeClass('focused');
				}
			});
		}
	}
});
angular.module("Sugar").directive("formFieldBinary",['$rootScope', function ($rootScope) {
	return {
		replace: true,
		restrict: 'E',
		scope: {
			labelKey: '@?label',
			model: '=',
			required: '=?',
			disabled: '=?',
			invalidMessage: '=?',
			type: '@?',
			name: '@?'
		},
		controller: ['$scope', function ($scope) {
		}],
		link: function (scope, element) {
			scope.language = $rootScope.language;
			scope.label = '';
			if (scope.labelKey) {
				scope.label = scope.language.instance.labels[scope.labelKey];
			}
			scope.$watch('model', function () {
				if (typeof scope.model != 'boolean') {
					scope.model = scope.model==1;
				}
			});
			scope.type = scope.type ? scope.type : 'multi-select';

			element.find(".checkbox").on("click", function (e) {
				e.stopPropagation();
			})
			element.on('click keyup', '.form-field-binary', function (e) {
				e.preventDefault();
				if (e.type == 'keyup') {
					var keycode = e.keyCode || e.which || e.charCode;
					if (keycode != 32 && keycode != 13) return;
				}
				var self = angular.element(this);
				self.focus();
				self.find('.checkbox:first').trigger('click');
			});

		},
		template:''+
		'<div class="form-field-group form-field-binary">'+
		'	<div class="form-control {{type}} form-field-binary" tabindex="0" ng-class="{\'selected\': model }">'+
		'		<input type="checkbox"'+
		'			   class="checkbox"'+
		'			   ng-model="model"'+
		'			   ng-class="{\'selected\': model }"'+
		'			   name="{{::name}}"'+
		'			   id="binary-{{uid}}"'+
		'			   ng-required="required"'+
		'			   ng-disabled="disabled" />'+
		'		<label ng-if="label" for="binary-{{uid}}" onclick="return false" ng-class="{\'invalid\': form[name].$invalid && form[name].$dirty}">{{label}}</label>'+
		'		<div class="input-error-message">'+
		'			<span ng-if="form[name].$invalid && form[name].$dirty">'+
		'	{{invalidMessage}}'+
		'			</span>'+
		'		</div>'+
		'	</div>'+
		'</div>',
	}

}])
function validarCPF(str) {
	if (!str) return false;
	str = str.replace('.', '');
	str = str.replace('.', '');
	str = str.replace('-', '');
	var Soma;
	var Resto;
	Soma = 0;
	if (str == "00000000000") return false;
	for (i = 1; i <= 9; i++) {
		Soma = Soma + parseInt(str.substring(i - 1, i)) * (11 - i);
	}
	Resto = (Soma * 10) % 11;
	if ((Resto == 10) || (Resto == 11)) {
		Resto = 0;
	}
	if (Resto != parseInt(str.substring(9, 10))) return false;

	Soma = 0;
	for (i = 1; i <= 10; i++) {
		Soma = Soma + parseInt(str.substring(i - 1, i)) * (12 - i);
	}
	Resto = (Soma * 10) % 11;
	if ((Resto == 10) || (Resto == 11)) {
		Resto = 0;
	}
	if (Resto != parseInt(str.substring(10, 11))) return false;
	return true;

}
function validarCNPJ(cnpj) {

	cnpj = cnpj.replace(/[^\d]+/g, '');

	if (cnpj == '') return false;

	if (cnpj.length != 14)
		return false;

	// LINHA 10 - Elimina CNPJs invalidos conhecidos
	if (cnpj == "00000000000000" ||
        cnpj == "11111111111111" ||
        cnpj == "22222222222222" ||
        cnpj == "33333333333333" ||
        cnpj == "44444444444444" ||
        cnpj == "55555555555555" ||
        cnpj == "66666666666666" ||
        cnpj == "77777777777777" ||
        cnpj == "88888888888888" ||
        cnpj == "99999999999999")
		return false; // LINHA 21

	// Valida DVs LINHA 23 -
	tamanho = cnpj.length - 2
	numeros = cnpj.substring(0, tamanho);
	digitos = cnpj.substring(tamanho);
	soma = 0;
	pos = tamanho - 7;
	for (i = tamanho; i >= 1; i--) {
		soma += numeros.charAt(tamanho - i) * pos--;
		if (pos < 2)
			pos = 9;
	}
	resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
	if (resultado != digitos.charAt(0))
		return false;

	tamanho = tamanho + 1;
	numeros = cnpj.substring(0, tamanho);
	soma = 0;
	pos = tamanho - 7;
	for (i = tamanho; i >= 1; i--) {
		soma += numeros.charAt(tamanho - i) * pos--;
		if (pos < 2)
			pos = 9;
	}
	resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
	if (resultado != digitos.charAt(1))
		return false; // LINHA 49

	return true; // LINHA 51

}
angular.module("Sugar").directive("formFieldInput", ['$rootScope', function ($rootScope) {
	return {
		replace: true,
		restrict: 'E',
		scope: {
			labelKey: '@?label',
			placeholderKey: '@?placeholder',
			model: '=',
			required: '=?',
			disabled: '=?',
			maxlength: '=?',
			minlength: '=?',
			invalidMessage: '=?',
			size: '@?',
			type: '@?',
			prefix: '@?',
			sufix: '@?',
			name: '@?',
			form: '=?',
			id: '@?',
		},
		link: function (scope, element, attrs, controller) {
			scope.language = $rootScope.language;
			scope.label = '';
			scope.placeholder = '';

			if (scope.labelKey) {
				scope.label = scope.language.instance.labels[scope.labelKey];
			}

			if (scope.placeholderKey) {
				scope.placeholder = scope.language.instance.labels[scope.placeholderKey];
			}

			var input = element.find('input');
			scope.noPlaceholder = attrs.hasOwnProperty('noPlaceholder');
			scope.uid = scope.id || Sugar.generateGuid();
			scope.pattern;
			var type = scope.type ? scope.type : 'text';
			scope.inputType = scope.type;

			function checkInputType() {
				var type = scope.type ? scope.type : 'text';
				scope.inputType = scope.type;
				switch (type) {
					case 'text':
						scope.inputType = 'text';
						break;
					case 'time':
					case 'date':
						scope.inputType = 'text';
						break;
					case 'cpf':
						scope.inputType = 'text';
						input.on('click', function (e) {
							if (!e.ctrlKey) return;
							this.value = Sugar.valueGenerator.cpf();
							$(this).trigger('input');
						});
						break;
					case 'cnpj':
						scope.inputType = 'text';
						input.on('click', function (e) {
							if (!e.ctrlKey) return;
							this.value = Sugar.valueGenerator.cnpj();
							$(this).trigger('input');
						});
						break;
					default:
						if (scope.mask) break;
						input.on('click', function (e) {
							if (!e.ctrlKey) return;
							this.value = Sugar.valueGenerator.name();
							$(this).trigger('input');
						});
						break;
				}

			}

			checkInputType();

			scope.$watch('type', function () {
				checkInputType();
			});

			if (!scope.placeholder) {
				scope.placeholder = scope.label;
			}
			if (scope.noPlaceholder) {
				scope.placeholder = '';
			}

			element.removeAttr('label placeholder model required disabled maxlength minlength invalid-message size type mask name');
			input.removeAttr('ng-required ng-model ng-disabled ng-maxlength ng-minlength ng-pattern');


			scope.$watch('model', function () {
				var oLabel = element.find('label');
				if (scope.model) {
					oLabel.addClass('focused');
				}
				else {
					if (document.activeElement == input.get(0) || oLabel.hasClass('always-focused')) return;
					oLabel.removeClass('focused');
				}
			})
		},
		template:''+
		'<div class="form-field-input form-field-group {{size}} input-type-{{inputType}}">'+
		'	<label ng-if="label" for="input-{{uid}}" ng-class="{'+
		'				\'focused always-focused\':(type && type != \'text\' && type != \'password\') || prefix || sufix ,'+
		'				\'invalid\': form[name].$invalid && form[name].$dirty,'+
		'				\'disabled\': (disabled)'+
		'		   }">{{label}}</label>'+
		'	<!--placeholder="{{placeholder}}"-->'+
		'	<input type="{{inputType}}"'+
		'		   class="form-control {{size}}"'+
		'		   ng-model="model"'+
		'		   name="{{::name}}"'+
		'		   id="input-{{uid}}"'+
		'		   ng-required="required"'+
		'		   ng-minlength="minlength"'+
		'		   ng-maxlength="maxlength"'+
		'		   ng-disabled="disabled"'+
		'		   virtual-mask="{{type}}"'+
		'		   virtual-mask-sufix="{{sufix}}"'+
		'		   virtual-mask-prefix="{{prefix}}"'+
		'		   virtual-mask-precision="{{precision}}"'+
		'		   language="language"'+
		'		   autocapitalize="off"'+
		'		   autocorrect="off"'+
		'		   autocomplete="off"'+
		'		   spellcheck="false" />'+
		'	<div class="input-border-bottom" ng-class="{\'invalid\': form[name].$invalid && form[name].$dirty}"></div>'+
		'	<div class="input-error-message">'+
		'		<span ng-if="form[name].$invalid && form[name].$dirty">' +
		'			{{invalidMessage ? invalidMessage : language.instance.phrases[\'por-favor-verifique-campo\']}}'+
		'		</span>'+
		'	</div>'+
		'</div>'
	}
}]);

angular.module("Sugar").directive('virtualMask', ['$rootScope',function ($rootScope) {
	return {
		restrict: 'A',
		require: 'ngModel',
		scope: {
			type: '@?virtualMask',
			prefix: '@?virtualMaskPrefix',
			sufix: '@?virtualMaskSufix',
			precision: '@?virtualMaskPrecision',
			placeholder: '@?',
		},
		link: function (scope, element, attrs, controller) {
			scope.prefix = scope.prefix ? scope.prefix.trim() + ' ' : '';
			scope.sufix = scope.sufix ? ' ' + scope.sufix.trim() : '';
			scope.language = $rootScope.language;
			function formatNumeric(value, precision, config) {
				value = isNaN(parseFloat(value)) ? 0 : parseFloat(value);
				value = (number_format(value, precision, config.decimal_point, config.thousand_point));
				if (value[0] == '.') {
					value = '0' + value;
				}
				return value;
			}
			function unformatNumeric(value, precision, config) {
				//10.00
				var zeroBeforeFix = precision;
				while (zeroBeforeFix--) {
					value = '0' + value;
				}
				value = value.replace(new RegExp("\\" + config.thousand_point, 'g'), '').replace(new RegExp("\\" + config.decimal_point, 'g'), '');
				value = value.substr(0, value.length - precision) + '.' + value.substr(-precision);
				if (value[0] == '.') {
					value = '0' + value;
				}
				value = !precision ? parseInt(value) : parseFloat(value);
				return value;
			}
			var aSkipChars = [];
			function skipChar(sChar) {
				return aSkipChars.indexOf(sChar);
			}
			function mask(value) {
				if (value == undefined || value == null) {
					return undefined;
				}

				scope.prefix = scope.prefix ? scope.prefix.trim() + ' ' : '';
				scope.sufix = scope.sufix ? ' ' + scope.sufix.trim() : '';
				var precision = scope.precision ? scope.precision : 2;
				var config = scope.language.instance.defaults.number;
				var value = unmask(value);
				switch (scope.type) {
					case 'integer':
						precision = 0;
						value = formatNumeric(value, precision, config);
						break;
					case 'decimal':
						value = formatNumeric(value, precision, config);
						aSkipChars = ['.', ','];

						break;
					case 'cpf':
						value = value.toString();
						value = str_pad(value.substr(0, 3), 3, "_") +
							'.' +
							str_pad(value.substr(3, 3), 3, "_") +
							'.' +
							str_pad(value.substr(6, 3), 3, "_") +
							'-' +
							str_pad(value.substr(9, 2), 2, "_");
						aSkipChars = ['.', '-'];
						controller.$setValidity('CPF', validarCPF(value));
						break;
					case 'cnpj':
						value = value.toString();
						value = str_pad(value.substr(0, 2), 2, "_") +
							'.' +
							str_pad(value.substr(2, 3), 3, "_") +
							'.' +
							str_pad(value.substr(5, 3), 3, "_") +
							'/' +
							str_pad(value.substr(8, 4), 4, "_") +
							'-' +
							str_pad(value.substr(12, 2), 2, "_");
						aSkipChars = ['.', '-', '/'];
						controller.$setValidity('CNPJ', validarCNPJ(value));
						break;
					case 'date':
						var format = scope.language.instance.defaults.datetime.format.toLowerCase();
						var divider = scope.language.instance.defaults.datetime.placeholder.toLowerCase().replace(/(d|m|y)/gi, '')[0];
						var re = new RegExp(divider, "gi");
						var day = value.substr(8, 2);
						var month = value.substr(5, 2);
						var year = value.substr(0, 4);


						value = "";
						value = value.toString();
						value = format.replace("d", str_pad(day, 2, "_"))
									  .replace("m", str_pad(month, 2, "_"))
									  .replace("y", str_pad(year, 4, "_"));
						aSkipChars = [divider];
						break;
					case 'time':
						var second_part_index = value.indexOf(":");
						value = str_pad(value.substr(0, 2), 2, "0") + ":" + str_pad(value.substr(second_part_index + 1, 2), 2, "0");
						aSkipChars = [':'];
						break;
				}

				return scope.prefix + value + scope.sufix;
			}
			function unmask(value) {
				if (value == undefined || value == null) {
					return undefined;
				}
				scope.prefix = scope.prefix ? scope.prefix.trim() + ' ' : '';
				scope.sufix = scope.sufix ? ' ' + scope.sufix.trim() : '';
				var precision = scope.precision ? scope.precision : 2;
				var config = scope.language.instance.defaults.number;

				if (value.indexOf && value.indexOf(scope.prefix) > -1) {
					value = value.substr(scope.prefix.length);
				}
				if (value.indexOf && value.indexOf(scope.sufix) > -1) {
					value = value.substr(0, value.length - scope.sufix.length);
				}
				switch (scope.type) {
					case 'integer':
						precision = 0;
						value = unformatNumeric(value, precision, config);
						break;
					case 'decimal':
						value = unformatNumeric(value, precision, config);
						break;
					case 'cnpj':
					case 'cpf':
						value = value.replace(/(\.|\-|\/|\_)/gi, '');
						break;
					case 'date':
						var format = scope.language.instance.defaults.datetime.placeholder.toLowerCase();
						var divider = format.replace(/(d|m|y)/gi, '')[0];
						var dayIndex = format.indexOf("d");
						var monthIndex = format.indexOf("m");
						var yearIndex = format.indexOf("y");
						var re = new RegExp(divider, "gi");

						if (value.indexOf("-") != 4) {
							value = str_pad(value.substr(yearIndex, 4).replace(re, ''), 4, "_") +
								"-" +
								str_pad(value.substr(monthIndex, 2).replace(re, ''), 2, "_") +
								"-" +
								str_pad(value.substr(dayIndex, 2).replace(re, ''), 2, "_");
						}
						break;
					case 'time':
						value = value;
						break;
				}

				return value;
			}
			var last_state = false;
			var char = getCaretPosition(element.get(0));

			element.on("keydown", function (e) {
				//if (scope.type != 'time') return;
				char = element.caret();
				last_state = element.val();

				var keycode = e.which | e.keyCode | e.charCode;
				if (keycode == 8) {
					char = char - 2;
				}
				if (keycode == 46) {
					char = char - 1;
				}
			});
			element.on("paste", function (event) {
				char = char + event.originalEvent.clipboardData.getData('text').length - 1;
			});
			controller.$formatters.push(function (value) {
				switch (scope.type) {
					case 'decimal':
					case 'integer':
						//When model comes from server sometimes i will come as an integer
						//and the masking system for decimal expects a float 123.45
						//so we provide a float with .00 at the end if this is the case
						var float = parseFloat(value);
						if (float % 1 === 0 && value.toString().indexOf('.') == -1) {
							value = value.toString() + (scope.type == 'integer' ? '' : '.00');
						}
						if (value == undefined || value == null) {
							value = '0';
						}
						break;
					case 'date':
					case 'time':
					case 'cpf':
					case 'cnpj':
						if (!value) {
							value = '';
						}
				}
				var parsed = mask(value);
				controller.$viewValue = (parsed);
				controller.$render();
				return parsed;
			});
			controller.$parsers.push(function (value) {
				var parsed = mask(value);
				controller.$viewValue = (parsed);
				controller.$render();

				var position = getCaretPosition(element);
				var prefix = scope.prefix ? scope.prefix.trim() + ' ' : '';
				var sufix = scope.sufix ? ' ' + scope.sufix.trim() : '';
				var caret = controller.$viewValue.length - sufix.length;

				char = char + 1;

				if (prefix.length && char < prefix.length + 1) {
					char = prefix.length;
				}
				if (sufix.length && char > parsed.length - sufix.length) {
					char = parsed.length - sufix.length;
				}
				while (char <= parsed.length && skipChar(parsed[char]) > -1) {
					console.log(skipChar(parsed[char]) > -1, parsed[char], aSkipChars, char);
					char = char + 1;
				}

				element.caret(char);

				return unmask(value);
			});
		}
	}
}])

var lastContentLength = 0;
function fixCaretPosition(ngModel, position, element, prefix, sufix, keycode) {
	var change = false;
	var content = element.value.length;
	var currentPosition = position;
	var prefixLenth = prefix ? (prefix.trim().length + 1) : 0;
	var sufixLength = sufix ? (sufix.trim().length + 1) : 0;
	var total = content - prefixLenth - sufixLength;
	switch (keycode) {
		case 8://BACKSPACE
			if (currentPosition == (content - sufixLength - 1)) {
				position = position + 1;
				setCaretPosition(element.firstChild, position, position);
				return position;
			}
			break;
		case 37://LEFT
			currentPosition = currentPosition - 1;
			break;
		case 39://RIGHT
			currentPosition = currentPosition + 1;
			break;
		case 46://DELETE
			content = content - 1;
			break;
		default:
			if (lastContentLength - content > 1) {
				currentPosition = currentPosition - 1;
			}
			else if (lastContentLength - content < -1) {
				currentPosition = currentPosition + 1;
			}
			else {
				currentPosition = currentPosition;
			}
			break;
	}
	if (prefix) {
		if (currentPosition < (prefixLenth) && content > 0) {
			currentPosition = prefixLenth + 1;
			change = true;
		}
	}
	if (sufix) {
		if (currentPosition > (content - sufixLength) && content > 0) {
			currentPosition = content - sufixLength;
			change = true;
		}
		if (currentPosition == (content - sufixLength)) {
			currentPosition = content - sufixLength;
			change = true;
		}
	}
	if (change && currentPosition >= 0) {
		setCaretPosition(element.firstChild, currentPosition, currentPosition);
	}
	lastContentLength = content;
	position = currentPosition;
	return position;

}
function getCaretPosition(control) {
	var caretPos = 0,
	  sel, range;
	if (window.getSelection) {
		sel = window.getSelection();
		if (sel.rangeCount) {
			range = sel.getRangeAt(0);
			if (range.commonAncestorContainer.parentNode == control) {
				caretPos = range.endOffset;
			}
		}
	} else if (document.selection && document.selection.createRange) {
		range = document.selection.createRange();
		if (range.parentElement() == control) {
			var tempEl = document.createElement("span");
			control.insertBefore(tempEl, control.firstChild);
			var tempRange = range.duplicate();
			tempRange.moveToElementText(tempEl);
			tempRange.setEndPoint("EndToEnd", range);
			caretPos = tempRange.text.length;
		}
	}
	return caretPos;
}
function setCaretPosition(elem, caretPos) {
	if (elem != null) {
		if (elem.createTextRange) {
			var range = elem.createTextRange();
			range.move('character', caretPos);
			range.select();
		}
		else {
			if (elem.selectionStart) {
				elem.focus();
				elem.setSelectionRange(caretPos, caretPos);
			}
			else
				elem.focus();
		}
	}
}
var stripTags = function (input, allowed) {
	//  discuss at: http://phpjs.org/functions/strip_tags/
	allowed = (((allowed || '') + '')
	  .toLowerCase()
	  .match(/<[a-z][a-z0-9]*>/g) || [])
	  .join('');
	var tags = /<\/?([a-z][a-z0-9]*)\b[^>]*>/gi,
	  commentsAndTags = /<!--[\s\S]*?-->/gi;
	return input.replace(commentsAndTags, '').replace(tags, function ($0, $1) {
		return allowed.indexOf('<' + $1.toLowerCase() + '>') > -1 ? $0 : '';
	});
}
angular.module("Sugar").directive("formFieldSelect", ['$rootScope', function ($rootScope) {
	return {
		replace: true,
		restrict: 'E',
		scope: {
			ngModel: '=',
			options: '=',
			labelKey: '@?label',
			placeholderKey: '@?placeholder',
			required: '=?',
			disabled: '=?',
			idProperty: '@?',
			size: '@?',
			type: '@?',
			optionsLabel: '@',
			labelFromLanguage: '@?',
			searchMethod: '&?'
		},
		link: function (scope, element, attrs, ctrl, transclude) {
			element.removeAttr('ng-required ng-model ng-disabled label placeholder model required disabled options options-label id-property');
			element.find('.select-box').removeAttr('ng-required ng-model ng-disabled');
		},
		controller: ['$scope', function (scope) {
			scope.model = {
				ngModel: scope.ngModel ? scope.ngModel : null,
				selectedOptions: '',
				searchText: ''
			};

			scope.uid = Sugar.generateGuid();
			scope.language = $rootScope.language;
			scope.label = '';
			scope.placeholder = '';
			scope.labelFromLanguage = scope.labelFromLanguage == 'true';

			if (scope.labelKey) {
				scope.label = scope.language.instance.labels[scope.labelKey];
			}

			if (scope.placeholderKey) {
				scope.placeholder = scope.language.instance.labels[scope.placeholderKey];
			}

			scope.header = scope.label ? scope.label : scope.placeholder;
			scope.type = scope.type ? scope.type : 'single-select';
			scope.search = function () {
				if (!scope.searchMethod) return;
				scope.searchMethod()(scope.model.searchText, function (result) {

				});
			}

			var doNotUpdateNgModel = false;
			scope.$watch('ngModel', function () {
				if (!scope.ngModel) return;
				scope.model.ngModel = scope.ngModel;
			});

			scope.$watch('model.ngModel', function () {
				if (doNotUpdateNgModel) {
					doNotUpdateNgModel = false;
					return;
				}
				scope.ngModel = scope.model.ngModel;
			});

			scope.$watch('model.selectedOptions', function () {
				scope.selectedOptions = scope.model.selectedOptions
			});

			var labelPropertyTypes = scope.optionsLabel.split(';');
			scope.titleProperty = labelPropertyTypes[0];
			scope.subtitleProperty = labelPropertyTypes[1] ? labelPropertyTypes[1] : '';
			scope.descriptionProperty = labelPropertyTypes[2] ? labelPropertyTypes[2] : '';
		}],
		//templateUrl: settings.path.components + 'form/field/select/template.html',
		template: ' ' +
		'<div class="form-field-select form-field-group">' +
		'	<label ng-if="label" ng-class="{\'invalid\': form[name].$invalid && form[name].$dirty,\'disabled\': (disabled)}">{{label}}</label>' +
		'	<div class="select-box {{size}} form-control"' +
		'		 data-open-modal="{{uid}}"' +
		'		 ng-disabled="disabled"' +
		'		 ng-required="required"' +
		'		 tabindex="0">' +
		'		<div class="select-text ng-binding">' +
		'			<span ng-if="model.selectedOptions">' +
		'				{{model.selectedOptions}}' +
		'			</span>' +
		'			<span ng-if="!model.selectedOptions">' +
		'				{{placeholder}}' +
		'			</span>' +
		'		</div>' +
		'	</div>' +
		'	<div class="input-border-bottom" ng-class="{\'invalid\': form[name].$invalid && form[name].$dirty}"></div>' +
		'	<div class="input-error-message">' +
		'		<span ng-if="form[name].$invalid && form[name].$dirty">' +
		'			{{invalidMessage}}' +
		'		</span>' +
		'	</div>' +
		'	<modal-window content-id="{{uid}}"' +
		'				  title="header"' +
		'				  class="form-select-window"' +
		'				  search-method="search"' +
		'				  search-text="model.searchText"' +
		'				  ok-action="select"' +
		'				  ok-label="selecionar">' +
		'			<list items="options"' +
		'				  id-property="{{idProperty}}"' +
		'				  title-property="{{titleProperty}}"' +
		'				  subtitle-property="{{subtitleProperty}}"' +
		'				  description-property="{{descriptionProperty}}"' +
		'				  selectable="true"' +
		'				  selectable-type="{{type}}"' +
		'				  search-text="searchText"' +
		'				  label-from-language="{{labelFromLanguage}}"' +
		'				  ng-model="model.ngModel"' +
		'				  selected-options="model.selectedOptions">' +
		'			</list>' +
		'	</modal-window>' +
		'</div>'
	}

}])

angular.module("Sugar").directive("formFieldTextarea",['$rootScope', function ($rootScope) {
	return {
		replace: true,
		restrict: 'E',
		scope: {
			labelKey: '@?label',
			placeholderKey: '@?placeholder',
			model: '=',
			required: '=?',
			disabled: '=?',
			maxlength: '=?',
			minlength: '=?',
			invalidMessage: '=?',
			name: '@?',
			form: '=?'
		},
		controller: ['$scope', function ($scope) {
			$scope.uid = Sugar.generateGuid();
		}],
		link: function (scope, element) {
			scope.language = $rootScope.language;
			scope.label = '';
			scope.placeholder = '';
			if (scope.labelKey) {
				scope.label = scope.language.instance.labels[scope.labelKey];
			}
			if (scope.placeholderKey) {
				scope.placeholder = scope.language.instance.labels[scope.placeholderKey];
			}
			element.removeAttr('label placeholder model required disabled maxlength minlength invalid-message size type mask name');
			element.find('textarea').removeAttr('ng-required ng-model ng-disabled ng-maxlength ng-minlength ng-pattern')
		},
		template:''+
		'<div class="form-field-textarea form-field-group">'+
		'	<label ng-if="label" for="textarea-{{uid}}" ng-class="{\'invalid\': form[name].$invalid && form[name].$dirty,\'disabled\': (disabled)}">{{label}}</label>'+
		'	<textarea class="form-control {{size}}" '+
		'		   placeholder="{{placeholder}}" '+
		'		   ng-model="model" '+
		'		   name="{{::name}}"'+
		'		   id="textarea-{{uid}}"'+
		'		   ng-required="required" '+
		'		   ng-minlength="minlength" '+
		'		   ng-maxlength="maxlength" '+
		'		   ng-disabled="disabled"'+
		'		   ng-pattern="{{pattern}}"'+
		'		   >'+
		'	</textarea>'+
		'	<div class="input-border-bottom" ng-class="{\'invalid\': form[name].$invalid && form[name].$dirty}"></div>'+
		'	<div class="input-error-message">'+
		'		<span ng-if="form[name].$invalid && form[name].$dirty">'+
		'			{{invalidMessage}}'+
		'		</span>'+
		'	</div>'+
		'</div>'
	}

}])
angular.module("Sugar").directive("formFieldDatetime", ['$rootScope', '$locale', function ($rootScope, $locale) {
	return {
		replace: true,
		restrict: 'E',
		require:'ngModel',
		scope: {
			labelKey: '@?label',
			placeholderKey: '@?placeholder',
			ngModel: '=ngModel',
			required: '=?',
			disabled: '=?',
			invalidMessage: '=?',
			form: '=?',
			name: '@?',
			id: '@?',
			size: '@?',
		},
		link: function (scope, element, attrs, controller) {
			scope.oController = controller;
			scope.form = controller.$$parentForm;
			scope.dateFormat = $locale.DATETIME_FORMATS.shortDate;
			scope.timeFormat = $locale.DATETIME_FORMATS.shortTime;
			scope.label = $rootScope.language.instance.labels[scope.labelKey];
			scope.uid = Sugar.generateGuid();
			scope.dateFieldName = scope.uid + "_date";
			scope.timeFieldName = scope.uid + "_time";
			scope.date = null;
			scope.time = '';


			if (scope.ngModel) {
				scope.date = new Date(date("Y-m-d H:i:s", strtotime(scope.ngModel)));
				scope.time = scope.date;
			}

			var bPreventUpdate = false;
			scope.$watch('ngModel', function (a, b) {
				if (bPreventUpdate) {
					bPreventUpdate = false;
					return;
				}
				if (!scope.ngModel) {
					scope.date = null;
					scope.time = null;
					return;
				}
				scope.date = new Date(date("Y-m-d H:i:s", strtotime(scope.ngModel)));
				scope.time = scope.date;
			});

			scope.$watch('date', function () {
				if (!scope.date) return;
				bPreventUpdate = true;
				scope.ngModel = date("Y-m-d H:i:s", strtotime(scope.date + ""));
				scope.time = scope.date;
			});

			scope.$watch('time', function () {
				if (!scope.time) return;
				bPreventUpdate = true;
				scope.ngModel = date("Y-m-d H:i:s", strtotime(scope.date + ""));
				scope.date = scope.time;
			});
		},
		template: '' +
		'<div class="form-field-input form-field-datetime form-field-group {{size}} input-type-{{inputType}}">' +
		'	<label ng-if="label" for="input-{{uid}}" class="focused always-focused" ng-class="{' +
		'				\'invalid\': (form[timeFieldName].$invalid && form[timeFieldName].$dirty)|| (form[dateFieldName].$invalid && form[dateFieldName].$dirty),' +
		'				\'disabled\': (disabled)'+
		'		   }">{{label}}</label>' +
		'	<div class="cols with-padding">' +
		'		<div class="col cols-size-5">' +
		'			<input type="date"' +
		'				   class="form-control date {{size}}"' +
		'				   ng-model="date"' +
		'				   name="{{dateFieldName}}"' +
		'				   placeholder="{{dateFormat}}" ' +
		'				   ng-required="required"' +
		'				   ng-disabled="disabled"' +
		'				   language="language"' +
		'				   autocapitalize="off"' +
		'				   autocorrect="off"' +
		'				   autocomplete="off"' +
		'				   spellcheck="false" />' +
		'		</div>' +
		'		<div class="col cols-size-5">' +
		'			<input type="time"' +
		'				   class="form-control time{{size}}"' +
		'				   ng-model="time"' +
		'				   placeholder="{{timeFormat}}" ' +
		'				   name="{{timeFieldName}}"' +
		'				   placeholder="" ' +
		'				   ng-required="required"' +
		'				   ng-disabled="disabled"' +
		'				   language="language"' +
		'				   autocapitalize="off"' +
		'				   autocorrect="off"' +
		'				   autocomplete="off"' +
		'				   spellcheck="false" />' +
		'		</div>' +
		'	</div>' +
		'	<div class="clear"></div>' +
		'	<div class="input-border-bottom" ng-class="{\'invalid\':(form[timeFieldName].$invalid && form[timeFieldName].$dirty)|| (form[dateFieldName].$invalid && form[dateFieldName].$dirty)}"></div>' +
		'	<div class="input-error-message">' +
		'		<div ng-if="form[dateFieldName].$invalid && form[dateFieldName].$dirty">' +
		'			{{invalidMessage ? invalidMessage : ("phrases[\'data-invalida\']"|translate)}}' +
		'		</div>' +
		'		<div ng-if="form[timeFieldName].$invalid && form[timeFieldName].$dirty">' +
		'			{{invalidMessage ? invalidMessage : ("phrases[\'horario-invalido\']"|translate)}}' +
		'		</div>' +
		'		<div ng-if="(form[dateFieldName].$dirty || form[dateFieldName].$dirty) && ngModel==\'\' ">' +
		'			{{invalidMessage ? invalidMessage : ("phrases[\'por-favor-verifique-campo\']"|translate)}}' +
		'		</div>' +
		'	</div>' +
		'</div>'
	}
}]);
angular.module("Sugar").directive("formFieldCpf", ['$rootScope', '$locale', function ($rootScope, $locale) {
	return {
		replace: true,
		restrict: 'E',
		scope: {
			labelKey: '@?label',
			ngModel: '=ngModel',
			required: '=?',
			disabled: '=?',
			invalidMessage: '=?',
			form: '=?',
			id: '@?',
			size: '@?',
		},
		link: function (scope, element, attrs, controller) {
			scope.language = $rootScope.language;
			scope.label = $rootScope.language.instance.labels[scope.labelKey];
			scope.name = Sugar.generateGuid();
			scope.placeholder = '___.___.___-__';

			element.removeAttr("disabled")

		},
		template: '' +
		'<div class="form-field-input form-field-datetime form-field-group {{size}} input-type-{{inputType}}">' +
		'	<label ng-if="label" for="input-{{uid}}" class="focused always-focused" ng-class="{' +
		'				\'invalid\': form[name].$invalid && form[name].$dirty,' +
		'				\'disabled\': (disabled)'+
		'		   }">{{label}}</label>' +
		'		<input type="text"' +
		'			   class="form-control cpf {{size}}"' +
		'			   ng-model="ngModel"' +
		'			   name="{{name}}"' +
		'			   placeholder="{{placeholder}}" ' +
		'			   ng-required="required"' +
		'			   ng-disabled="disabled"' +
		'			   ui-br-cpf-mask' +
		'			   language="language"' +
		'			   autocapitalize="off"' +
		'			   autocorrect="off"' +
		'			   autocomplete="off"' +
		'			   spellcheck="false" />' +
		'	<div class="input-border-bottom" ng-class="{\'invalid\': form[name].$invalid && form[name].$dirty}"></div>' +
		'	<div class="input-error-message">' +
		'		<span ng-if="form[name].$invalid && form[name].$dirty">' +
		'			{{invalidMessage ? invalidMessage : language.instance.phrases[\'cpf-invalido\']}}' +
		'		</span>' +
		'		<span ng-if="form[name].$invalid && form[name].$dirty">' +
		'			{{invalidMessage ? invalidMessage : language.instance.phrases[\'por-favor-verifique-campo\']}}' +
		'		</span>' +
		'	</div>' +
		'</div>'
	}
}]);
angular.module("Sugar").directive("list", ['$rootScope', function ($rootScope) {
	return {
		replace: true,
		restrict: 'E',
		scope: {
			items: '=?',
			headerProperty: '@?header',
			idProperty: '@?',
			titleProperty: '@',
			subtitleProperty: '@?',
			descriptionProperty: '@?',
			searchText: '=?',
			selectable: '=?',
			selectableType: '@?',
			selectedOptions: '=?',
			labelFromLanguage: '@?',
			sharedModel: '=?',
			ngModel: '=?'
		},
		link: function (scope, element, attrs) {
			scope.titleProperty = scope.titleProperty.replace(/\'/g, '\"');
			scope.titleProperties = JSON.parse(scope.titleProperty);
			scope.searchText = !scope.searchText ? '' : scope.searchText;
			scope.selectableType = !scope.selectableType ? 'multi-select' : scope.selectableType;
			scope.selectedOptions = '';
			scope.language = $rootScope.language;
			scope.listElement = !scope.items ? 'ul' : 'div';
			scope.header = scope.language.instance.labels[scope.headerProperty];

			var presetModelValue = scope.ngModel;

			scope.config = {
				iconLeft: false,
				iconRight: false,
				image: false,
				avatar: false,
				selected: false,
			}

			if (scope.subtitleProperty) {
				scope.subtitleProperty = scope.subtitleProperty.replace(/\'/g, '\"');
				scope.subtitleProperties = JSON.parse(scope.subtitleProperty);
			}
			if (scope.descriptionProperty) {
				scope.descriptionProperty = scope.descriptionProperty.replace(/\'/g, '\"');
				scope.descriptionProperties = JSON.parse(scope.descriptionProperty);
			}
			if (scope.subtitleProperty && scope.descriptionProperty) {
				scope.lineType = 'tree-line';
			}
			else if (scope.subtitleProperty && !scope.descriptionProperty || !scope.subtitleProperty && scope.descriptionProperty) {
				scope.lineType = 'two-line';
			}
			else {
				scope.lineType = 'one-line';
			}

			scope.setProperties = function (oItem) {
				oItem.$title = '';
				oItem.$subtitle = '';
				oItem.$description = '';
				function setProperty(property, lookInto) {
					var sValue = '';
					for (var i in lookInto) {
						sValue = '';
						if (scope.labelFromLanguage == 'true') {
							sValue = scope.language.instance.labels[oItem[lookInto[i]]];
							if (!sValue && !scope.language.instance.labels.hasOwnProperty(lookInto[i])) {
								sValue = lookInto[i];
							}
							else {
								sValue += ' ';
							}
						}
						else {
							sValue = oItem[lookInto[i]];
							if (!sValue && !oItem.hasOwnProperty(lookInto[i])) {
								sValue = lookInto[i];
							}
							else {
								sValue += ' ';
							}
						}
						oItem[property] += sValue;
					};
				}
				setProperty('$title', scope.titleProperties);
				setProperty('$subtitle', scope.subtitleProperties);
				setProperty('$description', scope.descriptionProperties);
			}

			scope.$watch('items', function () {
				if (!scope.items) return;
				//scope.ngModel = null;
				scope.items.forEach(function (oItem) {
					oItem.selectable = oItem.selectable === undefined && scope.selectable !== undefined ? scope.selectable : oItem.selectable;
					scope.setProperties(oItem);
				})
				if (presetModelValue && scope.items.length) {
					setTimeout(function () {
						scope.$apply(function () {
							scope.ngModel = presetModelValue;
						});
					}, 1);
				}
				else {
					scope.select();
				}
			});

			scope.selectAll = function () {
				if (!scope.items || !scope.items.length) return;
				scope.items.forEach(function (oItem) {
					if (!oItem.selectable) return;
					oItem.selected = scope.config.selected;
				});
				scope.select();
			}

			scope.select = function (addEverything) {
				if (!scope.ngModel) {
					scope.ngModel = scope.selectableType == 'single-select' ? null : [];
				}
				var selected = scope.items.filter(function (a) { return addEverything || a.selected }),
					model = selected;

				if (selected.length && selected.length === scope.items.filter(function (item) { return item.selectable }).length) {
					scope.config.selected = true;
				}
				else {
					scope.config.selected = false;
				}

				if (scope.selectableType == 'single-select') {
					model = selected.length > 0 ? selected[0] : null;
					if (scope.idProperty) {
						model = selected.length > 0 ? selected[0][scope.idProperty] : null;
					}
				}

				if (!doNotUpdateNgModel) {
					/**
                     * ## Complex Stuff
                     * Shared Model means that the selectable-list is conected with other selectable-lists
                     * and thoses conected lists share the same model to store the selected items.
                     * So for each instance of selectable-list the items that will be taken care of selection
                     * are those who are part of the items list of the instance
                     */
					if (scope.sharedModel) {
						var index = null;
						var i = scope.ngModel.length;
						while (i--) {
							for (var a = 0; a < scope.items.length; a++) {
								//Will remove only the items that are part of this instance scope.items
								//The item inside ngModel must be the exact same inside the scope.items
								if ((scope.ngModel[i] && scope.items[a].$$hashKey != scope.ngModel[i].$$hashKey)) {
									continue;
								}
								//Breaks the loop once the item was already found inside ngModel
								//The next ngModel item will be checked now
								scope.ngModel.splice(i, 1);
								break;
							}
						}
						scope.ngModel = scope.ngModel.concat(model);
					}
					else {
						scope.ngModel = model;
					}
				}
				doNotUpdateNgModel = false;
				var selected_option = [];
				for (var i = 0; i < selected.length; i++) {
					var label = [];
					label.push(selected[i].$title);
					selected_option.push(label.join(' '))
				}
				scope.selectedOptions = selected_option.join(', ');
			}
			if (scope.items) {
				scope.select();
			}

			var doNotUpdateNgModel = false;
			scope.$watch('ngModel', function (a, b) {
				if (doNotUpdateNgModel || a == b) return;

				scope.items.forEach(function (oItem) {
					if (scope.idProperty && oItem[scope.idProperty] + "" == scope.ngModel + "") {
						oItem.selected = true;
					}
					else if (!scope.idProperty && oItem == scope.ngModel) {
						oItem.selected = true;
					}
					else {
						if (scope.selectableType == 'single-select') {
							oItem.selected = false;
						}
					}
				});
				doNotUpdateNgModel = true;
				scope.select();
			})

			//SELECTABLE LISTS
			//can be radiobuttons or checkboxes
			element.on(Sugar.events.touchstart, ".selectable .actions-icon .icon", function (e) {
				e.stopPropagation();
			})
			element.on("click keydown", "li.selectable input[type=checkbox]", function (e) {
				e.stopPropagation();
			});
			element.on("click keydown", "li.selectable", function (e) {
				if (e.type == 'keydown') {
					var keycode = e.keyCode || e.which || e.charCode;
					if (keycode != 32 && keycode != 13) return;
				}
				e.preventDefault();
				e.stopPropagation();

				var $self = angular.element(this),
					$checkbox = $self.find("input[type=checkbox]");

				$checkbox.trigger('click');
				if (!$checkbox.is(':checked')) {
					return;
				}

				if ($self.hasClass('single-select')) {
					$self.parent('ul.list').find('li.selectable input[type=checkbox]').not($checkbox.get(0)).each(function () {
						if (!this.checked) return;
						$(this).trigger('click');
					});
				}
			});
		},
		template: '' +
		'<ul class="list">' +
		'	<li tabindex="0"' +
		'		class="header {{selectableType}} "' +
		'		ng-if="header"' +
		'		ng-click="selectAll()"' +
		'		ng-class="{ \'selected\': config.selected,' +
		'					\'two-line\': !selectable,' +
		'					\'tree-line\': selectable,' +
		'					\'selectable\': selectable }">' +
		'		<div>' +
		'			<div class="title">' +
		'				{{header}}' +
		'			</div>' +
		'			<div class="subtitle">' +
		'				<span>{{language.instance.labels.total}} <b>{{items.length}}</b></span>' +
		'			</div>' +
		'			<div class="description" ng-if="selectable">' +
		'				<span>{{language.instance.labels.selecionado}} <b>{{(items| filter: {selected:true}).length}}</b></span>' +
		'			</div>' +
		'		</div>' +
		'		<input ng-if="selectable" type="checkbox" ng-model="config.selected" ng-checked="config.selected" ng-class="{\'selected\': config.selected }" />' +
		'		<label ng-if="selectable" class="selectable-icon"></label>' +
		'	</li>' +
		'	<li ng-repeat="item in items | filter:searchText" tabindex="0"' +
		'		class="{{selectableType}} {{lineType}}"' +
		'		ng-disabled="item.disabled"' +
		'		ng-class="{ \'icon-left\': item.icon && !item.image && !item.avatar && !selectable && !item.iconRight,' +
		'					\'icon-right\': item.icon && (item.image || item.avatar || selectable || item.iconRight),' +
		'					\'with-image\': item.image,' +
		'					\'with-avatar\': item.avatar,' +
		'					\'line-through\': item.lineThrough,' +
		'					\'line-error\': item.lineError,' +
		'					\'selected\':item.selected,' +
		'					\'selectable\':selectable && (item.selectable===undefined || item.selectable )}"' +
		'		ng-click="select()">' +
		'		<div class="avatar" ng-if="item.avatar">' +
		'			<img src="{{item.avatar}}" />' +
		'		</div>' +
		'		<div class="image" ng-if="item.image">' +
		'			<img src="{{item.image}}" />' +
		'		</div>' +
		'		<div class="floating-icon" ' +
		'			 ng-class="{ \'icon-left\': item.icon && !item.image && !item.avatar && !selectable && !item.iconRight,' +
		'						 \'icon-right\': item.icon && (item.image || item.avatar || selectable || item.iconRight)}"' +
		'			 ng-if="item.icon" ng-click="item.iconAction(item)" data-open-modal="{{item.iconModal}}">' +
		'			<i class="icon fa fa-{{item.icon}} {{item.iconStatusClass}}"></i>' +
		'		</div>' +
		'		<div class="item-content">' +
		'			<div class="title">' +
		'				<span>' +
		'					{{item.$title}}' +
		'				</span>' +
		'			</div>' +
		'			<div class="subtitle" ng-if="subtitleProperties">' +
		'				<span>' +
		'					{{item.$subtitle}}' +
		'				</span>' +
		'			</div>' +
		'			<div class="description" ng-if="descriptionProperties">' +
		'				<span>' +
		'					{{item.$description}}' +
		'				</span>' +
		'			</div>' +
		'		</div>' +
		'		<input ng-if="selectable && (item.selectable===undefined || item.selectable )" type="checkbox" ng-model="item.selected" ng-checked="item.selected" ng-class="{\'selected\': item.selected }" />' +
		'		<label ng-if="selectable && (item.selectable===undefined || item.selectable )" class="selectable-icon"></label>' +
		'	</li>' +
		'</ul>'
	}
}])
angular.module("Sugar").directive("simpleList", ['$rootScope', function ($rootScope) {
	return {
		replace: true,
		restrict: 'E',
		scope: {
		},
		transclude: true,
		link: function (scope, element, attrs) {
		},
		template: '' +
		'<div>' +
		'	<ul class="list" ng-transclude>' +
		'	</ul>' +
		'</div>'
	}
}])
angular.module("Sugar").directive("simpleListItem", ['$rootScope', function ($rootScope) {
	return {
		replace: true,
		restrict: 'E',
		scope: {
			disabled: '=?',
			iconRight: '@?',
			iconLeft: '@?',
			icon: '@?',
			iconModal: '@?',
			iconPrefixTitle: '@?',
			iconSufixTitle: '@?',
			iconAction: '&?',
			iconLeftAction: '&?',
			iconRightAction: '&?',
			title: '@?',
			subtitle: '@?',
			description: '@?',
			iconStatusClass: '@?',
			iconLeftStatusClass: '@?',
			iconRightStatusClass: '@?',
			iconPrefixStatusClass: '@?',
			iconSufixStatusClass: '@?',
			lineThrough: '=?',
			lineError: '=?',
			avatar: '@?',
			image: '@?',
			selectableType: '@?',
			selectable: '=?',
			selected: '=?',
			selectAll: '=?',
			selectDisabled: '=?',
			type: '@?',
			click: '&?',
			holdon: '&?',
			list: '=?',
		},
		transclude: true,
		link: function (scope, element, attrs) {
			scope.selectableType = !scope.selectableType ? 'single-select' : scope.selectableType;
			scope.clickAction = function () {
				if (scope.click) {
					return scope.click();
				}
				if (scope.selectDisabled || !scope.selectable) return;

				var bSelected = scope.selected;
				if (scope.selectableType == 'single-select' && scope.list) {
					scope.list.forEach(function (oItem) {
						oItem.selected = false;
					});
				}
				scope.selected = !bSelected;

				if (scope.selectAll && scope.selectAll.length) {
					scope.selectAll.forEach(function (oItem) {
						oItem.selected = scope.selected;
					})
				}
			}
			scope.lineType = 'one-line';
			if (scope.subtitle && !scope.description) {
				scope.lineType = 'two-line';
			}
			if (scope.subtitle && scope.description) {
				scope.lineType = 'tree-line';
			}
			scope.$watch(function () {
				element.removeAttr('ng-class ng-disabled ng-repeat ng-click iconRight iconLeft icon iconModel iconAction iconPrefixTitle iconSufixTitle title subtitle description iconStatusClass lineThrough lineError avatar image selectableType selectable selected type click list')
			});

			scope.$watch('iconLeftAction', function () {
				if (!scope.iconLeftAction && scope.iconAction && scope.iconLeft) {
					scope.iconLeftAction = scope.iconAction;
				}
			});
			scope.$watch('iconRightAction', function () {
				if (!scope.iconRightAction && scope.iconAction && scope.iconRight) {
					scope.iconRightAction = scope.iconAction;
				}
			});
			scope.$watch('selected', function () {
				if (scope.selectDisabled && scope.selected) {
					scope.selected = false;
				}
			});
		},
		template: '' +
		'	<li tabindex="0"' +
		'		class="{{selectableType}} {{lineType}} {{type}}"' +
		'		ng-disabled="disabled"' +
		'		ng-class="{ \'icon-left\': (icon && !image && !avatar && !selectable && !iconRight)|| iconLeft,' +
		'					\'icon-right\': (icon && (image || avatar || selectable)) || iconRight,' +
		'					\'with-image\': image,' +
		'					\'with-avatar\': avatar,' +
		'					\'line-through\': lineThrough,' +
		'					\'line-error\': lineError,' +
		'					\'selected\': selected,' +
		'					\'selectable\':selectable && (selectable===undefined || selectable )}"' +
		'		ng-click="clickAction()">' +
		'		<div class="avatar" ng-if="avatar">' +
		'			<img src="{{avatar}}" />' +
		'		</div>' +
		'		<div class="image" ng-if="image">' +
		'			<img src="{{image}}" />' +
		'		</div>' +
		'		<div class="floating-icon" ng-if="icon" ng-click="iconAction()" data-open-modal="{{iconModal}}">' +
		'			<i class="icon {{iconStatusClass}} material-icons">{{icon}}</i>' +
		'		</div>' +
		'		<div class="floating-icon icon-right" ng-if="iconRight" ng-click="iconRightAction()" data-open-modal="{{iconModal}}">' +
		'			<i class="icon {{iconStatusClass}} {{iconRightStatusClass}} material-icons">{{iconRight}}</i>' +
		'		</div>' +
		'		<div class="floating-icon icon-left" ng-if="iconLeft" ng-click="iconLeftAction()" data-open-modal="{{iconModal}}">' +
		'			<i class="icon {{iconStatusClass}} {{iconLeftStatusClass}} material-icons">{{iconLeft}}</i>' +
		'		</div>' +
		'		<div class="item-content">' +
		'			<div class="title">' +
		'				<i class="icon {{iconPrefixStatusClass}} icon-prefix-title material-icons" ng-if="iconPrefixTitle">{{iconPrefixTitle}}</i>' +
		'				<span ng-class="{\'iconized\':iconPrefixTitle || iconSufixTitle}"> ' +
		'					{{title}}' +
		'				</span>' +
		'				<i class="icon {{iconSufixStatusClass}} icon-sufix-title material-icons" ng-if="iconSufixTitle">{{iconSufixTitle}}</i>' +
		'			</div>' +
		'			<div class="subtitle" ng-if="subtitle">' +
		'				<span>' +
		'					{{subtitle}}' +
		'				</span>' +
		'			</div>' +
		'			<div class="description" ng-if="description|| type==\'full-description\'">' +
		'				<span>' +
		'					{{description}}' +
		'				</span>' +
		'				<span ng-transclude></span>' +
		'			</div>' +
		'		</div>' +
		'		<input ng-disabled="selectDisabled" ng-if="selectable && (selectable===undefined || selectable )" type="checkbox" ng-model="selected" ng-checked="selected" ng-class="{\'selected\': selected }" />' +
		'		<label ng-disabled="selectDisabled" ng-if="selectable && (selectable===undefined || selectable )" class="selectable-icon"></label>' +
		'	</li>'
	}
}])
angular.module("Sugar").directive("infoPanel", [function () {
	return {
		replace: true,
		restrict: 'E',
		scope: {
			tabs: '=?'
		},
		transclude: true,
		link: function (scope, element, attrs) {
			var $tabs = element.find(".tab-scroll");
			scope.fixed = false;
			$(window).on('scroll', function () {
				if (!element.find(".tabs .tab").size()) return;
				var outerHeight = element.outerHeight();

				if (outerHeight - document.body.scrollTop <= $(".app-bar").outerHeight()) {
					scope.$apply(function () { scope.fixed = true; });
				}
				else {
					scope.$apply(function () { scope.fixed = false; });
				}
			});
			scope.$watch('tabs.list.length', function (a, b) {
				setTimeout(function () {
					outerHeight = element.outerHeight();
				}, 0);
			});

			if (!scope.tabs) {
				scope.tabs = { list: [] };
			}
		},
		template: '' +
		'<div class="info-panel" ng-class="{ \'tabs-fixed\' : fixed }" >' +
		'	<div class="content" ng-transclude></div>' +
		'	<app-bar-tabs tabs="tabs" ></app-bar-tabs>' +
		'</div>'
	}
}])
angular.module("Sugar").directive("modalWindow", ['$rootScope', function ($rootScope) {
	return {
		replace: true,
		transclude: true,
		scope: {
			contentId: '@',
			title: '=',
			okAction: '&?',
			okLabelKey: '@?okLabel',
			cancelAction: '&?',
			cancelLabel: '=?',
			searchText: '=?',
			searchMethod: '&?',
			filterSearch: '=?',
			keepNesting: '=?'
		},
		template: '' +
		'<div class="modal" id="{{contentId}}">' +
		'	<div class="modal-box" data-modal-id="{{contentId}}">' +
		'		<div class="modal-title" tabindex="0" role="dialog" data-modal-id="{{contentId}}">' +
		'			{{title}}' +
		'			<div class="modal-close" data-close-modal="{{contentId}}" tabindex="0"></div>' +
		'		</div>' +
		'		<search-box ng-if="filterSearch" search-text="model.searchText" search-method="search"></search-box>' +
		'		<div class="modal-content" ng-transclude>' +
		'			teste' +
		'		</div>' +
		'		<div class="modal-actions right" ng-if="okAction || cancelAction">' +
		'			<button tabindex="0" ng-if="okAction" type="button" class="" data-close-modal="{{contentId}}" ng-click="okAction()">{{okLabel}}</button>' +
		'			<button tabindex="0" ng-if="cancelAction" type="button" class="" data-close-modal="{{contentId}}" ng-click="cancelAction()">{{cancelLabel}}</button>' +
		'		</div>' +
		'	</div>' +
		'	<div class="modal-overlay" data-modal-id="{{contentId}}"></div>' +
		'</div>',
		link: function (scope, element, attr) {
			if (!scope.keepNesting) {
				//To prevent nested Modal Windows
				$("#application-view").append(element);
			}

			scope.language = $rootScope.language;
			scope.okLabelKey = scope.okLabelKey ? scope.okLabelKey : 'ok';
			scope.okLabel = scope.language.instance.labels[scope.okLabelKey];
			scope.cancelLabelKey = scope.cancelLabelKey ? scope.okLabelKey : 'cancelar';
			scope.cancelLabel = scope.language.instance.labels[scope.cancelLabelKey];

			scope.model = {
				searchText: scope.searchText
			}
			scope.filterSearch = scope.searchMethod ? true : scope.filterSearch;

			scope.$watch('model.searchText', function () {
				scope.searchText = scope.model.searchText;
			})
			scope.search = function () {
				scope.searchMethod()(scope.model.searchText);
			}
		}
	}

}]);

angular.module("Sugar").directive("openModal", ['$rootScope', function ($rootScope) {
	return {
		link: function (scope, element) {
			element.on(Sugar.events.click + ' keyup', function (e) {
				if (e.type == 'keyup') {
					var keycode = e.which || e.keyCode || e.charCode;
					if (keycode != 13 && keycode != 32) return;
				}
				var id = this.getAttribute("data-open-modal"),
					$modal = angular.element(document.getElementById(id));
				if (!$modal.length) return;
				$modal.show().addClass("open").css("z-index",$(".modal.open").size()+6001);
				$modal.find('.modal-box[data-modal-id=' + id + ']').animateCss('zoomIn');
				$modal.find('.modal-overlay[data-modal-id=' + id + ']').removeClass('fade-out').addClass('fade-in');
				$modal.find('.modal-title[data-modal-id=' + id + ']').focus();
			});
		}
	}
}]);

angular.module("Sugar").directive("closeModal", ['$rootScope', function ($rootScope) {
	return {
		link: function (scope, element) {
			element.on(Sugar.events.click + ' keyup', function (e) {
				if (e.type == 'keyup') {
					var keycode = e.which || e.keyCode || e.charCode;
					if (keycode != 13 && keycode != 32) return;
				}

				var id = this.getAttribute("data-close-modal"),
					$modal = angular.element(document.getElementById(id));
				$modal.find('.modal-box[data-modal-id=' + id + ']').animateCss("zoomOut", function () {
					$modal.hide().removeClass("open");
				});
				$modal.find('.overlay[data-modal-id=' + id + ']').removeClass('fade-in').addClass('fade-out');
			});
		}
	}
}]);
angular.module("Sugar").directive("searchBox", ['$rootScope', function ($rootScope) {
	return {
		replace: true,
		restrict: 'E',
		scope: {
			searchMethod: '&',
			placeholder: '@?',
			searchText: '=',
			fixed: '=?'
		},
		template: '' +
		'<div class="search-box">' +
		'	<button class="icon material-icons toggle-menu" onclick="Sugar.sidebar.toggle()">menu</button>' +
		'	<input type="text" placeholder="{{searchPlaceholder}}" ng-model="searchText" />' +
		'	<button class="search-button" ng-click="search()">' +
		'		<i class="icon material-icons">search</i>' +
		'	</button>' +
		'</div>',
		link: function (scope, element) {
			scope.language = $rootScope.language;
			scope.model = {
				searchText: (scope.searchText)
			}
			scope.searchPlaceholder = scope.placeholder ? scope.language.instance.labels[scope.placeholder] : scope.language.instance.labels.procurar;
			scope.$watch('model.searchText', function () {
				scope.searchText = scope.model.searchText;
			});

			scope.search = function () {
				if (!scope.searchMethod || typeof scope.searchMethod() != "function") return;
				scope.searchMethod()();
			}

			if (scope.fixed) {
				element.addClass("fixed");
				setTimeout(function(){
					$rootScope.header.hide();
				},1)
			}
		}
	}

}])
angular.module("Sugar").directive("ratingStars", [function () {
	return {
		replace: true,
		transclude: true,
		restrict: 'E',
		scope: {
			rating: '@?',
			max: '@?'
		},
		link: function (scope, element, attr) {
			scope.fullStars = [];
			scope.halfStars = [];
			scope.emptyStars = [];

			scope.max = scope.max ? scope.max : 5;

			function defineRatings() {
				scope.fullStars = [];
				scope.halfStars = [];
				scope.emptyStars = [];

				for (var i = 0; i < scope.max; i++) {
					if (i < scope.rating && i+1<=scope.rating) {
						scope.fullStars.push(i);						
					}
					if (i < scope.rating && i+1>scope.rating) {
						scope.halfStars.push(i);
					}
					if (i >= scope.rating) {
						scope.emptyStars.push(i);
					}
				}
			}

			scope.$watch('rating', function (a, b) {
				defineRatings();
			})
		},
		template: '<div class="rating">' +
		'	<i class="icon material-icons full-star" ng-repeat="star in fullStars">star</i>' +
		'	<i class="icon material-icons half-star" ng-repeat="star in halfStars">star_half</i>' +
		'	<i class="icon material-icons empty-star" ng-repeat="star in emptyStars">star_border</i>' +
		'	<span class="value">{{rating}}</span>' +
		'</div>'
	}
}]);
angular.module("Sugar").directive("formInput", [function () {
	return inputBase();
}]);
angular.module("Sugar").directive("formFieldCnpj", [function() {
	return new inputBase(['ui-br-cnpj-mask']);
}]);
angular.module("Sugar").directive("formSelect", [function () {
	return {
		restrict: 'E',
		replace: true,
		require: 'ngModel',
		scope: {
			label: '@?',
			ngModel: '=',
			name: '@?',
			disabled: '=?',
			type: '@?',
			required: '=?',
			minlength: '=?',
			maxlength: '=?',
			invalidMessage: '@?'
		},
		link: function (scope, element, attr, controller) {
			scope.oController = controller;
			scope.name = scope.name ? scope.name : Sugar.generateGuid();
			element.removeAttr('disabled ng-model type required minlength maxlength name label')
		},
		template:''+
		'<div class="form-field-select form-field-group {{size}}">' +
		'	<label ng-if="label" for="input-{{uid}}" ng-class="{' +
		'				\'disabled\': (disabled),'+
		'				\'focused always-focused\':(type && type != \'text\' && type != \'password\') || prefix || sufix || (disabled && ngModel),' +
		'				\'invalid\':oController.$$parentForm[name].$invalid && oController.$$parentForm[name].$dirty' +
		'		   }">{{label}}</label>' +
		'	<label ng-if="label" ng-class="{\'invalid\': form[name].$invalid && form[name].$dirty,\'disabled\': (disabled)}">{{label}}</label>' +
		'	<div class="select-box {{size}} form-control"' +
		'		 data-open-modal="{{uid}}"' +
		'		 ng-disabled="disabled"' +
		'		 ng-required="required"' +
		'		 tabindex="0">' +
		'		<div class="select-text ng-binding">' +
		'			<span ng-if="model.selectedOptions">' +
		'				{{model.selectedOptions}}' +
		'			</span>' +
		'			<span ng-if="!model.selectedOptions">' +
		'				{{placeholder}}' +
		'			</span>' +
		'		</div>' +
		'	</div>' +
		'	<div class="input-border-bottom" ng-class="{\'invalid\': form[name].$invalid && form[name].$dirty}"></div>' +
		'	<div class="input-error-message">' +
		'		<span ng-if="form[name].$invalid && form[name].$dirty">' +
		'			{{invalidMessage}}' +
		'		</span>' +
		'	</div>' +
		'	<input type="{{type}}"' +
		'		   class="form-control {{size}}"' +
		'		   ng-model="ngModel"' +
		'		   name="{{::name}}"' +
		'		   ng-required="required"' +
		'		   ng-minlength="minlength"' +
		'		   ng-maxlength="maxlength"' +
		'		   ng-disabled="disabled"' +
		'		   autocapitalize="off"' +
		'		   autocorrect="off"' +
		'		   autocomplete="off"' +
		'		   spellcheck="false" />' +
		'	<div class="input-border-bottom" ng-class="{\'invalid\': oController.$$parentForm[name].$invalid && oController.$$parentForm[name].$dirty}"></div>' +
		'	<div class="input-error-message">' +
		'		<span ng-if="oController.$$parentForm[name].$invalid && oController.$$parentForm[name].$dirty">' +
		'			{{invalidMessage ? invalidMessage : language.instance.phrases[\'por-favor-verifique-campo\']}}' +
		'		</span>' +
		'	</div>' +
		'</div>'

	}
}]);
angular.module("Sugar").directive("progressBar", ['$rootScope', function ($rootScope) {
	return {
		replace: true,
		restrict: 'E',
		scope: {
			percent: '=',
			expectedPercent: '=?'
		},
		transclude: true,
		link: function (scope, element, attrs) {
			scope.expectedPercent = scope.expectedPercent ? scope.expectedPercent : 100;
		},
		template: '' +
		'	<div class="progress-bar">' +
		'		<div class="current" style="width:{{percent}}%"></div>' +
		'	</div>'
	}
}])