﻿angular.module("Sugar").directive("formFieldGroup", function () {
	return {
		restrict: "C",
		link: function (scope, element) {
			var input = element.find("input"),
				oInputBorderBottom = element.find(".input-border-bottom");

			input.on('focus', function () {
				var oLabel = element.find('label');
				oLabel.addClass('focused');
				oInputBorderBottom.addClass("state-focus");
			});

			input.on('blur', function () {
				var oLabel = element.find('label');
				oInputBorderBottom.removeClass("state-focus");
				if (!this.value && !oLabel.hasClass('always-focused')) {
					oLabel.removeClass('focused');
				}
			});
		}
	}
});