﻿angular.module("Sugar").directive("accordionItem", ['$compile', function ($compile) {
	return {
		replace: true,
		restrict: 'E',
		transclude: {
			name: 'name',
			content: 'content',
		},
		scope: {
			active:'=?',
			withPadding:'=?'
		},
		template:''+
		'<section class="accordion-item" ng-class="{\'with-padding\':withPadding, \'active\':active}">' +
		'	<div class="accordion-item-title" tabindex="0" role="listitem">'+
		'		<span ng-transclude="name"></span>'+
		'		<i class="fa icon"></i>'+
		'	</div>'+
		'	<div class="accordion-item-content" ng-transclude="content"></div>'+
		'</section>',
		link: function (scope, element, attrs, ctrl, transclude) {

		}


	}
}])