﻿angular.module("Sugar").directive("appBar", [function () {
	return {
		replace: true,
		scope: {
			customText: '=?',
			mainIcon: '=?',
			mainIconAction: '&?',
			mainIconHidden:'=?',
			actions: '=?',
			hidden: '=?',
			title: '=?',
			tabs: '=?'
		},
		link: function (scope, element) {
			var height = 60,
				lastScrollTop = document.body.scrollTop;
			$(window).on('scroll', function () {
				var currentHeight = $(".app-bar").height();
				if (document.body.scrollTop > 0 && currentHeight > height && document.body.scrollTop > lastScrollTop) {
					element.addClass("almost-hidden");
					Sugar.transform.translate(element, 0, height - currentHeight);
				}
				else {
					element.removeClass("almost-hidden");
					Sugar.transform.translate(element, 0, 0);
				}
				lastScrollTop = document.body.scrollTop;
			});
			angular.element(document.querySelectorAll("#main, body")).removeClass("no-app-bar");
			scope.$watch('customText', function (a, b) {
				if (!a || !a.length) {
					$("body").removeClass("custom-header-text");
					return;
				}
				$("body").addClass("custom-header-text");
			});

			scope.$watch('tabs.list.length', function (a, b) {
				if (!a) {
					$("body").removeClass("app-bar-extended");
					return;
				}
				$("body").addClass("app-bar-extended");
			});
		},
		template: '' +
		'<header class="nav app-bar" ng-class="{\'hidden\':hidden, \'custom-header-text\': customText,\'main-icon-hidden\':mainIconHidden }">' +
		'	<button ng-if="!mainIcon && !mainIconHidden" class="action toggle-sidebar toggle-left" onclick="Sugar.sidebar.toggle()">' +
		'		<i class="material-icons icon">menu</i>' +
		'	</button>' +
		'	<button ng-if="mainIcon && !mainIconHidden" class="action " ng-click="mainIconAction()">' +
		'		<i class="material-icons icon">{{mainIcon}}</i>' +
		'	</button>' +
		'	<a class="title">{{title}}</a>' +
		'	<div class="actions" ng-if="actions.list.length>0" ng-class="{\'open\': actions.open }">' +
		'		<action ng-repeat="action in actions.list" action="action.click()" icon="{{action.icon}}" label="{{action.name}}"></action>' +
		'	</div>' +
		'	<div class="custom-header-text" ng-if="customText">{{customText}}</div>' +
		'	<app-bar-tabs tabs="tabs"></app-bar-tabs>' +
		'	<search-box ng-if="search.text || search.method" search-text="search.text" search-method="search-method"></search-box>' +
		'</header>'
	}
}])