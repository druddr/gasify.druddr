﻿angular.module('Axis-Router', ['Axis', 'Axis-Dependency-Injection']);
angular.module("Axis-Router").config([
	function () {
		angular.module("Axis-Router").Router = {
			getRoute: function (location) {
				location = location.replace("#/" + settings.path.app + "/", "");
				location = location.replace("/" + settings.path.app + "/", "");
				location = location[location.length - 1] == '/' ? location : location + '/';
				var route = {
					uri_parts: location.split("/"),
					//Defines the index of controller within all the uri parts
					controller_index: null,
					//Used to create CamelCase name
					controller_parts: null,
					//Name of the controller class object
					controller_name: '',
					//Controller might be under some module folder
					controller_path: '',
					//Name of the method which will be the real controller. Must be controller_class method
					action_name: '',
					//User to create pascalCase of action name
					action_name_parts: null,
					//Url broken into pieces by : separator
					params_parts: {},
					//Params index on the uri parts
					params_index: 0,
					//Params passed thru url
					params: {},
					//Route path without parameters
					path: '',
					//Array containing the files of dependencies not loaded. Will warn if some dependencie could not be loaded
					loader_dependencie_error: [],
					//View path
					view_path: '',
					a: -1
				}
				for (i in route.uri_parts) {
					route.params_parts = route.uri_parts[i].split('=');
					if (route.params_parts.length == 1) continue;
					route.params_index = route.params_index == 0 ? parseInt(i) : route.params_index;
					try {
						route.params[route.params_parts[0]] = JSON.parse(decodeURI(route.params_parts[1]));
					}
					catch (e) {
						route.params[route.params_parts[0]] = (decodeURI(route.params_parts[1]));
					}
				}
				route.params_index = route.params_index == 0 ? route.uri_parts.length - 1 : route.params_index;
				route.controller_index = route.params_index - 2;

				//This iterate uri_parts to find controller, action and possible parameters
				for (i in route.uri_parts) {
					if (route.uri_parts[i] == '') continue;

					if (i == route.controller_index + 1) {
						route.action_name = route.uri_parts[i];
					}
					if (i == route.controller_index && route.uri_parts[i - 1] != undefined) {
						route.controller_path += route.uri_parts[i - 1] + '/';
					}
				}

				route.path = '/' + settings.path.app + "/" + route.controller_path + route.uri_parts[route.controller_index] + '/' + route.action_name;

				route.controller_parts = route.uri_parts[route.controller_index].split("-");
				//Create the CamelCase controller name based on - separators
				for (var i = 0; i < route.controller_parts.length; i++) {
					route.controller_name += route.controller_parts[i].upperCaseFirst();
				}
				return route
			},
			resolve: ['$q', '$route', '$rootScope', '$injector', '$timeout', 'Component', function ($q, $route, $rootScope, $injector, $timeout, Component) {
				$rootScope.$broadcast("axis:resolve:default_components:load");

				//Loads default components
				Component.load(settings.components.default_components, function () {
					$rootScope.defaultComponentsLoaded = true;
				})

				$rootScope.$broadcast("axis:resolve:start");

				var route = angular.module('Axis-Router').Router.getRoute(window.location.hash),
					//Queue object
					deferred = $q.defer(),
					//Interval object for Loader.load.js events
					loader_interval = null,
					//Controller class object itself
					controller_class = null,
					//Controller method found matching the action. This will be Angular Controller
					controller_instance = null,
					//Correct path to access route object
					route_path = '/app/:name*';


				$rootScope.$broadcast("axis:resolve:unload_css:start");
				Loader.unload.byClass("dynamic-generated-css");
				$rootScope.$broadcast("axis:resolve:unload_css:finish");

				Loader.load.js(settings.path.controllers + route.controller_path + route.controller_name, function () {
					$rootScope.$broadcast("axis:resolve:controller_loading:start");

					controller_class = window[route.controller_name];
					if (!controller_class) {
						throw "Controller class was suposed to be '" + route.controller_name + "' but wasn\'t found";
					}
					//set the params property to give controller access to url given parameters
					controller_class.params = route.params;

					//Get controller method definition
					controller_instance = controller_class[route.action_name];

					//$route.routes[route_path].templateUrl = settings.path.views + controller_path + controller_name + '/' + action_name + '.html';
					//$route.routes[route_path].template = 'TESTE';
					if (!controller_instance && route.action_name) {
						//creating pascalCase for the action name if the dashed wasn't found
						route.action_name_parts = route.action_name.split('-');
						route.action_name = route.action_name_parts[0];
						for (var i = 1; i < route.action_name_parts.length; i++) {
							route.action_name += route.action_name_parts[i].upperCaseFirst();
						}
						controller_instance = controller_class[route.action_name];
					}

					//Sets Controller function declaration to the route object
					$route.routes[route_path].controller = controller_instance;

					var loaded = {
						js: (controller_class.js && controller_class.js.length > 0) ? controller_class.js.length : 0,
						css: (controller_class.css && controller_class.css.length > 0) ? controller_class.css.length : 0,
						models: (controller_class.models && controller_class.models.length > 0) ? controller_class.models.length : 0,
						services: (controller_class.services && controller_class.services.length > 0) ? controller_class.services.length : 0,
						directives: (controller_class.directives && controller_class.directives.length > 0) ? controller_class.directives.length : 0,
						components: (controller_class.components && controller_class.components.length > 0) ? controller_class.components.length : 0
					}

					$rootScope.$broadcast("axis:resolve:custom_css_loading:start");
					//Loading CSS
					if (controller_class.css && controller_class.css.length > 0) {
						for (var i in controller_class.css) {
							Loader.load.css(settings.path.css + controller_class.css[i].replace(".css", "") + ".css", "dynamic-generated-css");
							loaded.css--;
						}
					}
					function loadDependencyFile(dependency, type) {
						var dependencyFileName = settings.path[type] + dependency.replace(".js", "") + ".js";
						if (dependency.indexOf("http://") == 0 || dependency.indexOf("https://") == 0) {
							dependencyFileName = dependency;
						}
						Loader.load.js(dependencyFileName, function () {
							loaded[type]--;
							//if (type == 'models') {
							//	window[dependency].fill = function (properties) {
							//		for(var i in argument)
							//	};
							//}
						}, function () {
							route.loader_dependencie_error.push(dependency + ' on path ' + settings.path[type] + dependency.replace(".js", ""));
							console.error("Router could not find file from " + type + ":\r\n" + dependency);
						});
					}

					function loadDependency(type) {
						if (controller_class[type] && controller_class[type].length > 0) {
							for (var i in controller_class[type]) {
								loadDependencyFile(controller_class[type][i], type);
							}
						}
					}

					//Loading JS
					$rootScope.$broadcast("axis:resolve:custom_javascript_loading:start");
					loadDependency('js');

					//Loading services
					$rootScope.$broadcast("axis:resolve:services_loading:start");
					loadDependency('services');

					//Loading models
					$rootScope.$broadcast("axis:resolve:models_loading:start");
					loadDependency('models');

					//Loading directives
					$rootScope.$broadcast("axis:resolve:directives_loading:start");
					loadDependency('directives');

					//Loading components
					$rootScope.$broadcast("axis:resolve:components_loading:start");
					if (controller_class.components && controller_class.components.length > 0) {
						Component.load(controller_class.components, function () {
							loaded.components = 0;
						});
					}


					$rootScope.$broadcast("axis:resolve:models_loading:finish");


					//Start interval checking if dependencies are loaded correctly
					loader_interval = setInterval(function () {
						if (route.loader_dependencie_error.length > 0) {
							var loader_error_message = "Some controller dependencies could not be loaded due to some reason.";
							for (i in route.loader_dependencie_error) loader_error_message += "\r\n\t - " + route.loader_dependencie_error[i];
							console.error(loader_error_message);
							clearInterval(loader_interval);
							return;
						}
						
						if (loaded.css == 0 &&
							loaded.js == 0 &&
							loaded.models == 0 &&
							loaded.directives == 0 &&
							loaded.components == 0 &&
							loaded.services == 0 &&
							$rootScope.defaultComponentsLoaded) {
							clearInterval(loader_interval);
							
							$timeout(function () {
								$rootScope.$apply(function () {
									deferred.resolve();
									$rootScope.$broadcast("axis:resolve:controller_loading:finish");
								});
							});
						}
					}, 10);
				});
				return deferred.promise;
			}]
		}

	}
]);
angular.module("Axis-Router").config(['$routeProvider',
	function ($routeProvider) {
		$routeProvider.
			when('/' + settings.path.app + '/:name*', {
				templateUrl: function (a) {
					var name = a.name == undefined ? a : a.name,
						parts = a.name.split('/'),
						part,
						page = '';
					for (i in parts) {
						part = parts[i].split("=");
						if (part[1] != undefined) continue;
						if (parts[i] == '') continue;
						page += parts[i] + '/';
					}
					page = page.substr(0, page.length - 1);
					return settings.path.views + page + '.html';
				},
				resolve: {
					load: angular.module('Axis-Router').Router.resolve
				}
			}).
			otherwise({
				redirectTo: settings.router.default_route
			});
	}
]);
