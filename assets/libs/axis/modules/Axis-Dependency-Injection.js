﻿angular.module('Axis-Dependency-Injection', ['Axis']);
//Lazy Load Providers 
angular.module("Axis-Dependency-Injection").config(['$controllerProvider', '$compileProvider', '$filterProvider', '$provide',
	function ($controllerProvider, $compileProvider, $filterProvider, $provide) {
		angular.module("Axis-Dependency-Injection").load = {
			directive: $compileProvider.directive,
			controller: $controllerProvider.register,
			filter: $filterProvider.register,
			factory: $provide.factory
		}

		//Configuring Components object
		angular.module("Axis-Dependency-Injection").components = {
			_components_config: {},
			config: function (name, object) {
				if (!object) return angular.module("Axis-Dependency-Injection").components._components_config[name];
				angular.module("Axis-Dependency-Injection").components._components_config[name.trim()] = object;
				return angular.module("Axis-Dependency-Injection").components._components_config[name];
			}
		}
	}
]);

angular.module('Axis-Dependency-Injection').factory('Component', ['$injector', '$rootScope', '$timeout', function ($injector, $rootScope, $timeout) {
	var loaded_components = {};
	var factory = {
		load: function (components, fncallback) {
			components.total_components_loaded = 0;
			var load = {
				config: function (component) {
					var directory = component.directory;
					var name = directory.replace(/\//g, '-').trim();
					if (settings.debug) console.log('Loading config file for ', directory)
					Loader.load.js(settings.path.components + directory + '/config.js', function () {
						if (settings.debug) console.log('Loading config object for', directory);
						var interval = setInterval(function () {
							var config = angular.module('Axis-Dependency-Injection').components.config(name);
							if (!config) return;
							clearInterval(interval);
							loadcomponent(config);
						}, 1);
						function loadcomponent(config) {
							var component = config;
							component.directives = component.directives ? component.directives : [];
							component.directory = directory;
							component.componentServices = component.componentServices ? component.componentServices : [];
							component.services = component.services ? component.services : [];
							component.models = component.models ? component.models : [];
							component.controllers = component.controllers ? component.controllers : [];
							component.components = component.components ? component.components : [];
							load.components(component, directory);
						}
					});
				},
				components: function (component, directory) {
					if(settings.debug) console.log('Loading components for directory', directory)
					if (component.components.length == 0) {
						return load.componentServices(component, directory);
					}
					factory.load(component.components, function () {
						load.componentServices(component, directory);
					});
				},
				componentServices: function (component, directory) {
					if (settings.debug) console.log('Loading component services for directory', directory)
					var total = component.componentServices.length;
					if (total == 0) {
						load.services(component, directory);
						return;
					}
					for (var i = 0; i < component.componentServices.length; i++) {
						Loader.load.js(settings.path.components + directory + '/' + component.componentServices[i], function () {
							total--;
							if (total == 0) {
								load.services(component, directory);
							}
						})
					}
				},
				services: function (component, directory) {
					if (settings.debug) console.log('Loading services for directory', directory)
					var total = component.services.length;
					if (total == 0) {
						load.models(component, directory);
						return;
					}
					for (var i = 0; i < component.services.length; i++) {
						Loader.load.js(settings.path.services + '/' + component.services[i], function () {
							total--;
							if (total == 0) {
								load.models(component, directory);
							}
						})
					}
				},
				models: function (component, directory) {
					if (settings.debug) console.log('Loading models for directory', directory)
					var total = component.models.length;
					if (total == 0) {
						load.controllers(component, directory);
						return;
					}
					for (var i = 0; i < component.models.length; i++) {
						Loader.load.js(settings.path.models + '/' + component.models[i], function () {
							total--;
							if (total == 0) {
								load.controllers(component, directory);
							}
						})
					}
				},
				controllers: function (component, directory) {
					if (settings.debug) console.log('Loading controllers for directory', directory)
					var total = component.controllers.length;
					if (total == 0) {
						load.directives(component, directory);
						return;
					}
					for (var i = 0; i < component.controllers.length; i++) {
						Loader.load.js(settings.path.components + directory + '/' + component.controllers[i], function () {
							total--;
							if (total == 0) {
								load.directives(component, directory);
							}
						})
					}
				},
				directives: function (component, directory) {
					if (settings.debug) console.log('Loading directives for directory', directory)
					var total = component.directives.length;
					if (total == 0) {
						fncallback();
						return;
					}
					for (var i = 0; i < component.directives.length; i++) {
						Loader.load.js(settings.path.components + directory + '/' + component.directives[i], function () {
							total--;
							var interval;
							if (total == 0) {
								//	fncallback();
								interval = setInterval(function () {
									var directive = directory;
									var directive_name_parts = directive.split('/');
									directive_name = directive_name_parts[0];
									for (var i = 1; i < directive_name_parts.length; i++) {
										directive_name += directive_name_parts[i].upperCaseFirst();
									}
									var directive_name_parts = directive_name.split('-');
									directive_name = directive_name_parts[0];
									for (var i = 1; i < directive_name_parts.length; i++) {
										directive_name += directive_name_parts[i].upperCaseFirst();
									}
									directive_name = directive_name.replace('Directive', '') + 'Directive';
									if (total == 0 && $injector.has(directive_name)) {
										components.total_components_loaded++;
										loaded_components[directory] = true;
										if (settings.debug) {
											console.log('%cLoaded directive:' + directive_name, 'background:#0C5;color:#fff');
											console.log('%cDirective ready:', 'background:#0C5;color:#fff', $injector.has(directive_name));
											console.log('%cTotal loaded', 'background:#08c;color:#fff', components.total_components_loaded);
											console.log('%cFrom total', 'background:#08c;color:#fff', components.length, "IN", components);
										}
										clearInterval(interval);
									}
									else {
										if (settings.debug) console.log('%cDirective not loaded yet:'+ directive_name,'background:#fff;color:#fff');
									}
								}, 1);
							}
						});
					}
				}
			}
			for (var i = 0; i < components.length; i++) {
				var comp = components[i];
				if (typeof comp == 'string') {
					comp = {
						directory: comp
					}
				}
				if (loaded_components[comp.directory] != undefined) {
					components.total_components_loaded++;
					continue;
				}
				load.config(comp);
			}
			var callbackInterval = setInterval(function () {
				if (components.total_components_loaded != components.length) return;
				if (settings.debug) console.log('Components loaded for ', components)
				fncallback();
				clearInterval(callbackInterval);
			},1000);
		}
	}
	return factory;
}]);