﻿var settings = {
	debug: false,
	devmode: true,
	router: {
		//default_route: '/app/dashboard/index',
		default_route: '/',
		//authentication: '/app/authentication/login',
		authentication: '/login'//,
		//authentication: '/',
	},
	path: {
		app: 'app',
		controllers: 'app/controller/',
		services: 'app/services/',
		models: 'app/models/',
		components: 'app/components/',
		views: 'app/views/',
		templates: 'app/templates/',
		partials: 'app/partials/',
		directives: 'app/directives/',
		css: 'assets/css/',
		js: 'assets/js/'
	},
	components: {
		default_file: 'directive',
		default_components: [
			'LoginComponent'
		]
	},
	app: {
		baseurl: 'http://app.nexatlas/',
		theme: 'ng-theme-light',
		language: 'pt-BR',
		//header: {
		//	file: 'app/partials/header.html'
		//},
		//sidebar: {
		//	position: 'fixed',
		//	background: 'ng-background-5',
		//	file: 'app/partials/sidebar.html',
		//	width: 250
		//},
		services: {
			api: 'http://api.nexatlas/',
			facebook: 'http://facebook.com/',
			google: 'http://google.com/'
		},
		is_extension: window.location.protocol == 'chrome-extension:'
	}
};

//Sets crucial URI information based on executing location
switch (window.location.hostname) {
	case 'api.nexatlas':
		settings.app.baseurl = 'http://app.nexatlas';
		settings.app.services.api = 'http://api.nexatlas';
		break;
	default:
		settings.app.baseurl = 'http://' + window.location.hostname + '';
		settings.app.services.api = 'http://api.nexatlas.com';
		break;

}