﻿angular.module("Sugar").directive("simpleListItem", ['$rootScope', function ($rootScope) {
	return {
		replace: true,
		restrict: 'E',
		scope: {
			disabled: '=?',
			iconRight: '@?',
			iconLeft: '@?',
			icon: '@?',
			iconModal: '@?',
			iconPrefixTitle: '@?',
			iconSufixTitle: '@?',
			iconAction: '&?',
			iconLeftAction: '&?',
			iconRightAction: '&?',
			title: '@?',
			subtitle: '@?',
			description: '@?',
			iconStatusClass: '@?',
			iconLeftStatusClass: '@?',
			iconRightStatusClass: '@?',
			iconPrefixStatusClass: '@?',
			iconSufixStatusClass: '@?',
			lineThrough: '=?',
			lineError: '=?',
			avatar: '@?',
			image: '@?',
			selectableType: '@?',
			selectable: '=?',
			selected: '=?',
			selectAll: '=?',
			selectDisabled: '=?',
			type: '@?',
			click: '&?',
			holdon: '&?',
			list: '=?',
		},
		transclude: true,
		link: function (scope, element, attrs) {
			scope.selectableType = !scope.selectableType ? 'single-select' : scope.selectableType;
			scope.clickAction = function () {
				if (scope.click) {
					return scope.click();
				}
				if (scope.selectDisabled || !scope.selectable) return;

				var bSelected = scope.selected;
				if (scope.selectableType == 'single-select' && scope.list) {
					scope.list.forEach(function (oItem) {
						oItem.selected = false;
					});
				}
				scope.selected = !bSelected;

				if (scope.selectAll && scope.selectAll.length) {
					scope.selectAll.forEach(function (oItem) {
						oItem.selected = scope.selected;
					})
				}
			}
			scope.lineType = 'one-line';
			if (scope.subtitle && !scope.description) {
				scope.lineType = 'two-line';
			}
			if (scope.subtitle && scope.description) {
				scope.lineType = 'tree-line';
			}
			scope.$watch(function () {
				element.removeAttr('ng-class ng-disabled ng-repeat ng-click iconRight iconLeft icon iconModel iconAction iconPrefixTitle iconSufixTitle title subtitle description iconStatusClass lineThrough lineError avatar image selectableType selectable selected type click list')
			});

			scope.$watch('iconLeftAction', function () {
				if (!scope.iconLeftAction && scope.iconAction && scope.iconLeft) {
					scope.iconLeftAction = scope.iconAction;
				}
			});
			scope.$watch('iconRightAction', function () {
				if (!scope.iconRightAction && scope.iconAction && scope.iconRight) {
					scope.iconRightAction = scope.iconAction;
				}
			});
			scope.$watch('selected', function () {
				if (scope.selectDisabled && scope.selected) {
					scope.selected = false;
				}
			});
		},
		template: '' +
		'	<li tabindex="0"' +
		'		class="{{selectableType}} {{lineType}} {{type}}"' +
		'		ng-disabled="disabled"' +
		'		ng-class="{ \'icon-left\': (icon && !image && !avatar && !selectable && !iconRight)|| iconLeft,' +
		'					\'icon-right\': (icon && (image || avatar || selectable)) || iconRight,' +
		'					\'with-image\': image,' +
		'					\'with-avatar\': avatar,' +
		'					\'line-through\': lineThrough,' +
		'					\'line-error\': lineError,' +
		'					\'selected\': selected,' +
		'					\'selectable\':selectable && (selectable===undefined || selectable )}"' +
		'		ng-click="clickAction()">' +
		'		<div class="avatar" ng-if="avatar">' +
		'			<img src="{{avatar}}" />' +
		'		</div>' +
		'		<div class="image" ng-if="image">' +
		'			<img src="{{image}}" />' +
		'		</div>' +
		'		<div class="floating-icon" ng-if="icon" ng-click="iconAction()" data-open-modal="{{iconModal}}">' +
		'			<i class="icon {{iconStatusClass}} material-icons">{{icon}}</i>' +
		'		</div>' +
		'		<div class="floating-icon icon-right" ng-if="iconRight" ng-click="iconRightAction()" data-open-modal="{{iconModal}}">' +
		'			<i class="icon {{iconStatusClass}} {{iconRightStatusClass}} material-icons">{{iconRight}}</i>' +
		'		</div>' +
		'		<div class="floating-icon icon-left" ng-if="iconLeft" ng-click="iconLeftAction()" data-open-modal="{{iconModal}}">' +
		'			<i class="icon {{iconStatusClass}} {{iconLeftStatusClass}} material-icons">{{iconLeft}}</i>' +
		'		</div>' +
		'		<div class="item-content">' +
		'			<div class="title">' +
		'				<i class="icon {{iconPrefixStatusClass}} icon-prefix-title material-icons" ng-if="iconPrefixTitle">{{iconPrefixTitle}}</i>' +
		'				<span ng-class="{\'iconized\':iconPrefixTitle || iconSufixTitle}"> ' +
		'					{{title}}' +
		'				</span>' +
		'				<i class="icon {{iconSufixStatusClass}} icon-sufix-title material-icons" ng-if="iconSufixTitle">{{iconSufixTitle}}</i>' +
		'			</div>' +
		'			<div class="subtitle" ng-if="subtitle">' +
		'				<span>' +
		'					{{subtitle}}' +
		'				</span>' +
		'			</div>' +
		'			<div class="description" ng-if="description|| type==\'full-description\'">' +
		'				<span>' +
		'					{{description}}' +
		'				</span>' +
		'				<span ng-transclude></span>' +
		'			</div>' +
		'		</div>' +
		'		<input ng-disabled="selectDisabled" ng-if="selectable && (selectable===undefined || selectable )" type="checkbox" ng-model="selected" ng-checked="selected" ng-class="{\'selected\': selected }" />' +
		'		<label ng-disabled="selectDisabled" ng-if="selectable && (selectable===undefined || selectable )" class="selectable-icon"></label>' +
		'	</li>'
	}
}])