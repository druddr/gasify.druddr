﻿angular.module("Sugar").directive("formFieldCpf", ['$rootScope', '$locale', function ($rootScope, $locale) {
	return {
		replace: true,
		restrict: 'E',
		scope: {
			labelKey: '@?label',
			ngModel: '=ngModel',
			required: '=?',
			disabled: '=?',
			invalidMessage: '=?',
			form: '=?',
			id: '@?',
			size: '@?',
		},
		link: function (scope, element, attrs, controller) {
			scope.language = $rootScope.language;
			scope.label = $rootScope.language.instance.labels[scope.labelKey];
			scope.name = Sugar.generateGuid();
			scope.placeholder = '___.___.___-__';

			element.removeAttr("disabled")

		},
		template: '' +
		'<div class="form-field-input form-field-datetime form-field-group {{size}} input-type-{{inputType}}">' +
		'	<label ng-if="label" for="input-{{uid}}" class="focused always-focused" ng-class="{' +
		'				\'invalid\': form[name].$invalid && form[name].$dirty,' +
		'				\'disabled\': (disabled)'+
		'		   }">{{label}}</label>' +
		'		<input type="text"' +
		'			   class="form-control cpf {{size}}"' +
		'			   ng-model="ngModel"' +
		'			   name="{{name}}"' +
		'			   placeholder="{{placeholder}}" ' +
		'			   ng-required="required"' +
		'			   ng-disabled="disabled"' +
		'			   ui-br-cpf-mask' +
		'			   language="language"' +
		'			   autocapitalize="off"' +
		'			   autocorrect="off"' +
		'			   autocomplete="off"' +
		'			   spellcheck="false" />' +
		'	<div class="input-border-bottom" ng-class="{\'invalid\': form[name].$invalid && form[name].$dirty}"></div>' +
		'	<div class="input-error-message">' +
		'		<span ng-if="form[name].$invalid && form[name].$dirty">' +
		'			{{invalidMessage ? invalidMessage : language.instance.phrases[\'cpf-invalido\']}}' +
		'		</span>' +
		'		<span ng-if="form[name].$invalid && form[name].$dirty">' +
		'			{{invalidMessage ? invalidMessage : language.instance.phrases[\'por-favor-verifique-campo\']}}' +
		'		</span>' +
		'	</div>' +
		'</div>'
	}
}]);