﻿angular.module("Sugar").directive("ratingStars", [function () {
	return {
		replace: true,
		transclude: true,
		restrict: 'E',
		scope: {
			rating: '@?',
			max: '@?'
		},
		link: function (scope, element, attr) {
			scope.fullStars = [];
			scope.halfStars = [];
			scope.emptyStars = [];

			scope.max = scope.max ? scope.max : 5;

			function defineRatings() {
				scope.fullStars = [];
				scope.halfStars = [];
				scope.emptyStars = [];

				for (var i = 0; i < scope.max; i++) {
					if (i < scope.rating && i+1<=scope.rating) {
						scope.fullStars.push(i);						
					}
					if (i < scope.rating && i+1>scope.rating) {
						scope.halfStars.push(i);
					}
					if (i >= scope.rating) {
						scope.emptyStars.push(i);
					}
				}
			}

			scope.$watch('rating', function (a, b) {
				defineRatings();
			})
		},
		template: '<div class="rating">' +
		'	<i class="icon material-icons full-star" ng-repeat="star in fullStars">star</i>' +
		'	<i class="icon material-icons half-star" ng-repeat="star in halfStars">star_half</i>' +
		'	<i class="icon material-icons empty-star" ng-repeat="star in emptyStars">star_border</i>' +
		'	<span class="value">{{rating}}</span>' +
		'</div>'
	}
}]);