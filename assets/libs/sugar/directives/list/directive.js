﻿angular.module("Sugar").directive("list", ['$rootScope', function ($rootScope) {
	return {
		replace: true,
		restrict: 'E',
		scope: {
			items: '=?',
			headerProperty: '@?header',
			idProperty: '@?',
			titleProperty: '@',
			subtitleProperty: '@?',
			descriptionProperty: '@?',
			searchText: '=?',
			selectable: '=?',
			selectableType: '@?',
			selectedOptions: '=?',
			labelFromLanguage: '@?',
			sharedModel: '=?',
			ngModel: '=?'
		},
		link: function (scope, element, attrs) {
			scope.titleProperty = scope.titleProperty.replace(/\'/g, '\"');
			scope.titleProperties = JSON.parse(scope.titleProperty);
			scope.searchText = !scope.searchText ? '' : scope.searchText;
			scope.selectableType = !scope.selectableType ? 'multi-select' : scope.selectableType;
			scope.selectedOptions = '';
			scope.language = $rootScope.language;
			scope.listElement = !scope.items ? 'ul' : 'div';
			scope.header = scope.language.instance.labels[scope.headerProperty];

			var presetModelValue = scope.ngModel;

			scope.config = {
				iconLeft: false,
				iconRight: false,
				image: false,
				avatar: false,
				selected: false,
			}

			if (scope.subtitleProperty) {
				scope.subtitleProperty = scope.subtitleProperty.replace(/\'/g, '\"');
				scope.subtitleProperties = JSON.parse(scope.subtitleProperty);
			}
			if (scope.descriptionProperty) {
				scope.descriptionProperty = scope.descriptionProperty.replace(/\'/g, '\"');
				scope.descriptionProperties = JSON.parse(scope.descriptionProperty);
			}
			if (scope.subtitleProperty && scope.descriptionProperty) {
				scope.lineType = 'tree-line';
			}
			else if (scope.subtitleProperty && !scope.descriptionProperty || !scope.subtitleProperty && scope.descriptionProperty) {
				scope.lineType = 'two-line';
			}
			else {
				scope.lineType = 'one-line';
			}

			scope.setProperties = function (oItem) {
				oItem.$title = '';
				oItem.$subtitle = '';
				oItem.$description = '';
				function setProperty(property, lookInto) {
					var sValue = '';
					for (var i in lookInto) {
						sValue = '';
						if (scope.labelFromLanguage == 'true') {
							sValue = scope.language.instance.labels[oItem[lookInto[i]]];
							if (!sValue && !scope.language.instance.labels.hasOwnProperty(lookInto[i])) {
								sValue = lookInto[i];
							}
							else {
								sValue += ' ';
							}
						}
						else {
							sValue = oItem[lookInto[i]];
							if (!sValue && !oItem.hasOwnProperty(lookInto[i])) {
								sValue = lookInto[i];
							}
							else {
								sValue += ' ';
							}
						}
						oItem[property] += sValue;
					};
				}
				setProperty('$title', scope.titleProperties);
				setProperty('$subtitle', scope.subtitleProperties);
				setProperty('$description', scope.descriptionProperties);
			}

			scope.$watch('items', function () {
				if (!scope.items) return;
				//scope.ngModel = null;
				scope.items.forEach(function (oItem) {
					oItem.selectable = oItem.selectable === undefined && scope.selectable !== undefined ? scope.selectable : oItem.selectable;
					scope.setProperties(oItem);
				})
				if (presetModelValue && scope.items.length) {
					setTimeout(function () {
						scope.$apply(function () {
							scope.ngModel = presetModelValue;
						});
					}, 1);
				}
				else {
					scope.select();
				}
			});

			scope.selectAll = function () {
				if (!scope.items || !scope.items.length) return;
				scope.items.forEach(function (oItem) {
					if (!oItem.selectable) return;
					oItem.selected = scope.config.selected;
				});
				scope.select();
			}

			scope.select = function (addEverything) {
				if (!scope.ngModel) {
					scope.ngModel = scope.selectableType == 'single-select' ? null : [];
				}
				var selected = scope.items.filter(function (a) { return addEverything || a.selected }),
					model = selected;

				if (selected.length && selected.length === scope.items.filter(function (item) { return item.selectable }).length) {
					scope.config.selected = true;
				}
				else {
					scope.config.selected = false;
				}

				if (scope.selectableType == 'single-select') {
					model = selected.length > 0 ? selected[0] : null;
					if (scope.idProperty) {
						model = selected.length > 0 ? selected[0][scope.idProperty] : null;
					}
				}

				if (!doNotUpdateNgModel) {
					/**
                     * ## Complex Stuff
                     * Shared Model means that the selectable-list is conected with other selectable-lists
                     * and thoses conected lists share the same model to store the selected items.
                     * So for each instance of selectable-list the items that will be taken care of selection
                     * are those who are part of the items list of the instance
                     */
					if (scope.sharedModel) {
						var index = null;
						var i = scope.ngModel.length;
						while (i--) {
							for (var a = 0; a < scope.items.length; a++) {
								//Will remove only the items that are part of this instance scope.items
								//The item inside ngModel must be the exact same inside the scope.items
								if ((scope.ngModel[i] && scope.items[a].$$hashKey != scope.ngModel[i].$$hashKey)) {
									continue;
								}
								//Breaks the loop once the item was already found inside ngModel
								//The next ngModel item will be checked now
								scope.ngModel.splice(i, 1);
								break;
							}
						}
						scope.ngModel = scope.ngModel.concat(model);
					}
					else {
						scope.ngModel = model;
					}
				}
				doNotUpdateNgModel = false;
				var selected_option = [];
				for (var i = 0; i < selected.length; i++) {
					var label = [];
					label.push(selected[i].$title);
					selected_option.push(label.join(' '))
				}
				scope.selectedOptions = selected_option.join(', ');
			}
			if (scope.items) {
				scope.select();
			}

			var doNotUpdateNgModel = false;
			scope.$watch('ngModel', function (a, b) {
				if (doNotUpdateNgModel || a == b) return;

				scope.items.forEach(function (oItem) {
					if (scope.idProperty && oItem[scope.idProperty] + "" == scope.ngModel + "") {
						oItem.selected = true;
					}
					else if (!scope.idProperty && oItem == scope.ngModel) {
						oItem.selected = true;
					}
					else {
						if (scope.selectableType == 'single-select') {
							oItem.selected = false;
						}
					}
				});
				doNotUpdateNgModel = true;
				scope.select();
			})

			//SELECTABLE LISTS
			//can be radiobuttons or checkboxes
			element.on(Sugar.events.touchstart, ".selectable .actions-icon .icon", function (e) {
				e.stopPropagation();
			})
			element.on("click keydown", "li.selectable input[type=checkbox]", function (e) {
				e.stopPropagation();
			});
			element.on("click keydown", "li.selectable", function (e) {
				if (e.type == 'keydown') {
					var keycode = e.keyCode || e.which || e.charCode;
					if (keycode != 32 && keycode != 13) return;
				}
				e.preventDefault();
				e.stopPropagation();

				var $self = angular.element(this),
					$checkbox = $self.find("input[type=checkbox]");

				$checkbox.trigger('click');
				if (!$checkbox.is(':checked')) {
					return;
				}

				if ($self.hasClass('single-select')) {
					$self.parent('ul.list').find('li.selectable input[type=checkbox]').not($checkbox.get(0)).each(function () {
						if (!this.checked) return;
						$(this).trigger('click');
					});
				}
			});
		},
		template: '' +
		'<ul class="list">' +
		'	<li tabindex="0"' +
		'		class="header {{selectableType}} "' +
		'		ng-if="header"' +
		'		ng-click="selectAll()"' +
		'		ng-class="{ \'selected\': config.selected,' +
		'					\'two-line\': !selectable,' +
		'					\'tree-line\': selectable,' +
		'					\'selectable\': selectable }">' +
		'		<div>' +
		'			<div class="title">' +
		'				{{header}}' +
		'			</div>' +
		'			<div class="subtitle">' +
		'				<span>{{language.instance.labels.total}} <b>{{items.length}}</b></span>' +
		'			</div>' +
		'			<div class="description" ng-if="selectable">' +
		'				<span>{{language.instance.labels.selecionado}} <b>{{(items| filter: {selected:true}).length}}</b></span>' +
		'			</div>' +
		'		</div>' +
		'		<input ng-if="selectable" type="checkbox" ng-model="config.selected" ng-checked="config.selected" ng-class="{\'selected\': config.selected }" />' +
		'		<label ng-if="selectable" class="selectable-icon"></label>' +
		'	</li>' +
		'	<li ng-repeat="item in items | filter:searchText" tabindex="0"' +
		'		class="{{selectableType}} {{lineType}}"' +
		'		ng-disabled="item.disabled"' +
		'		ng-class="{ \'icon-left\': item.icon && !item.image && !item.avatar && !selectable && !item.iconRight,' +
		'					\'icon-right\': item.icon && (item.image || item.avatar || selectable || item.iconRight),' +
		'					\'with-image\': item.image,' +
		'					\'with-avatar\': item.avatar,' +
		'					\'line-through\': item.lineThrough,' +
		'					\'line-error\': item.lineError,' +
		'					\'selected\':item.selected,' +
		'					\'selectable\':selectable && (item.selectable===undefined || item.selectable )}"' +
		'		ng-click="select()">' +
		'		<div class="avatar" ng-if="item.avatar">' +
		'			<img src="{{item.avatar}}" />' +
		'		</div>' +
		'		<div class="image" ng-if="item.image">' +
		'			<img src="{{item.image}}" />' +
		'		</div>' +
		'		<div class="floating-icon" ' +
		'			 ng-class="{ \'icon-left\': item.icon && !item.image && !item.avatar && !selectable && !item.iconRight,' +
		'						 \'icon-right\': item.icon && (item.image || item.avatar || selectable || item.iconRight)}"' +
		'			 ng-if="item.icon" ng-click="item.iconAction(item)" data-open-modal="{{item.iconModal}}">' +
		'			<i class="icon fa fa-{{item.icon}} {{item.iconStatusClass}}"></i>' +
		'		</div>' +
		'		<div class="item-content">' +
		'			<div class="title">' +
		'				<span>' +
		'					{{item.$title}}' +
		'				</span>' +
		'			</div>' +
		'			<div class="subtitle" ng-if="subtitleProperties">' +
		'				<span>' +
		'					{{item.$subtitle}}' +
		'				</span>' +
		'			</div>' +
		'			<div class="description" ng-if="descriptionProperties">' +
		'				<span>' +
		'					{{item.$description}}' +
		'				</span>' +
		'			</div>' +
		'		</div>' +
		'		<input ng-if="selectable && (item.selectable===undefined || item.selectable )" type="checkbox" ng-model="item.selected" ng-checked="item.selected" ng-class="{\'selected\': item.selected }" />' +
		'		<label ng-if="selectable && (item.selectable===undefined || item.selectable )" class="selectable-icon"></label>' +
		'	</li>' +
		'</ul>'
	}
}])