﻿angular.module("Sugar").directive("actionToolbar", ['$compile',function ($compile) {
	return {
		replace: true,
		transclude: true,
		restrict: 'E',
		transclude: {
			content:'?',
			title:'?title'
		},
		scope: {
			top: '=?',
			bottom: '=?',
			region: '@?',
			fab: '=?',
			showCloseButton: '=?',
			closeButtonAction: '&?',
			allowSelection: '=?'
		},
		link: function (scope, element, attrs, ctrl, transclude) {
			$(".floating-action").fadeOut();
			if (scope.bottom) {
				scope.region = 'bottom';
			}
			if(scope.top){
				scope.region = 'top';
			}

			//In case item selection is active
			if (scope.allowSelection) {
				element.on('click', '.action', function (e) {
					$(this).parent().find(".action").removeClass('active');
					$(this).addClass('active');
				});
			}
			
			scope.closeButton = function (e) {
				element.fadeOut();
				$(".floating-action").fadeIn();
				if (e && e.preventDefault) {
					e.preventDefault();
				}
				if (scope.closeButtonAction) {
					return scope.closeButtonAction();
				}
			}
			
			var backButtonEvent = function () {
				element.fadeOut();
				$(".floating-action").fadeIn();
				var oAction = element.find(".close-action-toolbar");
				if (oAction) {
					oAction.trigger("click");
				}
				//After the bar is fadeOut and after all other back button events are executed
				setTimeout(function () {
					Sugar.preventBackButton = false;
				}, 100);
			}
			if (scope.showCloseButton) {
				Sugar.preventBackButton = true;
				document.addEventListener('backbutton', backButtonEvent, true);
				Sugar.addBackButtonAction(backButtonEvent);
				element.append($compile('<action icon="close" class="close-action-toolbar" action="closeButton()"></action>')(scope));
			}
			scope.$on("$destroy", function () {
				$(".floating-action").fadeIn();
				document.removeEventListener('backbutton', backButtonEvent, true);
				Sugar.removeBackButtonAction(backButtonEvent);
				setTimeout(function () {
					Sugar.preventBackButton = false;
				});
			});
		},
		template: '<section class="action-toolbar {{region ? region+\'-fixed\' : \'\'}}" ng-class="{\'floating-action\':fab}" ng-transclude></section>'
	}
}])