﻿Sugar.effects = {
	init: function () {
		Sugar.effects.bind();
	},
	bind: function () {
		var waveStart = new Date();
		angular.element(document).on(Sugar.events.touchstart, ".action, button, .button, .card-image,.tab", function (e) {
			$target = angular.element(this);
			if ($target.hasClass("no-effect")) return;
			$target.find(".wave-effect").remove();
			var div = document.createElement("div");
			div.className = "wave-effect";
			if (Sugar.IsMobileOrTablet) {
				div.style.top = (e.originalEvent.touches[0].pageY - $target.offset().top - 10) + "px";
				div.style.left = (e.originalEvent.touches[0].pageX - $target.offset().left - 10) + "px";
			}
			else {
				div.style.top = (e.pageY - $target.offset().top - 10) + "px";
				div.style.left = (e.pageX - $target.offset().left - 10) + "px";
			}
			$target.append(div);
			waveStart = new Date();
		});
		angular.element(document).on(Sugar.events.touchend + " " + Sugar.events.touchcancel, function (e) {
			var a = setInterval(function () {
				var date = new Date();
				if (date - waveStart < 300) return;

				$target = angular.element(this);
				$target.find(".wave-effect").addClass("fadeout");
				clearInterval(a);
			}.bind(this), 1);
		});

		$.fn.extend({
			animateCss: function (animationName, animationEndFn) {
				var animationEnd = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';
				$(this).addClass(animationName).addClass('animated').one(animationEnd, function () {
					var $self = $(this);
					setTimeout(function () {
						$self.removeClass('animated').removeClass(animationName);
					}, 0)
					if (animationEndFn) animationEndFn();
					animationEndFn = null;
				});
			}
		});
	}
}
Sugar.effects.init();