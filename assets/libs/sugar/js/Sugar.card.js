﻿Sugar.card = {
	init:function(){
		Sugar.card.events.generic();
	},
	select: function (card) {
		card = angular.element(card);
		if (!card.hasClass('selectable')) return;
		if (card.hasClass("selected")) {
			card.removeClass("selected");
			return;
		};
		if (!card.hasClass("multi-select")) {
			$(".card").removeClass("selected");
		}
		card.addClass("selected");
	},
	events: {
		select: function (card, e) {
			if (e.type == 'keydown' || e.type=='keyup') {
				var keycode = e.which || e.keyCode || e.charCode;
				if (keycode != 32) return;
				e.preventDefault();
			}
			Sugar.card.select(card);
		},
		generic: function () {
			angular.element(document).on(Sugar.events.contextmenu, ".card-image img", function (e) {
				e.preventDefault();
			});
		}
	}
}
Sugar.card.init();