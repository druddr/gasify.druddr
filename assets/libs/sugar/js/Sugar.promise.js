﻿Sugar.promise = function (context) {
	var _status = {
		pending: 'pending',
		fulfilled: 'fulfilled',
		rejected: 'rejected',
		settled: 'settled'
	}
	var _private = {
		then: [],
		catch: [],
		status: _status.pending,
		result: null,
		resolve: function (args) {
			_private.status = _status.fulfilled;;
			_private.call(args);
		},
		reject: function (args) {
			_private.status = _status.rejected
			_private.call(args);
		},
		call: function () {
			var oResult;
			var bBreak = false;

			if (_private.status === _status.fulfilled) {
				
				_private.then.slice().forEach(function (fnThen, i) {
					if (bBreak) return;

					oResult = fnThen.apply(context, [_private.result]);

					_private.then.splice(i, 1);

					bBreak = _private.checkResultExecution(oResult);
				});
			}
			if (_private.status === _status.rejected) {

				_private.catch.slice().forEach(function (fnCatch, i) {
					if (bBreak) return;

					oResult = fnCatch.apply(context, [_private.result]);

					_private.fnCatch.splice(i, 1);

					bBreak = _private.checkResultExecution(oResult);
				});

			}
		},
		checkResultExecution: function (oResult) {
			if (oResult === undefined) return false;

			if (oResult instanceof Sugar.promise) {
				_private.status = _status.pending;

				oResult.then(function (oNewResult) {
					_private.status = _status.fulfilled;
					if (_private.checkResultExecution(oNewResult)) {
						return;
					}
					_private.call();
				}).catch(function (oNewCallback) {
					_private.status = _status.rejected;
					if (_private.checkResultExecution(oNewResult)) {
						return;
					}
					_private.call();
				});

				return true;
			}
			_private.result = oResult;

			return false;
		}
	};

	this.resolve = function (result) {
		if (_private.status == _status.fulfilled) return this;
		_private.status = _status.fulfilled;
		_private.result = result;
		//Call all the multiple callbacks
		_private.call();
		return this;
	};
	this.reject = function (result) {
		_private.status = _status.rejected;
		_private.result = result;
		//Call all the multiple callbacks
		_private.call();
		return this;
	};
	this.then = function (fnCallback) {
		_private.then.push(fnCallback);
		//we have multiple callbacks
		//than we can not use "call"
		//because if this callback was
		//added before the promise resolve
		//than all the callbacks before promise
		//were already called
		_private.call();
		return this;
	};
	this.catch = function (fnCallback) {
		_private.catch.push(fnCallback);
		//we have multiple callbacks
		//than we can not use "call"
		//because if this callback was
		//added before the promise resolve
		//than all the callbacks before promise
		//were already called
		_private.call();
		return this;
	};

	return this;
};