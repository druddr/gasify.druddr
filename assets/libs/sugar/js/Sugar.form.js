﻿Sugar.form = {
	field: {
		binary: {
			init: function () {
				angular.element(document).on('focus blur', '.form-control', function (e) {
					$parent = $(this).parent();
					if (e.type=='focusin') {
						$parent.find('.input-border-bottom').addClass('state-focus')
					}
					else {
						$parent.find('.input-border-bottom').removeClass('state-focus');
					}
				})
			}
		}
	}
}
Sugar.form.field.binary.init();