﻿Sugar.touch = {
	fnQueue: {
		start: [],
		move: [],
		end: [],
		leave: [],
		cancel: [],
		swipeRight: [],
		swipeLeft: [],
		swipeUp: [],
		swipeDown: [],
		swipe:[]
	},
	direction: {
		x: '',
		y: '',
	},
	movement:'',
	distance: {
		x: 0,
		y: 0,
	},
	position: {
		x: { initial: 0, current: 0, last: 0 },
		y: { initial: 0, current: 0, last: 0 }
	},
	time:{
		start:0,
		end: 0,
		total: 0,
		last: 0,
	},
	speed: {
		average: 0,
		current: 0,
		history:[]
	},
	multiple:{

	},
	init: function(){			
		document.addEventListener("touchstart", function (e) {
			Sugar.touch.start(e);
		});
		document.addEventListener("touchmove", function (e) {
			Sugar.touch.move(e);
		});
		document.addEventListener("touchend", function (e) {
			Sugar.touch.end(e);
		});
		document.addEventListener("touchleave", function (e) {
			Sugar.touch.leave(e);
		});
		document.addEventListener("touchcancel", function (e) {
			Sugar.touch.cancel(e);
		});
	},
	clear: function () {
		this.position.x = { initial: 0, current: 0, last: 0 };
		this.position.y = { initial: 0, current: 0, last: 0 };
		this.direction.x = '';
		this.direction.y = '';
	},
	on: function(event,callback){
		if (!this[event]) return;
		Sugar.touch.fnQueue[event].push(callback);
		return this;
	},
	start: function (e) {
		this.time.start = new Date();
		this.speed.history = [];
		this.speed.current = 0;
		this.speed.last = this.time.start;
		this.position.x.initial = e.touches[0].pageX;
		this.position.y.initial = e.touches[0].pageY;
		for (i in this.fnQueue.start) {
			if (typeof this.fnQueue.start[i] == "function") this.fnQueue.start[i](e);
		}
		this.direction.x = null;
		this.direction.y = null;
	},
	move: function (e) {
		this.position.x.last = this.position.x.current;
		this.position.y.last = this.position.y.current;
		this.position.x.current = e.touches[0].pageX;
		this.position.y.current = e.touches[0].pageY;
		this.distance.x = this.direction.x == 'R' ? this.position.x.last - this.position.x.initial : this.position.x.initial - this.position.x.last;
		this.distance.y = this.direction.y == 'D' ? this.position.y.last - this.position.y.initial : this.position.y.initial - this.position.y.last;
		this.movement = this.distance.x > this.distance.y ? 'H' : 'V';
		
		this.speed.current = this.time.last - this.time.start;
		this.speed.history.push(this.speed.current);
		this.time.last = (new Date());

		if (this.direction.x == null) this.direction.x = (this.position.x.current > this.position.x.initial) ? 'R' : 'L';
		if (this.direction.y == null) this.direction.y = (this.position.y.current > this.position.y.initial) ? 'D' : 'U';

		for (i in this.fnQueue.move) {
			if (typeof this.fnQueue.move[i] == "function") this.fnQueue.move[i](e);
		}
	},
	end: function (e) {
		this.time.end = new Date();
		this.time.total = this.time.end - this.time.start;
		var total = 0;
		for (var i = 0; i < this.speed.history.length; i++) {
			total += this.speed.history[i];
		}
		this.speed.average = total / this.speed.history.length;

		var distance = 120;
		if (!Sugar.sidebar.isdragging) {
			if (this.movement == 'H' && this.direction.x == 'R' && this.distance.x >= distance && this.time.total < 400) {
				this.swipeRight(e);
			}
			if (this.movement == 'H' && this.direction.x == 'L' && this.distance.x >= distance && this.time.total < 400) {
				this.swipeLeft(e);
			}
			if (this.movement == 'V' && this.direction.y == 'U' && this.distance.y >= distance && this.time.total < 400) {
				this.swipeUp(e);
			}
			if (this.movement == 'V' && this.direction.y == 'D' && this.distance.y >= distance && this.time.total < 400) {
				this.swipeDown(e);
			}
			if ((this.distance.y >= distance || this.distance.x >= distance) && this.time.total < 400) {
				this.swipe(e);
			}
		}

		for (i in this.fnQueue.end) {
			if (typeof this.fnQueue.end[i] == "function") this.fnQueue.end[i](e);
		}
		this.position = {
			x: { initial: 0, current: 0, last: 0 },
			y: { initial: 0, current: 0, last: 0 }
		};
		this.distance = {
			x: 0,
			y: 0
		}
	},
	leave: function (e) {
		for (i in this.fnQueue.leave) {
			if (typeof this.fnQueue.leave[i] == "function") this.fnQueue.leave[i](e);
		}
	},
	cancel: function (e) {
		for (i in this.fnQueue.cancel) {
			if (typeof this.fnQueue.cancel[i] == "function") this.fnQueue.cancel[i](e);
		}
	},
	swipeRight: function (e) {
		for (i in this.fnQueue.swipeRight) {
			if (typeof this.fnQueue.swipeRight[i] == "function") this.fnQueue.swipeRight[i](e);
		}
	},
	swipeLeft: function (e) {
		for (i in this.fnQueue.swipeLeft) {
			if (typeof this.fnQueue.swipeLeft[i] == "function") this.fnQueue.swipeLeft[i](e);
		}
	},
	swipeDown: function (e) {
		for (i in this.fnQueue.swipeDown) {
			if (typeof this.fnQueue.swipeDown[i] == "function") this.fnQueue.swipeDown[i](e);
		}
	},
	swipeUp: function (e) {
		for (i in this.fnQueue.swipeUp) {
			if (typeof this.fnQueue.swipeUp[i] == "function") this.fnQueue.swipeUp[i](e);
		}
	},
	swipe: function (e) {
		for (i in this.fnQueue.swipe) {
			if (typeof this.fnQueue.swipe[i] == "function") this.fnQueue.swipe[i](e);
		}
	}
};
Sugar.touch.init();