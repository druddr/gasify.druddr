﻿Sugar.switchbox = {
	init:function(){
		Sugar.switchbox.bind();
	},
	bind: function () {
		angular.element(document).on(Sugar.events.touchstart, ".switch-box .switch", function () {
			var $self = $(this);
			if ($self.hasClass("on")) {
				$self.removeClass("on").attr("data-state", "off");
				$self.find("input[type=checkbox]").trigger("click").removeAttr("checked");
				return;
			}
			$self.addClass("on").attr("data-state", "on");
			$self.find("input[type=checkbox]").trigger("click").attr("checked", true);
			//$self.find("i").addClass("fa fa-check");
		});

	}
}
Sugar.switchbox.init();