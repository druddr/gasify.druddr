(function() {

  angular.module('textareaResize', [])
    .directive('textareaResize', function () {
      return {
        require: "ngModel",
        link: function (scope, element, attrs, ctrl) {
          element.bind('keyup', function () {
            var text = element[0].value;
            var rows = text.split('\n').length;

            if (rows > 5) element[0].style.height = (rows * 26) + 'px';
          });
        }
      };
    });

})();
