angular.module("Axis-Dependency-Injection").load.factory("CreditService", ['$resource', function ($resource) {
	return $resource(settings.app.services.api + 'Compra/', null, {
		/**
		 * Buys more credits to the user
		 */
		post: {
			url: settings.app.services.api + 'Compra/ComprarCredito',
			method: 'POST',
			isArray: false
		}
	});
}]);
