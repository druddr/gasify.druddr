angular.module("Axis-Dependency-Injection").load.factory("PurchaseService", ['$resource', function ($resource) {
	return $resource(settings.app.services.api + 'Compra/', null, {
		/**
		 * Gets a purchase statement
		 */
		get: {
			url: settings.app.services.api + 'Compra/BuscarExtratoCredito',
			method: 'GET',
			isArray: false
		},
		/**
		 * Buys more credits to the user
		 */
		buy: {
			url: settings.app.services.api + 'Compra/BuscarExtratoCredito',
			method: 'POST',
			isArray: false
		}
	});
}]);
