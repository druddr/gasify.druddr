﻿angular.module("Axis-Dependency-Injection").load.factory("UserService", ['$resource', function ($resource) {
	return $resource(settings.app.services.api + 'Acesso/', null, {
		/**
		 * Authenticates an user
		 */
		authenticate: {
			url: settings.app.services.api + 'Acesso/Login',
			method: 'POST',
			isArray: false
		},
		/**
		 * Creates an user account
		 */
		create: {
			url: settings.app.services.api + 'Cliente/CadastrarBasico',
			method: 'POST',
			isArray: false
		},
		/**
		 * Checks whether an e-mail is available
		 */
		checkEmailAvailable: {
			//TODO: REMOVE COMMENT WHEN AVAILABLE
			//url: settings.app.services.api + 'Cliente/VerificarEmailDisponivel',
			method: 'POST',
			isArray: false
		},
		/**
 		 * Requests an user account's change of password
		 */
		lostPasswordRequest: {
			url: settings.app.services.api + 'Acesso/RecuperarSenha',
			method: 'POST',
			isArray: false
		},
	});
}]);
