﻿angular.module("Axis-Dependency-Injection").load.factory("PasswordRequestService",
	['$resource', function ($resource) {
		return $resource(settings.app.services.api + 'Acesso/', null, {
			changePassword: {
				url: settings.app.services.api + 'Acesso/ResetarSenha',
				method: 'POST',
				isArray: false
			},
			checkPasswordRequest: {
				url: settings.app.services.api + 'Acesso/VerificarToken',
				method: 'POST',
				isArray: false
			},
			resetPassword: {
				url: settings.app.services.api + 'Acesso/RecuperarSenha',
				method: 'POST',
				isArray: false
			}
		});
	}]);
