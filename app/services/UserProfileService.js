﻿angular.module("Axis-Dependency-Injection").load.factory("UserProfileService", ['$resource', function ($resource) {
	return $resource(settings.app.services.api + 'Acesso/', null, {
		/**
		 * Gets an user profile data
		 */
		get: {
			url: settings.app.services.api + 'Acesso/BuscarPerfil',
			method: 'GET',
			isArray: false
		},
		/**
		 * Updates user profile data
		 */
		update: {
			url: settings.app.services.api + 'Cliente/AtualizarCadastro',
			method: 'POST',
			isArray: false
		},
	});
}]);
