angular.module("Axis-Dependency-Injection").load.factory("PurchaseStatusService", ['$resource', function ($resource) {
	return $resource(settings.app.services.api + 'Compra/', null, {
		/**
		 * Gets a purchase statement
		 */
		get: {
			url: settings.app.services.api + 'Compra/BuscarStatusCompraEnum',
			method: 'GET',
			isArray: false
		}
	});
}]);
