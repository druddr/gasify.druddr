﻿angular.module("Axis-Dependency-Injection").load.factory("SampleClassMapper", ['$resource', function ($resource) {
	return $resource(settings.app.services.api + 'EntityMapper/SampleClass/:id', {
		id: '@id'
	});
}]);