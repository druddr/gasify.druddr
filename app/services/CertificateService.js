angular.module("Axis-Dependency-Injection").load.factory("CertificateService", ['$resource', function ($resource) {
	return $resource(settings.app.services.api + 'Compra/', null, {
		/**
		 * Gets a purchase credit statement
		 */
		get: {
			url: settings.app.services.api + 'Compra/BuscarExtratoCredito',
			method: 'GET',
			isArray: false
		}
	});
}]);
