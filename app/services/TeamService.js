angular.module("Axis-Dependency-Injection").load.factory("TeamService", ['$resource', function ($resource) {
	return $resource(settings.app.services.api + 'Time/', null, {
		/**
		 * Gets a team list
		 */
		get: {
			url: settings.app.services.api + 'Time/BuscarTodosTimes',
			method: 'GET',
			isArray: false
		}
	});
}]);
