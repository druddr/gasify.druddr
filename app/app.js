﻿/******************************
 * Axis (angular) app settings file
 *
 * @author bruno.teixeira.silva
 * @version 1.0.0
 *
 ******************************/
/**
 * REF: DEV MODE
 */
settings.devmode = true;
/**
 * REF: DEBUG MODE
 */
settings.debug = false;


/**
 * Front-end app general configuration section
 */
settings.app = {
	/**
	 * Application name; Must be referenced in language file!
	 * Nb.1: Will be associated to the language file inside /app/i18n/<language>.json
	 */
	title: "gasify", //Will be associated to the language file inside /app/i18n/<language>.json
	baseurl: "https://gasify.druddr.com/",
	/**
	 * App-bar directive defaults
	 */
	appBar: {
		title: "gasify",
		hidden: false,
		mainIcon: "menu",
		mainIconAction: function() {
			return console.log(
				"Header main icon click function is not-implemented."
			);
		},
		actions: []
	},
	/**
	 * Token based authentication enabler
	 */
	sendsAuthorizationToken: true,
	authorizationTokenKeyword: "Token",
	/**
	 * Angular theme selection
	 */
	theme: "ng-theme-light",
	/**
	 * Default app language
	 */
	language: "pt-BR",
	/**
	 * Service location configurator
	 */
	services: {
		/**
		 * App API primary location
		 */
		//api: 'https://api.gasify.net.br/', //Isn't used now
		api: "",
		/**
		 * Firebase configuration object
		 */
		firebase: {
			apiKey: "AIzaSyDROHmNe5s5apGcoHeRWMSZ04oaaV2gWhw",
			authDomain: "druddr-152717.firebaseapp.com",
			databaseURL: "https://druddr-152717.firebaseio.com",
			projectId: "druddr-152717",
			storageBucket: "druddr-152717.appspot.com",
			messagingSenderId: "405894163505"
		},
		/**
		 * Google Maps API configuration object
		 */
		googleMaps: {
			key: "AIzaSyCxhv4Io8ILqo4GX2P8kivz2uzn3MJSCAU",
			v: "3.20", //defaults to latest 3.X anyhow
			libraries: "weather,geometry,visualization"
		},
		/**
		 * Sentry request monitor configuration
		 */
		sentry: {
			active: false,
			url: "" //'https://e9c97ff880484068b258e6b8000b2a61@sentry.io/195215'
		},
		/**
		 * Other location settings
		 */
		facebook: "http://facebook.com/",
		google: "http://google.ccom/"
	},
	is_extension: window.location.protocol == "chrome-extension:"
};
/**
 * Default router routes
 */
settings.router.signupform = '/app/authentication/form';
settings.router.signupgoogle = '/app/authentication/form';
settings.router.signupfacebook = '/app/authentication/form';
settings.router.forbidden = '/app/static/errors/forbidden';
settings.router.login = '/app/authentication/login';
/**
 * Default system route
 */
settings.router.default_route = '/app/dashboard/i';
/**
 * Login page system route
 */
settings.router.authentication = '/app/authentication/login';
/**
 * Browser not supported error page system route
 */
settings.router.notSupported = "/app/not-supported/message";
//TODO: Implement auto-router tables
settings.router.custom.param = function (route, paramName, paramValue) {
	return "";
}
/*
 * User custom routes
 *
 * OBS.1: Alternate solution to the router tables
 *
 */
settings.router.custom = {
	/**
	 * Routes under #/app/authentication/
	 */
	authentication: {
		/**
		 * Route: #/app/authentication/logout
		 */
		logout: '/app/authentication/logout'
	},
	/**
	 * Routes under #/app/user/
	 */
	user: {
		/**
		 * Route: #/app/user/register
		 */
		register: '/app/user/register',
		/**
		 * Route: #/app/user/profile
		 */
		profile: '/app/user/profile',
		/**
		 * Route: #/app/user/confirmEmail
		 */
		confirmEmail: '/app/user/confirmEmail',
		/**
		 * Route: #/app/user/lost-password
		 */
		lostPassword: '/app/user/lost-password'
	},
	/**
	 * Routes under #/app/user-profile/
	 */
	userProfile: {
		/**
		 * Route: #/app/user-profile/edit
		 */
		edit: '/app/user-profile/edit',
	},
	/**
	 * Routes under #/app/password-request/
	 */
	passwordRequest: {
		/**
		 * Route: #/app/password-request/register
		 */
		checkPasswordRequest: '/app/password-request/check',
		/**
		 * Route: #/app/password-request/reset-password
		 */
		resetPassword: '/app/password-request/reset-password'
	},
	/**
	 * Routes under #/app/dashboard/
	 */
	dashboard: {
		/**
		 * Route: #/app/dashboard/i
		 */
		i: '/app/dashboard/i',
		/**
		 * Route: #/app/dashboard/certificates
		 */
		certificates: '/app/dashboard/certificates',
		/**
		 * Route: #/app/dashboard/prizes
		 */
		prizes: '/app/dashboard/prizes',
		/**
		 * Route: #/app/dashboard/purchases
		 */
		purchases: '/app/dashboard/purchases',
		/**
		 * Route: #/app/dashboard/teams
		 */
		teams: '/app/dashboard/teams'
	},
	/**
	 * Routes under #/app/redeem/
	 */
	redeem: {
		/**
		 * Route: #/app/redeem/instructions
		 */
		instructions: "/app/redeem/instructions",
		/**
		 * Route: #/app/redeem/spots
		 */
		spots: "/app/redeem/spots"
	}
}

/**
 * Default (custom) system components to be loaded
 *
 * OBS.1: Will generally look at the "/app/components/" folder for some possible files
 * * config.js - Component configuration script file
 * * directive.js - Directive behaviour script file
 * * [OPTIONAL] template.html - Directive template HTML file (the view HTML can be located inside directive.js)
 *
 */
settings.components.default_components = [
	"app-bar",
	"app-drawer",
	"login/form", //e.g /app/components/<component> becomes /app/components/login/form
	"pi/img", //e.g /app/components/<component> becomes /app/components/pi/img
	"d/svg", //e.g /app/components/<component> becomes /app/components/d/svg
	"pi/select", //e.g /app/components/<component> becomes /app/components/pi/select
	"pi/terms-modal", //e.g /app/components/<component> becomes /app/components/pi/terms-modal
	"d/message", //e.g /app/components/<component> becomes /app/components/d/message
	"d/general-modal", //e.g /app/components/<component> becomes /app/components/d/general-modal
	"shopping/cart/layout",
	"user/profile/form",
	"user/lost-password/form",
	"user/redefine-password/form"
];

//BEGIN
//TODO: Remove this functions from here
function getBase64Image(url, callback) {
	var canvas = document.createElement("canvas"),
		img = document.createElement("img");
	img.onload = function () {
		var canvas = document.createElement("canvas");
		canvas.id = toString((new Date()).getTime()) + '__canvas_id';
		canvas.width = img.width;
		canvas.height = img.height;
		var ctx = canvas.getContext("2d");
		ctx.drawImage(img, 0, 0);
		var dataURL = canvas.toDataURL("image/png");
		callback(dataURL);
	};
	img.setAttribute('crossOrigin', 'anonymous');
	img.src = url;
}

function arrayFindString(arr, s) {
	//console.log(arr);
	for (var i = 0; i < arr.length; i++) {
		if (s.indexOf(arr[i]) !== -1)
			return i;
	}
	return -1;
}
//END: TODO

///////////////////////////
// ENVIRONMENT SELECTION //
///////////////////////////

/**
 * Environment selection
 *
 * OBS.1: Some configuration may vary given the possible different scenarios
 *
 */
switch (window.location.hostname) {
	/**
	 * Live (production)
	 */
	case 'gasify.net.br':
		settings.app.baseurl = 'https://' + window.location.hostname + '/';
		settings.app.services.api = 'https://servicosapi.aplub.com.br/AppCampeaoapi/';
		settings.app.sendsAuthorizationToken = true;
		settings.devmode = false;
		break;
	/**
	 * Stage (alfa, beta)
	 */
	case 'stage.gasify.net.br':
		settings.app.baseurl = 'https://' + window.location.hostname + '/';
		settings.app.services.api = 'https://servicosapi.aplub.com.br/AppCampeaoapi/';
		settings.app.sendsAuthorizationToken = false;
		settings.devmode = false;
		break;
	/**
	 * Local (localhost) mode v-host
	 */
	case 'gasify.base':
		settings.app.baseurl = 'http://gasify.base/';
		settings.app.services.api = 'http://capemisa-hom.aplub.com.br/Gasify/';
		settings.app.sendsAuthorizationToken = true;
		settings.devmode = true;
		break;
	/**
	 * Local (localhost) mode port
	 */
	case 'localhost':
		settings.app.baseurl = 'http://localhost:3000/';
		settings.app.services.api = 'http://localhost:8080/';
		settings.app.sendsAuthorizationToken = true;
		settings.devmode = true;
		break;
	/**
	 * Any other environment
	 */
	default:
		settings.app.baseurl = 'https://' + window.location.hostname + '/';
		settings.app.services.api = 'https://servicosapi.aplub.com.br/AppCampeaoapi/';
		settings.app.sendsAuthorizationToken = true;
		settings.devmode = false;
		break;
}

///////////////////////////////////////////
// Injecting/configuring Angular modules //
///////////////////////////////////////////

/**
 * Injecting default directives
 *
 * OBS.1: Directives like:
 * * ng-sanitize
 * * ng-showdown
 * * ui.sortable - angular sortable view for drag-n-drop sort
 * * firebase
 */
angular.module('System-Authorization', ['ngSanitize', 'ng-showdown', 'ui.sortable', 'firebase']);

/**
 * Injecting the (user) Session factory into the system-authorization module
 * @param {object} $rootScope main/root objects wrapper
 */
angular.module("System-Authorization").factory("Session", ['$rootScope', function ($rootScope) {
	/**
	 * localstorage data keys
	 */
	var $current_user_store_key = '#current$user@';
	var $current_userProfile_store_key = '#current$userProfile@';
	var $remember_current_user_store_key = '#remember$user@';
	var $current_token_store_key = '#session$token@';
	var $current_team_store_key = '#current$team@';

	/**
	 * Session object instance
	 *
	 * OBS.1: Includes the requirement for allowed use of local storage.
	 *
	 */
	var session = {
		/**
		 * When Session factory is available,
		 * Removes all data that was stored at localStorage
		 *
		 * For usage, invoke
		 * Session.clear(function(Session) { alert('Function was ran after the "clear" process'); });
		 */
		clear: function (callback) {
			//Unsets local storage, sets user not logged in
			localStorage.clear()
			$rootScope.logged = false;

			////Notifies listeners
			//broadcast(session);
			//Clears each object from session references
			session.token("")
			session.rememberUser(false)
			session.setCurrent.user(null)
			session.setCurrent.userProfile(null)
			//TODO: remove? session.setCurrent.team(null)

			//If a callback was specified
			if (angular.isFunction(callback)) callback(session);
		},
		/**
		 * When Session factory is available,
		 * Defines all data that is currently stored at localStorage
		 *
		 * For usage, invoke (as a sample)
		 * Session.define(
		 *	{
		 *		token: '31aG...',
		 *		rememberMe: true,
		 *		user: {
		 *			id: 0,
		 *			email: 'user@domain'
		 *		},
		 *		userProfile: {
		 *			name: 'User',
		 *		},
		 *		//TODO: remove? team: {
		 *			id: 'User',
		 *			name: 'Test'
		 *		}
		 *	}, function(Session) { alert('Function was ran after the "define" process'); });
		 */
		define: function (objects, callback) {
			//First clears all previous objects
			session.clear();
			//Write new ones, but only those passed on
			if (objects.token) session.token(objects.token);
			if (objects.rememberMe) session.rememberUser(objects.rememberMe);
			if (objects.user) session.setCurrent.user(objects.user);
			if (objects.userProfile) session.setCurrent.userProfile(objects.userProfile);
			//TODO: remove? if (objects.team) session.setCurrent.team(objects.team);

			return callback && angular.isFunction(callback) ? callback(session) : session;
		},
		/**
		 * When Session factory is available,
		 * Defines whether the user should be remembered at the current device or not
		 *
		 * For usage, invoke
		 * Session.rememberUser(true|false);
		 */
		rememberUser: function (remember) {
			session.current.rememberMe = remember;
			//If session should not be recorded to local storage
			if (!session.current.rememberMe) return remember;
			return Storage.local.set($remember_current_user_store_key, remember);
		},
		/**
		 * When Session factory is available,
		 * Re-defines the current token at this device
		 *
		 * For usage, invoke
		 * Session.token(some_token);
		 */
		token: function (token) {
			//Builds a RegExp token cleaner object
			regExp = new RegExp('(' + settings.app.authorizationTokenKeyword + ')(\s{0,})', 'ig');

			//Sets the token
			token = token.replace(regExp, '').trim();
			session.current.token = token;

			//Removes the regexp object
			delete regExp;

			//if (!session.current.rememberMe) return token;
			return Storage.local.set($current_token_store_key, token);
		},
		/**
		 * When Session factory is available,
		 * Re-defines user and/or user profile at the current device
		 *
		 * For usage, invoke
		 * Session.setCurrent.user({ property: value, property2: value2 });
		 * Session.setCurrent.userProfile({ property: value, property2: value2 });
		 * Session.setCurrent.team({ property: value, property2: value2 });
		 */
		setCurrent: {
			user: function (user) {
				session.current.user = user;
				//If session should not be recorded to local storage
				if (!session.current.rememberMe) return user;
				return Storage.local.set($current_user_store_key, user);
			},
			userProfile: function (userProfile) {
				session.current.userProfile = userProfile;
				//If session should not be recorded to local storage
				if (!session.current.rememberMe) return userProfile;
				return Storage.local.set($current_userProfile_store_key, userProfile);
			},
			//TODO: remove?
			//team: function (team) {
			//	session.current.team = team;
			//	//If session should not be recorded to local storage
			//	if (!session.current.rememberMe) return team;
			//	return Storage.local.set($current_team_store_key, team);
			//}
		},
		/**
		 * When Session factory is available,
		 * Retrieves localstorage values at the current device
		 *
		 * For usage, invoke
		 * Session.current.user;
		 * Session.current.userProfile;
		 * Session.current.rememberMe;
		 * Session.current.team;
		 * Session.current.token;
		 */
		current: {
			//Localstorage access
			user: Storage.local.get($current_user_store_key, {}),
			userProfile: Storage.local.get($current_userProfile_store_key, {}),
			rememberMe: Storage.local.get($remember_current_user_store_key, false),
			//TODO: remove? team: Storage.local.get($current_team_store_key),
			token: Storage.local.get($current_token_store_key)
			//user: localStorage.getItem($current_user_store_key) || {},
			//userProfile: localStorage.getItem($current_userProfile_store_key) || {},
			//rememberMe: localStorage.getItem($remember_current_user_store_key) || false,
			//team: localStorage.getItem($current_team_store_key) || {},
			//token: localStorage.getItem($current_token_store_key)
		}
	}

	//Object includes locally saved data
	return session;
}]);

/**
 * Runs the system-authorization module
 * Configures any additional security modules
 * For instance:
 * * Firebase Auth ($firebaseAuth)
 * @param {object} $rootScope main/root objects wrapper
 * @param {object} $location url location angular service
 * @param {object} $http http-request angular service
 * @param {object} Session exposable user data service container
 */
angular.module("System-Authorization").run(
	[
		'$rootScope',
		'$location',
		'$http',
		'$firebaseAuth',
		'Session',
		function ($rootScope, $location, $http, $firebaseAuth, Session) {
			$rootScope.$on('$locationChangeStart', function (event, next, current) {
				/**
				 * By default, root scope is kept unlogged in
				 */
				$rootScope.logged = false;

				//Sets the last route, if it has changed
				if ($location.$$path != settings.router.forbidden
					&& $location.$$path != settings.router.signupform
					&& $location.$$path != settings.router.authentication
					&& $location.$$path != settings.router.signupgoogle
					&& $location.$$path != settings.router.signupfacebook) {
					$rootScope.lastRoute = $location.$$url;
				}

				/**
				 * Registers user authentication state changed listener
				 * Deals with user info and
				 */
				$rootScope.auth().$onAuthStateChanged(function(user) {
					//debugger;
					// TODO: apply correct functioning of
					// session/localstorage usage with firebase
					if (user) {
						// User is signed in.
						$rootScope.logged = true;
						//Sets the user info session
						Session.setCurrent.user(user);
						//Session.setCurrent.profile(user);
						$rootScope.processLoggedInRoute(user);
					} else {
						// User is signed out.
						$rootScope.logged = false;
						// Clears session
						Session.clear();
						//Executes the processing of the anonymous route
						$rootScope.processAnonymousRoute();
					}
				});

				/**
				 * Keeps the no-auth-required routes collection
				 */
				$rootScope.noAuthRequiredPages = [
					settings.router.forbidden
					//, settings.router.signupform
					, settings.router.authentication
					, settings.router.signupgoogle
					, settings.router.signupfacebook
					, settings.router.custom.user.register
					, settings.router.custom.user.profile
					, settings.router.custom.user.lostPassword
					, settings.router.custom.user.confirmEmail
					, settings.router.custom.passwordRequest.checkPasswordRequest
					, settings.router.custom.passwordRequest.resetPassword
				]

				/**
				 * Process the anonymous route
				 */
				$rootScope.processAnonymousRoute = function () {
					//If no auth is required for the current page
					if (arrayFindString($rootScope.noAuthRequiredPages, $location.$$path) === -1) {
						//Redirects to the requested page
						$location.path(settings.router.authentication);
					}
				}

				/**
				 * Processes the logged in route for a user
				 * @param {FirebaseUser} user
				 */
				$rootScope.processLoggedInRoute = function (user) {
					//This is the case when the use tries to access a page which is only useful
					//When he is not authenticated or registered, except for forbidden page which
					//Might yet be accessed even though he is authenticated (and else pages)
					if (arrayFindString($rootScope.noAuthRequiredPages, $location.$$path) > -1
						&& !($location.$$path.indexOf(settings.router.forbidden) > -1)
						&& !($location.$$path.indexOf(settings.router.custom.user.confirmEmail) > -1)
						&& !($location.$$path.indexOf(settings.router.custom.user.profile) > -1)
						&& !($location.$$path.indexOf(settings.router.custom.authentication.logout) > -1)) {
						//If the user is already logged in, and the condition described has a match
						//Drives it to the default route
						$location.path(settings.router.default_route);
					}
				}

				// // Checks Session, which actually looks for:
				// // The presence of
				// // * Session.current.user,
				// // * Session.current.user.email and
				// // * Session.current.token
				// if (!Session.current.user || !Session.current.user.email || !Session.current.token) {
				// 	//User is not logged in anyway
				// 	$rootScope.logged = false;

				// 	//If no auth is required for the current page
				// 	if (arrayFindString(noAuthRequiredPages, $location.$$path) === -1) {
				// 		//Redirects to the requested page
				// 		$location.path(settings.router.authentication);
				// 	}
				// }
				// else {
				// 	$rootScope.logged = true;

				// 	//This is the case when the use tries to access a page which is only useful
				// 	//When he is not authenticated or registered, except for forbidden page which
				// 	//Might yet be accessed even though he is authenticated (and else pages)
				// 	if (arrayFindString(noAuthRequiredPages, $location.$$path) > -1
				// 		&& !($location.$$path.indexOf(settings.router.forbidden) > -1)
				// 		&& !($location.$$path.indexOf(settings.router.custom.user.confirmEmail) > -1)
				// 		&& !($location.$$path.indexOf(settings.router.custom.user.profile) > -1)
				// 		&& !($location.$$path.indexOf(settings.router.custom.authentication.logout) > -1)) {
				// 		//If the user is already logged in, and the condition described has a match
				// 		//Drives it to the default route
				// 		$location.path(settings.router.default_route);
				// 	}
				// }
			});
}]);

/**
 * Adds a factory of HTTP Interceptor into the system-authorization module
 * @param {object} $q state identifier
 * @param {object} $rootScope main/root objects wrapper
 * @param {object} $log log service
 * @param {object} Session exposable user data service container
 */
angular.module("System-Authorization").factory('HeadersDefault-httpInterceptor', ['$q', '$rootScope', '$log', 'Session', function ($q, $rootScope, $log, Session) {
	return {
		/*
		 * Request interceptor
		 */
		request: function (config) {
			//Sets the basic request headers
			config.headers['X-Requested-With'] = 'XMLHttpRequest';
			config.headers['Rest'] = 'REST';

			//If enabled tokens, set it as a header too
			//Follows this pattern "Authorization: Token <token-hash>"
			if (settings.app.sendsAuthorizationToken) {
				//Without "Token" keyword :
				config.headers['Authorization'] = Session.current.token;
				//With "Token" keyword : config.headers['Authorization'] = settings.app.authorizationTokenKeyword + ' ' + Session.current.token;
			}

			return config || $q.when(config);
		},
		/*
		 * Error request interceptor
		 */
		requestError: function (rejection) {
			//Devmode ON
			if (settings.devmode) {
				console.log("Request rejected. :/");
				console.log(rejection);
			}
			//Will just reject the step
			return $q.reject(rejection);
		},
		/*
		 * Response interceptor
		 */
		response: function (response) {
			//If enabled tokens
			if (settings.app.sendsAuthorizationToken) {
				//Gets the response headers object
				var headersAuth = response.headers('Authorization');
				//If the object has a new auth token, updates it at the session
				if (headersAuth) {
					Session.token(headersAuth);
					//If devmode is enabled
					if (settings.devmode) console.log('Token changed to ' + headersAuth + '.');
				}
			}

			return response || $q.when(response);
		},
		/*
		 * Error response interceptor
		 */
		responseError: function (response) {
			//Devmode ON
			if (settings.devmode) {
				console.log("Response with an error. :/");
				console.log(response);
			}
			//Will just reject the step
			return $q.reject(response);
		}
	};
}])
.config(['$httpProvider', function ($httpProvider) {
	$httpProvider.interceptors.push('HeadersDefault-httpInterceptor');
}]);

/**
 * (Angular) main system variable and "System" module dependencies
 * Configures
 * * System router (Axis-Router)
 * * Axis dependency manager
 * * Authorization management
 * * Firebase service
 * * Google Maps API service
 */
var System = angular
	.module("System", [
		"Axis",
		"Axis-Router",
		"System-Authorization",
		"firebase",
		"ngMap"
	])
	.config([
		function() {
			//Initializes Firebase service
			firebase.initializeApp(settings.app.services.firebase);
			//Configures persistence for localStorage mode
			firebase
				.auth()
				.setPersistence(firebase.auth.Auth.Persistence.LOCAL);
		}
	]);

/**
 * Creating the language factory into the system module
 */
System.factory("i18n", [(function () {
	var service = {
		language: {},
		changeLanguage: function (lang) {
			$.ajax({
				url: "app/i18n/" + lang + ".json",
				contentType: "application/json; charset=utf-8",
				dataType: "json",
				scriptCharset: "utf-8",
				async: false,
				success: function (data) {
					service.language = data;
				},
				error: function (e) {
					//Error loading the language file
					console.log("Language file couldn't load.");
					console.log(e);
				}
			});
		},
		getTranslated: function (key) {
			var value = '';
			try {
				value = eval("service.language." + key);
			}
			catch (e) { }
			return value;
		}
	}

	service.changeLanguage("pt-BR");
	return function () {
		return service;
	}
})()]);

/**
 * Creating the language filter into the system module
 * @param {object} i18n translation service container
 */
System.filter("translate", ['i18n', function (i18n) {
	return function (input) {
		return i18n.getTranslated(input);
	}
}]);

/**
 * Injecting the Application controller into the system module
 * @param {object} $rootScope main/root objects wrapper
 * @param {object} $location url location angular service
 * @param {object} $scope controller objects container
 * @param {object} i18n translation service container
 * @param {object} Session exposable user data service container
 */
System.controller("ApplicationController",
	[
		'$rootScope',
		'$location',
		'$scope',
		'$firebaseAuth', // Must have loaded firebase authentication
		'i18n',
		'Session',
		function ($rootScope, $location, $scope, $firebaseAuth, i18n, Session) {
	/**
	 * Keeps language file
	 */
	$rootScope.language = {
		instance: i18n.language
	}
	/**
	 * Firebase Auth info provider instance mapping
	 * At the moment, will persist login locally
	 */
	$rootScope.auth = $firebaseAuth
	/**
	 * Driver the user to the default route
	 */
	$rootScope.navigateToDefault = function () {
		if (settings.devmode) console.log("Navigating to default route.")
		return $location.path(settings.router.default_route);
	}
	/**
	 * Drives the user to the specified location (param)
	 * @param {string} sPath /path/to/go/to
	 */
	$rootScope.navigate = function (sPath) {
		if (settings.devmode) console.log("Navigating to route: " + sPath)
		return $location.path(sPath);
	}
	/**
	 * Used to control some items visibility
	 */
	$scope.loggedIn = function() {
		//return $rootScope.logged;
		return $rootScope.auth().$getAuth() ? true : false
	}
	/**
	 * Same as $rootScope.navigate does, except that this makes available for all $scopes
	 * @param {string} sPath /path/to/go/to
	 */
	$scope.navigate = function (sPath) {
		if (settings.devmode) console.log("Navigating to route: " + sPath)
		return $location.path(sPath);
	}
	/**
 	 * Prepares user info to be used by components, like the app-drawer
	 */
	$scope.prepareUserInfo = function () {
		if (!Session || !Session.current.userProfile) return false;

		/**
		 * Executes the sequence of info preparation
		 */
		$scope.appDrawer.set.picture(Session.current.userProfile.imagem)
		$scope.appDrawer.set.name(Session.current.userProfile.nome)
		$scope.appDrawer.set.email(Session.current.user.email)
	}
	/**
	 * Keeps the functionality of message box
	 * Can be used like to generate an error
	 * $scope.messageBox.error('An error occurred')
	 *
	 * or a success message
	 * $scope.messageBox.success('Yeah!!')
	 *
	 * or an info box
	 * $scope.messageBox.info('Something strange happened, but everything is ok')
	 *
	 * or something to be concerned at last happened
	 * $scope.messageBox.alert('You should expect delays')
	 *
	 * or any message, styled, obviously
	 * $scope.messageBox.message('A plain message', 'alert')
	 *
	 */
	$scope.messageBox = {
		type: 'error',
		message: '',
		hide: true,
		toggleCallback: () => {},
		buttonText: $rootScope.language.instance.labels.ok,
		/**
		 * Pops a success styled messagebox
		 * @param {string} message to be shown in the box
		 */
		success: (message) => {
			return $scope.messageBox.open(message, 'success')
		},
		/**
		 * Pops an alert styled messagebox
		 * @param {string} message to be shown in the box
		 */
		alert: (message) => {
			return $scope.messageBox.open(message, "alert");
		},
		/**
		 * Pops an info styled messagebox
		 * @param {string} message to be shown in the box
		 */
		info: (message) => {
			return $scope.messageBox.open(message, "info");
		},
		/**
		 * Pops an error styled messagebox
		 * @param {string} message to be shown in the box
		 */
		error: (message) => {
			return $scope.messageBox.open(message, "error");
		},
		/**
		 * Pops up a messagebox
		 * Can be typed to:
		 * * error
		 * * alert
		 * * info
		 * * success
		 * @param {string} message to be shown in the box
		 */
		open: (message, type) => {
			$scope.messageBox.message = message
			$scope.messageBox.type = type ? type : 'error'
			$scope.messageBox.hide = false

			return $scope.messageBox;
		}
	}
	/**
	 * Maintains app-bar/header functions, specification and instance
	 * TODO: DOCUMENT PROPERLY
	 */
	$scope.header = {
		title: "",
		class: "nav",
		hidden: true, //false, when the app has a top bar by default; true when not
		customText: null,
		mainIcon: 'menu', // settings.app.appBar.mainIcon,
		mainIconAction: null, // not implemented
		/**
		 * Sets app-bar background
		 * @param {string} background background color for the app-bar
		 * @param {string} color text color for the app-bar
		 */
		setBackgrond: function (background, color) {
			if (typeof StatusBar !== "undefined") {
				StatusBar.hide();
				StatusBar.backgroundColorByHexString(hexc(background));
			}
			$("." + $scope.header.class).css('background', background).css('color', color);
		},
		/**
		 * Keeps app-bar main icon definition
		 */
		icon: {
			/**
			 * Shows the backbutton
			 * @param {string} action function/action to be executed
			 */
			showBack: function (action) {
				$scope.header.setMainIcon('arrow_back', action ? action : function() {
					if (settings.devmode) console.log('Navigating back to previous route');
					return window.history.back()
				});
			}
		},
		/**
		 * Sets app-bar custom text
		 */
		setCustomText: function (text) {
			$scope.header.customText = text;
		},
		/**
		 * Sets app-bar main icon
		 * @param {string} icon the icon to be shown
		 * @param {string} fnAction action to be executed by the app bar icon click
		 */
		setMainIcon: function (icon, fnAction) {
			//debugger;
			$scope.header.mainIcon = icon;
			$scope.header.mainIconAction = fnAction;
		},
		/**
		 * Sets app-bar title
		 * @param {string} title title to be shown in the bar
		 */
		setTitle: function (title) {
			var translated;
			if (translated = i18n.getTranslated(title)) {
				$scope.header.title = translated;
				return;
			}
			$scope.header.title = title;
		},
		/**
		 * Resets the app-bar to the default definitions
		 * Used e.g. when switching application states (route to another route)
		 */
		default: function() {
			//In case a developer is testing
			if (settings.app.devmode) {
				console.log("Triggered appBar.default() function");
				console.log("Default app-bar visibility setting - hidden: " + settings.app.appBar.hidden);
			}
			//Sets the app bar title default
			$scope.header.setCustomText(null);
			$scope.header.setTitle(settings.app.appBar.title);
			$scope.header.actions.clear();
			//Default icon setting, opens lateral menu (app-drawer)
			$scope.header.setMainIcon(settings.app.appBar.mainIcon, function() { $scope.appDrawer.open() });
			//$scope.header.setBackgrond($("." + $scope.header.class).css("backgroundColor"), $("." + $scope.header.class).css("color"));
			$scope.header.tabs.clear();
			//Depending on configuration, hides or shows the appbar by default
			if (settings.app.appBar.hidden)
				$scope.header.hide();
			else
				$scope.header.show();
		},
		/**
		 * Shows the app-bar
		 */
		show: function () {
			//In case a developer is testing
			if (settings.app.devmode) {
				console.log("Triggered appBar.show() function");
				console.log("Default app-bar visibility setting - hidden: " + settings.app.appBar.hidden);
			}
			$scope.header.hidden = false;
		},
		/**
		 * Hides the app-bar
		 */
		hide: function () {
			//In case a developer is testing
			if (settings.app.devmode) {
				console.log("Triggered appBar.hide() function");
				console.log("Default app-bar visibility setting - hidden: " + settings.app.appBar.hidden);
			}
			$scope.header.hidden = true;
		},
		/**
		 * Access to actions defined in the app-bar functionality
		 * Items of type:
		 * {
		 * 		name: 'name',
		 * 		icon: 'icon',
		 * 		click: function () {},
		 * 		items: [],
		 * 		attributes: []
		 * }
		 */
		actions: {
			open: false,
			list: [],
			/**
			 * Clears the app-bar action list
			 */
			clear: function () {
				$scope.header.actions.list = [];
			},
			/**
			 * Sets the app-bar action list at the app-bar
			 */
			set: function (actions) {
				$scope.header.actions.clear();

				for (i in actions) {
					$scope.header.actions.add(actions[i].name, actions[i].icon, actions[i].click, actions[i].items, actions[i].attributes)
				}
			},
			/**
			 * Adds a single app-bar action to the app-bar
			 */
			add: function (name, icon, click, items, attributes) {
				if (typeof icon == 'function') {
					attributes = items;
					items = click;
					click = icon;
					icon = undefined;
				}
				$scope.header.actions.list.push($scope.header.actions.create(name, icon, click, items, attributes));
			},
			/**
			 * Removes an app-bar action by its id
			 */
			remove: function (id) {
				for (i in $scope.header.actions.list) {
					if ($scope.header.actions.list[i].id == id) {
						$scope.header.actions.list.splice(i, 1);
						break;
					}
				}
			},
			/**
			 * Creates an app-bar action object with the specified params
			 */
			create: function (name, icon, click, items, attributes) {
				return { name: name, icon: icon, click: click, items: items, attributes: attributes };
			}
		},
		/**
		 * Access to app-bar tabs
		 * Items of type:
		 * {
		 *		label: '',
		 *		icon: '',
		 *		contentId: '',
		 *		active: bool
		 * }
		 */
		tabs: {
			list: [],
			visible: true,
			centered: false,
			/**
			 * Centres app-bar tab collection
			 */
			center: function () {
				$scope.header.tabs.centered = true;
			},
			/**
			 * Decentres app-bar tab collection
			 */
			uncenter: function () {
				$scope.header.tabs.centered = false;
			},
			/**
			 * Adds an app-bar tab pane to the collection
			 */
			add: function (label, icon, active, hideLabel) {
				this.list.push({
					label: hideLabel ? '' : i18n.getTranslated(label),
					icon: icon,
					contentId: label.split('.').slice(-1)[0],
					active: active
				});
			},
			/**
			 * Removes an app-bar tab pane based on its label
			 * @param {string} label label of the tab pane to be removed
			 */
			remove: function (label) {
				for (var i = 0; i < this.list.length; i++) {
					if (this.list[i].contentId == label) {
						this.list.shift(i, 1);
						return true;
					}
				}
				return false;
			},
			/**
			 * Clears the app-bar tab-list
			 */
			clear: function () {
				this.list = [];
				this.centered = false;
			},
			/**
			 * Hides the app-bar tab-list
			 */
			hide: function () {
				this.visible = false;
			},
			/**
			 * Shows the app-bar tab-list
			 */
			show: function () {
				this.visible = true;
			}
		}
	}
	/**
	 * Keeps data related to the user logged in
	 * TODO: DOCUMENT PROPERLY
	 */
	$scope.appDrawer = {
		hidden: true, //Default is not to be shown
		profilePicture: null,
		userName: null,
		userEmail: null,
		set: {
			/**
			 * Sets the profile picture
			 */
			picture: function (url) { $scope.appDrawer.profilePicture = url },
			/**
			 * Sets the user name
			 */
			name: function (name) { $scope.appDrawer.userName = name },
			/**
			 * Sets the user email
			 */
			email: function (email) { $scope.appDrawer.userEmail = email }
		},
		/**
		 * Closes the app drawer
		 */
		close: function () {
			$scope.appDrawer.hidden = true
			if (settings.devmode) console.log('Triggered app-drawer close. Hidden: ' + $scope.appDrawer.hidden)
		},
		/**
		 * Opens the app drawer
		 */
		open: function () {
			$scope.appDrawer.hidden = false
			if (settings.devmode) console.log('Triggered app-drawer open. Hidden: ' + $scope.appDrawer.hidden)
		}
	}

	//Sets the router and routes table to the $scope
	$scope.router = $rootScope.router = settings.router;
	$scope.routes = $rootScope.routes = settings.router.custom;

	/**
	 * Navigates to the default route.
	 * It invokes: $scope.navigate($scope.router.default_route)
	 */
	$scope.navigateToDefault = function () {
		if (settings.devmode) console.log("Navigating to default route.")
		$scope.navigate($scope.router.default_route);
	}

	//Configures user data to be displayed, now!
	$scope.prepareUserInfo()

	/**
	 * When any of the controllers starts loading
	 */
	$rootScope.$on("axis:resolve:controller_loading:start", function () {
		/**
		 * Starts the Sentry Client
		 * If:
		 * * settings.app.service.sentry.active == true
		 * * AND settings.devmode == true
		 *
		 * OBS.1: The settings.app.service.sentry property is the reference to the Sentry thread monitorer
		 */
		if (settings.app.services.sentry.active && !settings.devmode)
			Raven.config(settings.app.services.sentry.url).install({
				environment: window.location.hostname,
			});

		////Sets the app bar back to defaults
		////TODO: OK OR ANYTHING ELSE HERE? MOVED TO controller load finish
		//$scope.header.default();

		//Goes to the document top
		document.body.scrollTop = 0;

		//TODO: Remove this workaround from here
		$('#modal-date-picker').hide();
	});

	/**
	 * When any of the controllers finishes loading
	 */
	$rootScope.$on("axis:resolve:controller_loading:finish", function () {
		//Sets the app bar back to defaults
		$scope.header.default();
	});
}]);
