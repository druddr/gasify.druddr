/**
 * "loginForm" Directive behaviour file
 *
 * Including dependencies that should be available at this instance
 *
 * Nb.1: For this directive to work, it should be imported globally at "./app/app.js"
 * 	Or at the controller which will use this component
 * Nb.2: The object "scope" includes which attributes are going to be usable from the directive tag
 *
 * Usage:
 * 	<login-form ng-model="sample.object"></login-form>
 *
 * @author bruno.teixeira.silva
 * @version 1.0.0
 */
angular.module("Axis-Dependency-Injection").load.directive("loginForm", [
	'$rootScope',
	'UserService',
	'Session',
	function ($rootScope, UserService, Session) {
		return {
			replace: true,
			restrict: 'E',
			scope: {
				ngModel: '='
			},
			controller: [
				'$scope', function ($scope) { }
			],
			link: function (scope, elem, attr, ctrl) {
				scope.language = $rootScope.language.instance
				scope.errors = []
				/**
				 * Controls visibility of the message-box
				 */
				scope.showErrors = () => { scope.errors && scope.errors.length > 0 }

				//Keeps the editable properties of this component
				scope.loginForm = {
					email: "",
					password: "",
					rememberMe: true
				}

				//Redirects the user to registration page
				scope.userRegister = function () {
					if (!angular.isFunction(scope.$parent.userRegister))
						throw new DOMException("This component requires a scope.$parent.userRegister function which redirects to user registration.");

					scope.$parent.userRegister();
				}

				//Redirects the user to registration page
				scope.userLostPassword = function () {
					if (!angular.isFunction(scope.$parent.userLostPassword))
						throw new DOMException("This component requires a scope.$parent.userLostPassword function which redirects to user registration.");

					scope.$parent.userLostPassword();
				}

				//Submits the form for authentication
				scope.loginFormSubmit = function () {
					//Clears errors
					scope.errors = []

					//If the scope variable is available
					if (undefined !== scope.$parent.loggingIn)
						//Sets the logging in flag
						scope.$parent.loggingIn = true;

					//Tries to authenticate the user
					/**
					 * Tries to authenticate the user
					 * Sends the following JSON model:
					 * 	{
					 * 		email: "string",
					 * 		password: "string"
					 * 	}
					 */
					//UserService.authenticate({}, { email: scope.loginForm.email, password: scope.loginForm.password}).$promise.then(function (uAuth) {
					$rootScope
						.auth()
						.$signInWithEmailAndPassword(scope.loginForm.email, scope.loginForm.password)
						.then(function(user) {
							//Sets whether to maintain user data logged in
							user.rememberMe = scope.loginForm.rememberMe;

							var token = user.getToken()

							var u = {
								token: token,
								rememberMe: true,
								user: {
									id: token,
									email: user.email,
								},
								userProfile: {
									name: user.displayName,
									picture: user.photoURL
								}
							}

							//Logs the user setting the session object
							scope.setLoggedUser(user);
						})
						.catch(function(error) {
							debugger;

							//In case of error
							if (error) {
								//If the scope variable is available
								if (scope.$parent.loggingIn) //Stops the logging in
									scope.$parent.loggingIn = false;

								//Defines the action to be done with the errror
								if (error.code) {
									////If somehow the process failed completely
									//Raven.captureMessage('[login-form][loginFormSubmit]', {
									//	level: 'error',
									//	extra: { error: uAuth },
									//});
									//scope.errors.push(scope.language.exceptions[error.code]);
									$scope.messageBox.error(scope.language.exceptions[error.code]);
								} else {
									//Unknown error
									if (settings.devmode && settings.debug)
										console.log(error.code + ': ' + error.message);
								}

								//Stops process, given the error
								return false;
							}
						});
				}

				/**
				 * Sets the logged user info
				 * @param {*} oAuthUser the user authenticated object, which should contain:
				 * 						{
				 * 							user: <obj()>,
				 * 							rememberMe: <bool>
				 * 						}
				 */
				scope.setLoggedUser = function (oAuthUser) {
					//Logs the user setting the session object
					Session.define(oAuthUser, function (session) {
						//TODO: check whether anything else should be done in here
					})

					//Stops the screen blockage, in case the variable exists at the parent
					if (undefined !== scope.$parent.loggingIn)
						scope.$parent.loggingIn = false;

					//Keeps it in the scope for reusage
					scope.ngModel = oAuthUser;

					return scope.$parent.navigateToDefault()
				}
			},
			templateUrl: settings.path.components + 'login/form/template.html'
		}
	}
])
