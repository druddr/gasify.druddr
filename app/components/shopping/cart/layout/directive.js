/**
 * "shoppingCartLayout" Directive behaviour file
 *
 * Including dependencies that should be available at this instance
 *
 * Nb.1: For this directive to work, it should be imported globally at "./app/app.js"
 * 	Or at the controller which will use this component
 * Nb.2: The object "scope" includes which attributes are going to be usable from the directive tag
 *
 * Usage:
 * 	<shopping-cart-layout ng-model="sample.object"></shopping-cart-layout>
 *
 * @author bruno.teixeira.silva
 * @version 1.0.0
 */
angular.module("Axis-Dependency-Injection").load.directive("shoppingCartLayout", [
	'$rootScope',
	function ($rootScope) {
		var shoppingCartLayout = {
			restrict: 'E',
			replace: true,
			scope: {
				visible: '='
			},
			link: function (scope, elem, attr, ctrl) {
				scope.visible = undefined !== scope.visible ? scope.visible : false
				scope.modal = {}
				scope.shoppingCart = {
					errors: [],
					quantity: 20,
					minimumQuantity: 20,
					creditCard: {
						number: null,
						goodThru: null,
						verifier: null
					}
				}

				/**
				 * Keeps watching for changes at the modal visibility
				 * flag
				 */
				scope.$watch('visible', (newValue, oldValue) => {
					if (newValue) scope.modal.openModal()
					else scope.modal.closeModal()
				})

				/**
				 * Defines the modal reference to be used
				 * @param {object} modalRef
				 */
				scope.getModalRef = function (modalRef) {
					scope.modal = modalRef;
					//$scope.myModal.toggleModal();
					//$scope.myModal.closeModal();
					//$scope.myModal.openModal();
				}

				/**
					 * Confirms a credits purchase
					 */
				scope.buyCredits = function () {
					//Clears error collection
					scope.shoppingCart.errors = [];

					//Builds the credit request object
					var creditRequestObject = {
						valor: scope.shoppingCart.quantity,
						numeroCartao: scope.shoppingCart.creditCard.number,
						vencimentoCartao: scope.shoppingCart.creditCard.goodThru,
						verificadorCartao: scope.shoppingCart.creditCard.verifier
					}

					/**
					 * Requests credit purchase
					 */
					CreditService.post({}, creditRequestObject).$promise.then(
						result => {
							//No success at buying more credits
							if (!result.success) {
								if (result.message) {
									scope.shoppingCart.errors.push(
										result.message
									);
								}

								return false;
							}

							scope.modal.closeModal();
						}
					);
				};
			
			
			},
			templateUrl: settings.path.components + 'shopping/cart/layout/template.html'
		}

		return shoppingCartLayout;
	}
])