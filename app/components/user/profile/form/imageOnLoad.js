angular.module("Axis-Dependency-Injection").load.directive('imageonload', function() {
	return {
		restrict: 'A',
		link: function(scope, element, attrs) {
			// image load event
			element.bind('load', function() {
				// gets the height and width
				var height = element.height(),
					width = element.width();

				if (height > width) {
					// if more height = higher (should bind to higher class)
					element.addClass('higher');
					try {
						element.removeClass('wider');
					} catch (e) {
						// console.error(e);
					}
				} else {
					// if more width = wider (should bind to wider class)
					element.addClass('wider');
					try {
						element.removeClass('higher');
					} catch (e) {
						// console.error(e);
					}
				}
			});
			element.bind('error', function() {});
		}
	};
});
