/**
 * "userProfileForm" Directive behaviour file
 *
 * Including dependencies that should be available at this instance
 *
 * Nb.1: For this directive to work, it should be imported globally at "./app/app.js"
 * 	Or at the controller which will use this component
 * Nb.2: The object "scope" includes which attributes are going to be usable from the directive tag
 *
 * Usage:
 * 	<user-profile-form ng-model="sample.object"></user-profile-form>
 *
 * @author bruno.teixeira.silva
 * @version 1.0.0
 */
angular.module("Axis-Dependency-Injection").load.directive("userProfileForm", [
	'$rootScope',
	'TeamService',
	'UserProfileService',
	'Session',
	function ($rootScope, TeamService, UserProfileService, Session) {
		return {
			replace: true,
			restrict: 'E',
			scope: {
				ngModel: '=',
				userId: '=',
				form: '=?'
			},
			link: function(scope, elem, attr, ctrl) {
				//Sets up the language instance ready for use
				scope.language = $rootScope.language.instance;
				scope.errors = [];
				scope.emptyTeamItem = { name: "", id: null }

				//In case no user has started a profile creation
				if (!Session.current.user) return $rootScope.navigateToDefault()

				/**
				 * Picks country-states collection from language file
				 */
				scope.stateChoices = scope.language.states;

				/**
				 * Scope info aggregator object
				 * TODO: review front-end input ng-models
				 * TODO: check how the object is going to come out of the new API
				 */
				scope.profileForm = {
					email: Session.current.user.email,
					cpf: Session.current.userProfile.cpf,
					nome: Session.current.userProfile.nome,
					dataNascimento: new Date(Session.current.userProfile.dataNascimento),
					telefone: Session.current.userProfile.telefone,
					estado: Session.current.userProfile.estado,
					idTime: !Session.current.team ? 1 : Session.current.team.id, //TODO: remove this and make it load from the
				}

				//TODO: remove this Workaround which is for testing only
				scope.profileForm.dataNascimento.setHours(12)

				/**
				 * Loads the team list
				 */
				scope.loadTeams = () => {
					//Clears the list (there's an error), sets the list default
					scope.teamChoices = [scope.emptyTeamItem]

					/**
					 * Requests teams list
					 */
					TeamService.get({},{}).$promise.then((result) => {
						//Request process failure
						if (!result.success) {
							//Error message available
							if (result.message) {
								scope.errors.push(result.message)
							}

							return false;
						}

						//Sets the resulting data as response
						result.data.forEach((team, index) => scope.teamChoices.push({ text: team.nomePopular, id: team.idTime }))

						console.log(scope.teamChoices)
					})
				}

				/**
				 * Submits user profile data
				 * JSON data:
				 * 	{
				 *		email: "",
				 *		cpf: "",
				 *		nome: "",
				 *		dataNascimento: "",
				 *		telefone: "",
				 *		estado: "", //TODO: SENT BUT NOT USED
				 *		idTime: "", //TODO: SENT BUT NOT USED
				 * 	}
				 */
				scope.profileFormSubmit = () => {
					//Clears any possible errors
					scope.errors = [];

					////It is compulsory agreeing to the terms
					//if (!scope.profileForm.aceitouOsTermos) {
					//	scope.errors.push(scope.language.profile.errorTermsPolicy);
					//	return false;
					//}

					// Sends the data for storage at the profile info
					UserProfileService.update({}, scope.profileForm).$promise.then((oProfile) => {
						debugger;
						//In case of error
						if (!oProfile.success) {
							//And message is present
							if (oProfile.message) {
								//Displays the error
								scope.errors.push(oProfile.message);
							}

							//Stops the process anyway
							return false;
						}
						//TODO: study best practices after user profile edited
						scope.ngModel = oProfile.userProfile;
						//Remember this was set one screen before (if the user should be remembered)
						//Session.rememberUser(oUser.rememberMe)
						//Sets the current user on the user session object
						//It means it is already authenticated
						Session.define(oProfile, function(session) {
							$rootScope.navigateToDefault()
						})

						//Drives the user to the profile creation
						//scope.$parent.navigate(scope.$parent.router.default_route);
						//$rootScope.navigate($rootScope.router.default_route);
					});
				}

				/**
				 * Initializes the scope
				 */
				scope.init = () => {
					//Loads collections
					scope.loadTeams()
				}

				scope.init()
			},
			templateUrl: settings.path.components + 'user/profile/form/template.html'
		}
	}
])
