﻿/**
 * "userRegisterProfileForm" Directive configuration file
 *
 * Usage:
 * <user-register-profile-form></user-register-profile-form>
 *
 * Nb.1: For this directive to work, it should be imported globally at "./app/app.js"
 * 	Or at the controller which will use this component
 *
 * @property "directives":
 * 	Is which directives should be configured within this one
 *
 * @property "services":
 * 	Services which will be used by this directive
 *
 * @author bruno.teixeira.silva
 * @version 1.0.0
 */
angular.module("Axis-Dependency-Injection").components.config('user-register-profile-form', {
	directives: [
		'directive'
	],
	services: [
		'UserService',
		'UserProfileService'
	]
});
