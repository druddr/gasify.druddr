/**
 * "userRegisterProfileForm" Directive behaviour file
 *
 * Including dependencies that should be available at this instance
 *
 * Nb.1: For this directive to work, it should be imported globally at "./app/app.js"
 * 	Or at the controller which will use this component
 * Nb.2: The object "scope" includes which attributes are going to be usable from the directive tag
 *
 * Usage:
 * 	<user-register-profile-form ng-model="sample.object"></user-register-profile-form>
 *
 * @author bruno.teixeira.silva
 * @version 1.0.0
 */
angular.module("Axis-Dependency-Injection").load.directive("userRegisterProfileForm", [
	'$rootScope',
	'UserService',
	'UserProfileService',
	'Session',
	function($rootScope, UserService, UserProfileService, Session) {
		return {
			replace: true,
			restrict: 'E',
			scope: {
				ngModel: '=',
				userId: '=',
				form: '=?'
			},
			link: function(scope, elem, attr, ctrl) {
				//Sets up language to be used for the component
				scope.language = $rootScope.language.instance;
				scope.errors = [];

				//In case no user has started profile creation
				if (!Session.current.user) $rootScope.navigateToDefault()

				/**
				 * Picks country-states collection from language file
				 */
				scope.stateChoices = scope.language.states;

				/**
				 * TODO: load team choices collection from API
				 */
				scope.teamChoices = [
					{
						name: "",
						id: null
					},
					{
						name: "Juventude",
						id: 1
					}
				]

				/**
				 * Scope info aggregator object
				 * TODO: review front-end input ng-models
				 * TODO: check how the object is going to come out of the new API
				 */
				scope.profileForm = {
					cpf: null, //Session.current.userProfile.cpf,
					nome: null, //Session.current.userProfile.name,
					email: Session.current.user.email,
					senha: Session.current.user.password,
					dataNascimento: null, //new Date(),
					telefone: null,
					estado: scope.stateChoices[0].id,
					idTime: scope.teamChoices[0].id,
					aceitouOsTermos: (Session.current.user && Session.current.user.readTerms)
				}

				/**
				 * Submits user signup data
				 * JSON data:
				 * 	{
				 *		cpf: "",
				 *		nome: "",
				 *		email: "",
				 *		senha: "",
				 *		dataNascimento: "",
				 *		telefone: "",
				 *		estado: "", //TODO: SENT BUT NOT USED
				 *		idTime: "", //TODO: SENT BUT NOT USED
				 * 	}
				 */
				scope.profileFormSubmit = function() {
					//Clears any possible errors
					scope.errors = [];

					//It is compulsory agreeing to the terms
					if (!scope.profileForm.aceitouOsTermos) {
						scope.errors.push(scope.language.profile.errorTermsPolicy);
						return false;
					}

					//This is a workaround - since we consider timezone differences,
					//It is necessary to add 12 hours in order not to mess with the birthdays
					//When a user signs up to the system
					if (scope.profileForm.dataNascimento)
						scope.profileForm.dataNascimento.setHours(12);

					// Sends the data for storage at the profile info
					UserService.create({}, scope.profileForm).$promise.then(function(oProfile) {
						debugger;
						//In case of error
						if (!oProfile.success) {
							//And message is present
							if (oProfile.message) {
								//Displays the error
								scope.errors.push(oProfile.message);
							}

							//Stops the process anyway
							return false;
						}
						scope.ngModel = oProfile.userProfile;
						//Remember this was set one screen before (if the user should be remembered)
						//Session.rememberUser(oUser.rememberMe)
						//Sets the current user on the user session object
						//It means it is already authenticated
						Session.define(oProfile, function(session) {
							$rootScope.navigateToDefault()
						})

						//Drives the user to the profile creation
						//scope.$parent.navigate(scope.$parent.router.default_route);
						//$rootScope.navigate($rootScope.router.default_route);
					});
				}
			},
			templateUrl: settings.path.components + 'user/register/profile/form/template.html'
		}
	}
])
