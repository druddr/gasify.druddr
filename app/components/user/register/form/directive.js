/**
 * "userRegisterForm" Directive behaviour file
 *
 * Including dependencies that should be available at this instance
 *
 * Nb.1: For this directive to work, it should be imported globally at "./app/app.js"
 * 	Or at the controller which will use this component
 * Nb.2: The object "scope" includes which attributes are going to be usable from the directive tag
 *
 * Usage:
 * 	<user-register-form ng-model="sample.object"></user-register-form>
 *
 * @author bruno.teixeira.silva
 * @version 1.0.0
 */
angular.module("Axis-Dependency-Injection").load.directive("userRegisterForm", [
	'$rootScope',
	'UserService',
	'Session',
	function($rootScope, UserService, Session) {
		return {
			replace: true,
			restrict: 'E',
			scope: {
				ngModel: '=',
				// userId: '=',
				// form: '=?'
			},
			link: function(scope, elem, attr, ctrl) {
				//Sets the scope language and error collection
				scope.language = $rootScope.language.instance
				scope.errors = []

				//Keeps the properties introduced through the component
				scope.registerForm = {
					email: "",
					emailConfirmation: "",
					password: "",
					readTerms: false,
					rememberMe: true,
				}

				//Submits the registration form for creation
				scope.registerFormSubmit = function () {
					//Clears up errors array
					scope.errors = [];
					let emailConfirmationIsValid = scope.registerForm.emailConfirmation === scope.registerForm.email;

					// raise email comparison error / Policy needed error
					if (!emailConfirmationIsValid) {
						scope.errors = [];
						scope.errors.push(scope.language.register.errorEmailConfirmation);

						return false;
					}

					// raise Terms / Policy needed error
					if (!scope.registerForm.readTerms) {
						scope.errors = [];
						scope.errors.push(scope.language.register.errorTermsPolicy);

						return false;
					}

					/**
					 * Checks whether the informed e-mail is available
					 * Sends on the following JSON:
					 * {
					 * 		email: scope.registerForm.email
					 * }
					 * /
					UserService.checkEmailAvailable({}, { email: scope.registerForm.email }).$promise.then(function (objectsReturned) {
						//In case of error
						if (!objectsReturned.success) {
							//Logs the message at the screen
							if (objectsReturned.message) {
								scope.errors.push(objectsReturned.message)
							}

							return false;
						}

						//TODO: Place all the Session definition (below) code in here
					});
					*/

					/**
					 * Saves pre subcription (user profile) data at local storage
					 * Uses the following JSON model:
					 * {
					 * 	email: "string",
					 * 	emailConfirmation: "string",
					 * 	password: "string",
					 * 	readTerms: true
					 * }
					 */
					Session.define({ user: scope.registerForm }, function(sessionObject){
						//Drives the user to the profile info screen
						scope.$parent.navigate(scope.$parent.routes.user.profile);

						return true;
					});

					//Until no e-mail verification has come
					//TODO: remove this return (including email verification)
					return;
				}
			},
			templateUrl: settings.path.components + 'user/register/form/template.html'
		}
	}
])
