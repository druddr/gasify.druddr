/**
 * "userLostPasswordForm" Directive behaviour file
 *
 * Including dependencies that should be available at this instance
 *
 * Nb.1: For this directive to work, it should be imported globally at "./app/app.js"
 * 	Or at the controller which will use this component
 * Nb.2: The object "scope" includes which attributes are going to be usable from the directive tag
 *
 * Usage:
 * 	<user-lost-password-form ng-model="model.reference"></user-lost-password-form>
 *
 * @author bruno.teixeira.silva
 * @version 1.0.0
 */
angular.module("Axis-Dependency-Injection").load.directive("userLostPasswordForm", [
	'$rootScope', 'UserService',
	function($rootScope, UserService) {
		return {
			replace: true,
			restrict: 'E',
			scope: {
				ngModel: '='
			},
			link: function(scope, elem, attr, ctrl) {
				scope.language = $rootScope.language.instance;
				scope.lostPasswordForm = {
					email: ''
				};
				scope.errors = []
				scope.formSubmitting = false
				scope.formSubmitted = false

				/**
				 * Redirects the scope to the default
				 */
				scope.redirectToDefault = function () {
					$rootScope.navigateToDefault();
				}

				/**
				 * Method which submits the password request
				 */
				scope.lostPasswordFormSubmit = function() {
					//Hides the form, clears error buffer
					scope.formSubmitting = true;
					scope.errors = [];

					/**
					 * Begins a request to the API
					 * @argument object lostPasswordForm will receive the following JSON model:
					 * 		{
					 * 			email: <string>
					 * 		}
					 *
					 * @callback object oResult will contain the following JSON model:
					 * 		{
					 * 			success: <bool>,
					 * 			message: <string>
					 * 		}
					 */
					UserService.lostPasswordRequest({}, scope.lostPasswordForm).$promise.then(function (oResult) {
						//Unsuccessful request
						if (!oResult.success) {
							//Message available
							if (oResult.message) {
								scope.errors.push(oResult.message);
							}

							//Shows the form again
							scope.formSubmitting = false;
							return false;
						}

						scope.formSubmitting = false;
						scope.formSubmitted = true;

						//Goes to the default route
						$rootScope.navigateToDefault();
					});
				};
			},
			templateUrl: settings.path.components + 'user/lost-password/form/template.html'
		}
	}
])
