/**
 * "userRedefinePasswordForm" Directive behaviour file
 *
 * Including dependencies that should be available at this instance
 *
 * Nb.1: For this directive to work, it should be imported globally at "./app/app.js"
 * 	Or at the controller which will use this component
 * Nb.2: The object "scope" includes which attributes are going to be usable from the directive tag
 *
 * Usage:
 * 	<user-redefine-password-form password-request-token="<token-reference>"></user-redefine-password-form>
 *
 * @author bruno.teixeira.silva
 * @version 1.0.0
 */
angular.module("Axis-Dependency-Injection").load.directive("userRedefinePasswordForm", [
	'$rootScope',
	'PasswordRequestService',
	function ($rootScope, PasswordRequestService) {
		return {
			replace: true,
			restrict: 'E',
			scope: {
				passwordRequestToken: '='
			},
			link: function(scope, elem, attr, ctrl) {
				scope.language = $rootScope.language.instance;
				scope.errors = []
				scope.redefinePasswordForm = {
					token: scope.passwordRequestToken,
					password: "",
					passwordConfirmation: ""
				};

				//  TODO: Validate Token
				// this.$onInit = function() {
				// 	console.log('validate redefine password token');
				// 	scope.validToken = true;
				// }.bind(this);

				scope.validToken = true;
				scope.formSubmitting = false;

				/**
				 * Submits the form and executes password change
				 */
				scope.redefinePasswordFormSubmit = function() {
					//Safe copy of the object, clears the error cluster
					var form = scope.redefinePasswordForm;
					scope.errors = []

					//Password and confirmation matches?
					if (!form.password === form.passwordConfirmation) {
						scope.errors.push(scope.language.redefinePassword.passwordComparisonError);

						return false;
					}

					//TODO: unwrap reset-password
					//PasswordRequestService.resetPassword({}, form).$promise.then(
						(
						function (oResult) {
							if (!oResult.success) {
								switch (oResult.message) {
									case "passwordConfirmationMismatch":
										scope.errors.push(scope.language.exceptions.passwordConfirmationMismatch);
										break;
									case "invalidPasswordRequest":
										console.log(scope.language.exceptions.invalidPasswordRequest);
										$rootScope.navigate($rootScope.router.authentication);
										break;
									default:
										console.log(oResult)
										scope.errors.push(scope.language.exceptions.generalError);
										break;
								}
							}

							window.location.hash = "#" + settings.router.authentication;
						}).call(scope, { success: true, status: 200 })
					//).catch(function(error) {
					//	scope.formSubmitting = true;
					//});

				}.bind(this);
			},
			templateUrl: settings.path.components + 'user/redefine-password/form/template.html'
		}
	}
])
