/*
 * Message modal
 *
 * A simple messages modal
 * Renders a message with a color and buttons, if passed
 * Types:
 * * null (default)
 * * alert
 * * error
 * * success
 * * info
 *
 * Example of usage:
 * <d-message
 * 		type="error"
 * 		message="Example of usage"
 * 		toggle-callback="visibleWhen()"
 * 		button-text="Close"
 * 		button-action="null">
 * </d-message>
 *
*/
//angular.module("Axis-Dependency-Injection").load.directive("dMessage", [ //Not to be injected at the dependency injector
angular.module("System").directive("dMessage", [
	'$rootScope',
	function ($rootScope) {
		var dMessage = {
			replace: true,
			restrict: 'E',
			scope: {
				'toggleCallback': '=',
				'title': '=',
				'message': '=',
				'type': '=',
				'buttonAction': '&?',
				'buttonText': '=?',
				'hide': '=?'
				//'previousText': '@',
			},
			link: function (scope, elem, attr, ctrl) {
				scope.language = $rootScope.language.instance;
				scope.hide = scope.hide || true;
				scope.defaultText = 'no-text';
				scope.defaultType = 'success';

				/**
				 * Button properties object mapping
				 */
				scope.button = {
					action: angular.isDefined(scope.buttonAction) ? () => scope.buttonAction : () => scope.close(),
					text: angular.isDefined(scope.buttonText) ? scope.buttonText : scope.defaultText
				}

				/**
				 * In case the message is actually an array of messages
				 * Displays as a list of items
				 */
				if (angular.isArray(scope.message)) {
					//Builds an error list
					scope.message.unshift('<ul>')
					scope.message = scope.message.map(function(msg){ return "<li>" + msg + "</li>"; })
					scope.message.push('</ul>')
				}

				/**
				 * Opens the message-box
				 */
				scope.open = () => {
					/**
					 * Tries toggling using the passed function
					 */
					scope.toggleCallback();

					//Cancels the hiding effect
					scope.hide = false;
				}

				/**
				 * Closes the message-box
				 */
				scope.close = () => {
					/**
					 * Tries toggling using the passed function
					 */
					scope.toggleCallback();

					//Hides the message-box
					scope.hide = true;
				}

				/**
				 * Monitors for changes in message element
				 */
				scope.$watch('message', (newValue, oldValue) => {
					if (settings.devmode)
						console.log('message-box-message-changed');
				})

				/**
				 * Monitors for changes in message-box type
				 */
				scope.$watch('type', (newValue, oldValue) => {
					if (settings.devmode)
						console.log('message-box-type-changed');

					//Defines the message title, based on current scope.type
					switch (newValue) {
						case "info":
						case "alert":
						case "success":
						case "error":
							scope.title = scope.language.labels[newValue];
							break;
						default:
							scope.title = "no-title";
							break;
					}
				})

				//Resolves scope.type and sets it based on default versus informed
				scope.type = !scope.type ? scope.defaultType : scope.type;
			},
			templateUrl: settings.path.components + 'd/message/template.html'
		}

		return dMessage;
	}
])
