/**
 *
 */
angular.module("Axis-Dependency-Injection").load.directive("dList", [
	'$rootScope',
	function($rootScope) {
		//return {
		var dList = {
			replace: true,
			restrict: 'E',
			scope: {
				items: '=',
				titleProperty: "@",
				subtitleProperty: "@",
				orderByProperty: "@?",
				icon: "=?",
				type: "=?",
				action: '&?'
			},
			link: function(scope, elem, attr, ctrl) {
				//Defines the list type
				scope.type = undefined !== scope.type ? scope.type : 'single';

				/**
				 * Monitors for changes in the list item
				 */
				scope.$watch('items', function(newValue, oldValue) {
					//Checks whether the item collection should be empty
					scope.items = newValue && newValue.length > 0 ? newValue : []
				});
			},
			templateUrl: settings.path.components + 'd/List/template.html'
		}

		return dList;
	}
])
