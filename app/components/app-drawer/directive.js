/**
 * "appDrawer" Directive behaviour file
 *
 * Including dependencies that should be available at this instance
 *
 * Nb.1: For this directive to work, it should be imported globally at "./app/app.js"
 * 	Or at the controller which will use this component
 * Nb.2: The object "scope" includes which attributes are going to be usable from the directive tag
 *
 * Usage:
 * 	<app-drawer>
 *  </app-drawer>
 *
 * Where:
 * * mainActionObject = { mainIcon: 'sample', mainIconAction: function() { return false; } }
 * * actionsCollection = [ actions-list ]
 * * action = { icon: 'sample', click: function() { return false; } }
 *
 * @author bruno.teixeira.silva
 * @version 1.0.0
 */
angular.module("System").directive("appDrawer", [
	'$rootScope',
	function ($rootScope) {
		return {
			replace: true,
			restrict: 'E',
			scope: {
				ngModel: '=?',
				dataHidden: '&?',
				profileImage: '@?',
				userName: '@?',
				userEmail: '@?'
			},
			link: function(scope, elem, attr, ctrl) {
				//Controls visibility of the element
				scope.dataHidden = undefined !== scope.dataHidden ? scope.dataHidden : scope.$parent.appDrawer.hidden;
				scope.profileImage = !scope.profileImage ? null : scope.profileImage;
				scope.userName = !scope.userName ? null : scope.userName;
				scope.userEmail = !scope.userEmail ? null : scope.userEmail;

				/**
				 * Side menu items
				 */
				scope.items = [
					{
						icon: 'account_circle',
						text: 'labels.profile',
						action: () => {
							//Drives the user to its own profile edit
							scope.$parent.navigate($rootScope.routes.userProfile.edit)
							scope.$parent.appDrawer.close()
						}
					},
					{
						icon: 'payment',
						text: 'labels.payments',
						action: () => scope.$parent.appDrawer.close()
					},
					{
						icon: 'settings',
						text: 'labels.configuration',
						action: () => scope.$parent.appDrawer.close()
					},
					{
						icon: 'exit_to_app',
						text: 'labels.logout',
						action: () => {
							//Logs user out: bye bulldog!
							$rootScope.navigate($rootScope.routes.authentication.logout)
							scope.$parent.appDrawer.close()
						}
					}
				]

				/**
				 * Drawer closing function
				 */
				scope.close = function() {
					if (!angular.isFunction(scope.$parent.appDrawer.close)) throw new Error($rootScope.language.instance.exceptions.appDrawerCloseNotFound);
					//Invokes the parent app drawer close function
					scope.$parent.appDrawer.close();
				}

				/**
				 * Keeps watching for changes in app-drawer visibility
				 */
				scope.$watch('$parent.appDrawer.hidden', function (newValue, oldValue){
					if (settings.devmode) console.log('App-drawer hidden: ' + newValue);
					scope.dataHidden = newValue;
				})
			},
			templateUrl: settings.path.components + 'app-drawer/template.html'
		}
	}
]);
