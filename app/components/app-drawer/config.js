/**
 * "appDrawer" Directive configuration file
 *
 * Usage:
 * <app-drawer></app-drawer>
 *
 * Nb.1: For this directive to work, it should be imported globally at "./app/app.js"
 * 	Or at the controller which will use this component
 *
 * @property "directives":
 * 	Is which directives should be configured within this one
 *
 * @property "services":
 * 	Services which will be used by this directive
 *
 * @author bruno.teixeira.silva
 * @version 1.0.0
 */
//angular.module("System").components.config('app-drawer', {
angular.module("Axis-Dependency-Injection").components.config('app-drawer', {
	directives: ['directive'],
	services: []
});
