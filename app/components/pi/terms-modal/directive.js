/*
 * Terms modal
 *
 * A simple terms modal
 * Renders two links with one single modal to access terms text
 *
 * "piTermsModal" Directive behaviour file
 * 
 * Including dependencies that should be available at this instance
 * 
 * Nb.1: For this directive to work, it should be imported globally at "./app/app.js"
 * 	Or at the controller which will use this component
 * Nb.2: The object "scope" includes which attributes are going to be usable from the directive tag
 * 
 * Usage:
 * 	<pi-terms-modal link-text="text" link-class="text" previous-text="text" toggle-callback="function () {}"></pi-terms-modal>
 * 
 * @author bruno.teixeira.silva
 * @version 1.0.0
 */
angular.module("Axis-Dependency-Injection").load.directive("piTermsModal", [
	'$rootScope',
	function ($rootScope) {
		var termsModal = {
			replace: true,
			restrict: 'E',
			scope: {
				'linkText': '@',
				'linkClass': '@',
				'previousText': '@',
				'toggleCallback': '=?',
			},
			link: function (scope, elem, attr, ctrl) {
				scope.language = $rootScope.language.instance;

				scope.linkText = attr.linkText || "";
				scope.linkClass = attr.linkClass || "";
				scope.previousText = attr.previousText || "";

				scope.getTermsModalInstance = function (modal) {
					scope.toggleModal = modal.toggleModal;
				}
			},
			templateUrl: settings.path.components + 'pi/terms-modal/template.html'
		}

		return termsModal;
	}
])
