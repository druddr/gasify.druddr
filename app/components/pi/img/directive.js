/**
 * "piImg" Directive behaviour file
 * 
 * Including dependencies that should be available at this instance
 * 
 * Nb.1: For this directive to work, it should be imported globally at "./app/app.js"
 * 	Or at the controller which will use this component
 * Nb.2: The object "scope" includes which attributes are going to be usable from the directive tag
 * 
 * Usage:
 * 	<pi-img name="path/to/image" extension="png" />
 * 
 * @author bruno.teixeira.silva
 * @version 1.0.0
 */
angular.module("Axis-Dependency-Injection").load.directive("piImg", [
	'$rootScope',
	function($rootScope) {
		return {
			replace: true,
			restrict: 'E',
			scope: {
				name: '=',
				extension: '='
			},
			link: function(scope, elem, attr, ctrl) {},
			templateUrl: function(elem, attr) {
				return 'assets/img/' + attr.name + '.' + attr.extension
			}
		}

	}
])
