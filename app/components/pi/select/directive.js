/*
How should it work:
Get an array with option
Each options follows this pattern:
{
  id: "",
  text: "",
  checked: true | false (default is false)
}
*/
angular.module("Axis-Dependency-Injection").load.directive("piSelect", [
	'$rootScope',
	function($rootScope) {
		//return {
		var piSelect = {
			replace: true,
			restrict: 'E',
			scope: {
				options: '=',
				optionsKey: "=",
				optionsText: "=",
				ngModel: '=?',
				ngRequired: '=',
				blank: '=',
				ngPlaceholder: '=?',
			},
			link: function(scope, elem, attr, ctrl) {
				scope.showOptions = false;
				scope.openUp = false;
				scope.placeholder = attr.placeholder
				scope.selectedOption = {}
				//naSelect.scope.elem = elem;

				//This configures click only with no typing to the input
				var readOnlyInput = elem[0].getElementsByClassName("readonly-input")[0];
				readOnlyInput.onkeydown = handleReadOnlyInputOnkeypress;
				readOnlyInput.onclick = handleReadOnlyInputOnClick;
				readOnlyInput.addEventListener('click', handleReadOnlyInputOnClick);

				/**
				 * Maps each list item for the appropriate format
				 * to be bound to the select-box
				 */
				scope.options = scope.options.map(function(value, index) {
					var obj = {};
					if (attr.optionsKey && attr.optionsText) {
						obj = {
							id: value[attr.optionsKey],
							text: value[attr.optionsText]
						};
					} else {
						obj = {
							id: value,
							text: value
						};
					}

					if (value.category) {
						obj['category'] = true;
					}

					return obj;
				});

				/**
				 * Shows the drop-down/up menu
				 * Corrects open-up / down based on screen height
				 */
				scope.show = function() {
					var offset = getScreenHeight() - (elem.height() + elem.offset().top + 250);
					if (offset < 0) {
						scope.openUp = true;
					}
					scope.showOptions = true
					scope.$apply();
				};

				/**
				 * Hides input related dropdown list
				 * @param {event} e
				 */
				scope.hide = function(e) {
					setTimeout(function() {
						scope.showOptions = false;
						scope.openUp = false;
						scope.$apply();
					}, 350) //Changed so after 350ms scope props are changed and applied to the DOM
				};

				/**
				 * Loads the dropdown/up menu and focus at the input
				 */
				scope.dropDownArrowClick = function () {
					//Shows dropdown/up menu
					scope.show();
					////Focus at the input
					//readOnlyInput.onclick();
				}

				function handleReadOnlyInputOnClick(e) {
					e.preventDefault();
					scope.show();
				}

				function handleReadOnlyInputOnkeypress(e) {
					e.preventDefault();
					if (e.keyCode === 32) {
						scope.show()
					}
				}

				//TODO: Moved to the controller (not a link function)
				scope.onSelectItem = function(option, e) {
					if (option.id) {
						//This will be used to display the option name
						scope.selectedOption = option
						scope.ngModel = option.id
						scope.hide(e)
					}
				};
			},
			templateUrl: settings.path.components + 'pi/select/template.html'
		}

		return piSelect;
	}
])

function getScreenHeight() {
	var g = document.getElementsByTagName('body')[0];
	return window.innerHeight || document.documentElement.clientHeight || g.clientHeight;
}
