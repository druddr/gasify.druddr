/**
 * "appBar" Directive behaviour file
 *
 * Including dependencies that should be available at this instance
 *
 * Nb.1: For this directive to work, it should be imported globally at "./app/app.js"
 * 	Or at the controller which will use this component
 * Nb.2: The object "scope" includes which attributes are going to be usable from the directive tag
 *
 * Usage:
 * 	<app-bar mainAction="mainActionObject" text="settings.app.title" actions="actionsCollection">
 *  </app-bar>
 *
 * Where:
 * * mainActionObject = { mainIcon: 'sample', mainIconAction: function() { return false; } }
 * * actionsCollection = [ actions-list ]
 * * action = { icon: 'sample', click: function() { return false; } }
 *
 * @author bruno.teixeira.silva
 * @version 1.0.0
 */
angular.module("System").directive("appBar", [
	'$timeout',
	function ($timeout) {
		return {
			replace: true,
			restrict: 'E',
			scope: {
				title: '=?',
				ngModel: '=?',
				mainIcon: '=?',
				mainIconAction: '&?',
				actions: '=?',
				dataHidden: '&?',
				customText: '=?',
				backgroundColor: '@?',
				color: '@?'
			},
			link: function(scope, elem, attr, ctrl) {
				//Controls visibility of the element
				scope.dataHidden = undefined !== scope.dataHidden ? scope.dataHidden : scope.$parent.header.hidden;

				/**
				 * Keeps watching for changes in header visibility
				 */
				scope.$watch('$parent.header.hidden', function (newValue, oldValue){
					if (settings.devmode) console.log('Header hidden: ' + newValue);
					scope.dataHidden = newValue;
				})
			},
			templateUrl: settings.path.components + 'app-bar/template.html'
		}
	}
]);
