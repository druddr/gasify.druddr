﻿/**
 * Password request angular controller
 * Base URL: #/app/password-request/[methods]
 *
 * Nb.1: This is used when the code is injected into the current state
 *
 * @property "components":
 * 	In order for some components to be useful here it depends on this mapping.
 * 	Some components might have already been imported globally at ./app/app.js
 *
 * @property "services":
 * 	In order for services to be "in sight" for this controller,
 *  They should be declared as a "service" inside this property.
 *
 * @author bruno.teixeira.silva
 * @version 1.0.0
 */
var PasswordRequest = (function () {
	var oPasswordRequest = {
		components: [
		],
		services: [
			'PasswordRequestService'
		],
		/// Route usage:
		/// #/app/password-request/check?t=<token>
		check: ['$scope', 'PasswordRequestService', function ($scope, PasswordRequestService) {
			//Context object and language
			$scope.token = { token: PasswordRequest.params.t };
			$scope.language = $scope.language.instance;
			//No header should be displayed here
			$scope.header.hide();

			/**
			 * Executes a check-up on that provided token
			 * TODO: unwrap password-request-check
			 */
			//PasswordRequestService.checkPasswordRequest({}, $scope.token).$promise.then(
				(
				function (oChecked) {
					if (!oChecked.success) {
						switch (oChecked.message) {
							case "invalidPasswordRequest":
								if (settings.devmode) console.log($scope.language.exceptions.invalidPasswordRequest);
								return $scope.navigate($scope.router.authentication);
								break;
							default:
								if (settings.devmode) console.log(oChecked);
								return $scope.navigate($scope.router.authentication);
								break;
						}
					}

					//Checks the http status received
					if (oChecked.status != 302) return false;

					//Drives the user to the reset password screen
					$scope.navigate(oChecked.url);
					//Call by using a test-wrapper
					}).call(this, { success: true, status: 302, url: $scope.routes.passwordRequest.resetPassword })
			//});
		}],
		/// Route usage:
		/// #/app/password-request/reset-password
		"reset-password": ['$scope', 'PasswordRequestService', function ($scope, PasswordRequestService) {
			//Context object
			$scope.passwordRequest = {
				token: PasswordRequest.params.t
			};
			//No header should be displayed here
			$scope.header.hide();
		}],
	}

	return oPasswordRequest;
})();
