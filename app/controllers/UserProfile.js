﻿/**
 * UserProfile angular controller
 * Base URL: #/app/user-profile/[methods]
 *
 * Nb.1: This is used when the code is injected into the current state
 *
 * @property "components":
 * 	In order for some components to be useful here it depends on this mapping.
 * 	Some components might have already been imported globally at ./app/app.js
 *
 * @property "services":
 * 	In order for services to be "in sight" for this controller,
 *  They should be declared as a "service" inside this property.
 *
 * @author bruno.teixeira.silva
 * @version 1.0.0
 */
var UserProfile = (function () {
	var oUserProfile = {
		components: [
			'user/profile/form'
		],
		services: [
			'UserService',
			'UserProfileService'
		],
		/// Route usage:
		/// #/app/user-profile/edit
		edit: [
			'$scope',
			'$rootScope',
			'UserService',
			'UserProfileService',
			'Session',
			function ($scope, $rootScope, UserService, UserProfileService, Session) {
				//Define language reference
				$scope.language = $rootScope.language.instance;
				$scope.header.setTitle('labels.yourProfile');
				$scope.header.icon.showBack();
				//Context object
				$scope.userProfile = Session.current.userProfile;

				/**
				 * Loads the whole user profile
				 */
				$scope.loadFullProfile = function() {

				}

				/**
				 * Saves the user profile
				 */
				$scope.saveFullProfile = function() {

				}
			}]
	}

	return oUserProfile;
})();
