var Dashboard = (function () {
	var oDashboard = {
		//Used components
		components: [
		],
		//Used services
		services: [
		],
		/// Route usage:
		/// #/app/dashboard/i
		i: [
			'$scope',
			'$rootScope',
			'$timeout',
			'Session',
			'NgMap',
			function ($scope, $rootScope, $timeout, Session, NgMap) {
				//Define language reference
				$scope.language = $rootScope.language.instance;
				//Configures the scope header
				$scope.header.setTitle('labels.dashboard')
				// $scope.header.setMainIcon('menu', )
				$scope.header.actions.list = [
					{ icon: "search", click: () => $scope.openSearch() },
					{ icon: "error", click: () => $scope.openErrorReporter() }];
				$scope.map = { center: [ 45, -73 ], zoom: 8 };
				$scope.suggestions = []
				// //Reusable modal
				// $scope.modal = {}

				// /**
				//  * Defines the modal reference to be used
				//  * @param {object} modalRef
				//  */
				// $scope.getModalRef = function (modalRef) {
				// 	$scope.modal = modalRef;
				// 	//$scope.myModal.toggleModal();
				// 	//$scope.myModal.closeModal();
				// 	//$scope.myModal.openModal();
				// }

				//--------------------------------------------------------------

				/**
				 * Opens search screen
				 */
				$scope.openSearch = function () {

				}

				/**
				 * Starts error reporter
				 */
				$scope.openErrorReporter = function () {

				}

				//--------------------------------------------------------------

				/**
				 * Initializable functions under scope
				 */
				$scope.init = function () {
					/**
					 * Promise for when the maps API was successfully loaded
					 */
					NgMap.getMap().then(function(map) {
						console.log('map-loaded')
					});

					$scope.suggestions = [
						{
							name: 'Posto Unisinos',
							fuelTypes: [
								{
									name: "GC",
									prices: [
										{ tag: 4.11, date: new Date() }
									]
								},
								{
									name: "GA",
									prices: [
										{ tag: 4.16, date: new Date() }
									]
								}
							]
						}
					]
				}

				$scope.init();

				//--------------------------------------------------------------
				// // Track user for errors
				// Raven.setUserContext({
				// 	email: Session.current.user.email,
				// 	id: Session.current.user.id
				// });
			}],
		// /// Route usage:
		// /// #/app/dashboard/purchases
		// purchases: [
		// 	'$scope',
		// 	'$rootScope',
		// 	'Session',
		// 	'PurchaseStatusService',
		// 	'PurchaseService',
		// 	function ($scope, $rootScope, Session, PurchaseStatusService, PurchaseService) {
		// 		//Define language reference
		// 		$scope.language = $rootScope.language.instance;
		// 		$scope.header.setTitle('labels.purchases');
		// 		$scope.header.icon.showBack();
		// 		$scope.purchaseStatuses = {};
		// 		$scope.purchases = [];
		// 		//Keeps the pager control
		// 		$scope.pager = {
		// 			totalResults: 0,
		// 			maxResultCount: 10,
		// 			currentPage: 1
		// 		}

		// 		//--------------------------------------------------------------

		// 		/**
		// 		 * Loads possible purchase statuses
		// 		 */
		// 		$scope.loadPurchaseStatus = function () {
		// 			/**
		// 			 * Executes purchase status list service
		// 			 */
		// 			PurchaseStatusService.get({}, {}).$promise.then(function (result) {
		// 				//In case of request error
		// 				if (!result.success) {
		// 					//And a message is available
		// 					if (result.message) {
		// 						console.log(result.message);
		// 						alert(result.message);
		// 					}

		// 					return false;
		// 				}

		// 				//Map results to the scope object
		// 				$scope.purchaseStatuses = result.data;

		// 				//Invokes the purchase list seeker
		// 				$scope.loadPurchases();
		// 			})
		// 		}

		// 		/**
		// 		 * Loads logged user purchases
		// 		 */
		// 		$scope.loadPurchases = function () {
		// 			/**
		// 			 * Executes credit statement list service
		// 			 */
		// 			PurchaseService.get({ limite: $scope.pager.maxResultCount, pagina: $scope.pager.currentPage }, {}).$promise.then(function(result){
		// 				//In case of request error
		// 				if (!result.success) {
		// 					//And a message is available
		// 					if (result.message) {
		// 						console.log(result.message);
		// 						alert(result.message);
		// 					}

		// 					return false;
		// 				}

		// 				/**
		// 				 * For each of the results, corrects status message
		// 				 */
		// 				result.data.resultados.forEach(function(item) {
		// 					item.status = $scope.purchaseStatuses[item.status]
		// 				})

		// 				//Map results and totalizer
		// 				$scope.purchases = result.data.resultados;
		// 				$scope.pager.totalResults = result.data.totalResultadosBusca;
		// 			})
		// 		}

		// 		//--------------------------------------------------------------

		// 		/**
		// 		 * Initializes screen functions
		// 		 */
		// 		$scope.init = function () {
		// 			//Gets the possible purchase statuses
		// 			$scope.loadPurchaseStatus();
		// 		}

		// 		$scope.init();
		// 	}]
	}

	return oDashboard;
})();
