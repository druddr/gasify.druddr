﻿var NotSupported = (function () {

	var oNotSupportedController = {
		services: [],
		components: [],
		/// Route usage:
		/// #/app/not-supported/message
		message: ['$scope', '$location', 'Session', function ($scope, $location, Session) {
			//Sets the language at the scope
			$scope.language = $scope.language.instance;

			$scope.userProfile = {
				name: $scope.language.labels.user.toLowerCase()
			}

			/**
			 * Initializes the scope functionality
			 */
			$scope.init = function () {
				//Sets user name in case it is logged in
				if (Session.current.userProfile)
					$scope.userProfile.name = Session.current.userProfile.name;
			}

			$scope.init();
		}]
	}

	return oNotSupportedController;
})();
