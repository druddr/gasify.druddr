﻿/**
 * Authentication angular controller
 * Base URL: #/app/authentication/[methods]
 *
 * Nb.1: This is used when the code is injected into the current state
 *
 * @property "components":
 * 	In order for some components to be useful here it depends on this mapping.
 * 	Some components might have already been imported globally at ./app/app.js
 *
 * @property "services":
 * 	In order for services to be "in sight" for this controller,
 *  They should be declared as a "service" inside this property.
 *
 * @author bruno.teixeira.silva
 * @version 1.0.0
 */
var Authentication = (function () {
	var oAuthenticationController = {
		components: [
		],
		services: [
		],
		/// Route usage:
		/// #/app/authentication/login
		login: [
			'$scope',
			'$location',
			'Session',
			function ($scope, $location, Session) {
				//Scope usable variables
				$scope.user = {};
				//TODO: define whether to use this or not
				$scope.loggingIn = false;
				$scope.emailMode = false;

				//Hides the app-bar/header
				$scope.header.hide();

				/**
				 * Enables e-mail mode screen
				 */
				$scope.enableEmailMode = function () {
					return $scope.emailMode = true;
				}

				//Drives the user to registration
				$scope.userRegister = function () {
					$scope.navigate($scope.routes.user.register);
				}

				//Drives the user to lost password form
				$scope.userLostPassword = function () {
					$scope.navigate($scope.routes.user.lostPassword);
				};
		}],
		/// Route usage:
		/// #/app/authentication/logout
		logout: [
			'$scope',
			'$rootScope',
			'$timeout',
			'Session',
			function ($scope, $rootScope, $timeout, Session) {
				//Hides the app-bar/header
				$scope.header.hide();

				//Signs out of firebase service
				$rootScope.auth().$signOut();

				//user should be unlogged, cancels localStorage itens
				if (Session.current.user)
					Session.clear((session) => $rootScope.navigateToDefault())
				//	return $scope.navigate($scope.router.authentication)

				////Clears authentication by cleaning session storage
				//Session.clear((sess) => $rootScope.navigate($rootScope.router.authentication))
		}]
	}

	return oAuthenticationController;
})();
